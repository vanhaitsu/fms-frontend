import { toast } from "@/components/ui/use-toast";
import api from "@/config/axios";
import { User } from "@/types";
import { useNavigate } from "react-router-dom";
import { jwtDecode } from "jwt-decode";

const useAuth = () => {
  const navigate = useNavigate();

  // Utils
  const handleSuccessLogin = (data: any) => {
    const decodedJwt: any = jwtDecode(data.accessToken);
    const userData: User = {
      userId: decodedJwt["userId"],
      userEmail: decodedJwt["userEmail"],
      accessToken: data.accessToken,
      accessTokenExpiryTime: data.accessTokenExpiryTime,
      role: decodedJwt[
        "http://schemas.microsoft.com/ws/2008/06/identity/claims/role"
      ],
    };
    localStorage.setItem("user", JSON.stringify(userData));
    navigate("/home");
  };

  // Hooks
  const authenticated = !!localStorage.getItem("user");

  const user: User | null =
    localStorage.getItem("user") != null
      ? JSON.parse(localStorage.getItem("user")!)
      : null;

  const login = async (
    provider: "credentials" | "google",
    credentials: { email: string; password: string } | string
  ) => {
    if (provider === "credentials" && typeof credentials === "object") {
      try {
        const response = await api.post("api/v1/authentication/login", {
          email: credentials.email,
          password: credentials.password,
        });
        if (response.status) {
          handleSuccessLogin(response.data.data);
        }
      } catch (error: any) {
        toast({
          variant: "destructive",
          description: error.response.data.message,
        });
        console.error(error);
      }
    } else if (provider === "google" && typeof credentials === "string") {
      try {
        const response = await api.post(
          "api/v1/authentication/login-google?httpOnly=true",
          {
            idToken: credentials,
          }
        );
        if (response.status) {
          handleSuccessLogin(response.data.data);
        }
      } catch (error: any) {
        toast({
          variant: "destructive",
          description: error.response.data.message,
        });
        console.error(error);
      }
    }
  };

  const logout = () => {
    localStorage.clear();
    navigate("/login");
  };

  return { user, authenticated, login, logout };
};

export default useAuth;
