import { useSearchParams } from "react-router-dom";

const useQuery = () => {
  const [searchParams, setSearchParams] = useSearchParams();

  const setQuery = (key: string, value: string) => {
    if (value === "all") {
      searchParams.delete(key);
    } else {
      searchParams.set(key, value);
    }
    setSearchParams(searchParams);
  };

  const getQuery = () => {
    return searchParams;
  };

  const resetQuery = (expectedParams?: string[]) => {
    if (!expectedParams) {
      setSearchParams(undefined);
    } else {
      const paramsToDelete = [];
      for (const [key] of searchParams.entries()) {
        if (expectedParams && !expectedParams.includes(key)) {
          paramsToDelete.push(key);
        }
      }
      paramsToDelete.forEach((key) => searchParams.delete(key));
      setSearchParams(searchParams);
    }
  };

  const getQueryValue = (key: string) => {
    return searchParams.get(key) ?? "";
  };

  const isQuerying = (expectedParams?: string[]) => {
    for (const [key] of searchParams.entries()) {
      if (expectedParams && !expectedParams.includes(key)) {
        return true;
      }
    }
    return false;
  };

  const setSortDirection = () => {
    if (
      getQueryValue("order-by-descending") === null ||
      getQueryValue("order-by-descending") === "true"
    ) {
      setQuery("order-by-descending", "false");
    } else {
      setQuery("order-by-descending", "true");
    }
  };

  return {
    setQuery,
    getQuery,
    resetQuery,
    getQueryValue,
    isQuerying,
    setSortDirection,
  };
};

export default useQuery;
