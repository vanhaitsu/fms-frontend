import { Navigate, useRoutes } from "react-router-dom";
import Login from "./pages/login/page";
import NotFound from "./pages/not-found";
import PrivateRoute from "./routes/PrivateRoute";
import HomeLayout from "./pages/home/layout";
import Accounts from "./pages/home/pages/accounts/page";
import Freelancers from "./pages/home/pages/freelancers/page";
import Setting from "./pages/setting/page";
import Home from "./pages/home/page";
import Projects from "./pages/home/pages/projects/page";
import CreateProject from "./pages/home/pages/projects/pages/create-project/page";
import InviteAndDelivery from "./pages/home/pages/projects/pages/invite-and-delivery/page";
import ProjectId from "./pages/home/pages/projects/pages/[id]/page";
import UpdateProject from "./pages/home/pages/projects/pages/[id]/pages/update-project/page";
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage";
import AppliedProjects from "./pages/home/pages/applied-projects/page";
import DeliverableTypes from "./pages/home/pages/deliverable-type/page";
import Skills from "./pages/home/pages/skills/page";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAxdORtKAnvKPncLjDPXkVTVRL0mU8_9ZQ",
  authDomain: "fms-nextbean-edition.firebaseapp.com",
  projectId: "fms-nextbean-edition",
  storageBucket: "fms-nextbean-edition.appspot.com",
  messagingSenderId: "1084675511455",
  appId: "1:1084675511455:web:94d68b8cce0544c9d77eaa",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const storage = getStorage(app);

function App() {
  const routes = useRoutes([
    // Default URL
    {
      path: "/",
      element: <Navigate to={"/home"} />,
    },

    // Main URL
    {
      path: "/home",
      element: (
        <PrivateRoute isAllowed="PermitAll">
          <HomeLayout />
        </PrivateRoute>
      ),
      children: [
        {
          path: "",
          element: <Home />,
        },
        {
          path: "projects",
          children: [
            {
              path: "",
              element: (
                <PrivateRoute isAllowed="PermitAll">
                  <Projects />
                </PrivateRoute>
              ),
            },
            {
              path: ":id",
              children: [
                {
                  path: "",
                  element: (
                    <PrivateRoute isAllowed="PermitAll">
                      <ProjectId />
                    </PrivateRoute>
                  ),
                },
                {
                  path: "update-project",
                  element: (
                    <PrivateRoute isAllowed={["Administrator", "Staff"]}>
                      <UpdateProject />
                    </PrivateRoute>
                  ),
                },
              ],
            },
            {
              path: "create-project",
              children: [
                {
                  path: "",
                  element: (
                    <PrivateRoute isAllowed={["Administrator", "Staff"]}>
                      <CreateProject />
                    </PrivateRoute>
                  ),
                },
                {
                  path: "invite-and-delivery",
                  element: (
                    <PrivateRoute isAllowed={["Administrator", "Staff"]}>
                      <InviteAndDelivery />
                    </PrivateRoute>
                  ),
                },
              ],
            },
          ],
        },
        {
          path: "applied-projects",
          children: [
            {
              path: "",
              element: (
                <PrivateRoute isAllowed={["Freelancer"]}>
                  <AppliedProjects />
                </PrivateRoute>
              ),
            },
            {
              path: ":id",
              element: (
                <PrivateRoute isAllowed="PermitAll">
                  <ProjectId />
                </PrivateRoute>
              ),
            },
          ],
        },
        {
          path: "accounts",
          element: (
            <PrivateRoute isAllowed={["Administrator"]}>
              <Accounts />
            </PrivateRoute>
          ),
        },
        {
          path: "freelancers",
          element: (
            <PrivateRoute isAllowed={["Administrator"]}>
              <Freelancers />
            </PrivateRoute>
          ),
        },
        {
          path: "deliverable-types",
          element: (
            <PrivateRoute isAllowed={["Administrator", "Staff"]}>
              <DeliverableTypes />
            </PrivateRoute>
          ),
        },
        {
          path: "skills",
          element: (
            <PrivateRoute isAllowed={["Administrator", "Staff"]}>
              <Skills />
            </PrivateRoute>
          ),
        },
        {
          path: "setting",
          element: (
            <PrivateRoute isAllowed={"PermitAll"}>
              <Setting />
            </PrivateRoute>
          ),
        },
      ],
    },

    // Elements URL
    {
      path: "/login",
      element: <Login />,
    },

    // Not found URL
    {
      path: "*",
      element: <NotFound />,
    },
  ]);

  return routes;
}

export default App;
