import { ProjectVisibility } from "@/types/initialSeed";
import { create } from "zustand";

interface UpdateState {
  status: boolean;
  toggleStatus: () => void;
}

interface CreateProjectState {
  id: string | null;
  visibility: ProjectVisibility;
  category: string | null;
  invited: boolean;
  setId: (id: string) => void;
  setInvited: (invited: boolean) => void;
  setCategory: (category: string) => void;
  setVisibility: (visibility: ProjectVisibility) => void;
}

export const useUpdateAccountStore = create<UpdateState>()((set) => ({
  status: false,
  toggleStatus: () => set((state) => ({ status: !state.status })),
}));

export const useUpdateSkillStore = create<UpdateState>()((set) => ({
  status: false,
  toggleStatus: () => set((state) => ({ status: !state.status })),
}));

export const useUpdateFreelancerStore = create<UpdateState>()((set) => ({
  status: false,
  toggleStatus: () => set((state) => ({ status: !state.status })),
}));

export const useUpdateProfileStore = create<UpdateState>()((set) => ({
  status: false,
  toggleStatus: () => set((state) => ({ status: !state.status })),
}));

export const useUpdateProjectApplyStore = create<UpdateState>()((set) => ({
  status: false,
  toggleStatus: () => set((state) => ({ status: !state.status })),
}));

export const useUpdateProjectDeliverableStore = create<UpdateState>()(
  (set) => ({
    status: false,
    toggleStatus: () => set((state) => ({ status: !state.status })),
  })
);

export const useUpdateDeliverableTypeStore = create<UpdateState>()((set) => ({
  status: false,
  toggleStatus: () => set((state) => ({ status: !state.status })),
}));

export const useCreateProjectStore = create<CreateProjectState>()((set) => ({
  id: null,
  invited: false,
  category: null,
  visibility: ProjectVisibility.Private,
  setId: (id: string) => set(() => ({ id })),
  setInvited: (invited: boolean) => set(() => ({ invited })),
  setCategory: (category: string) => set(() => ({ category })),
  setVisibility: (visibility: ProjectVisibility) => set(() => ({ visibility })),
}));
