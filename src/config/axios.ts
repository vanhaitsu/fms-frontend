import axios, { AxiosRequestConfig } from "axios";
import applyCaseMiddleware from "axios-case-converter";
import { User } from "@/types";

const config: AxiosRequestConfig = {
  baseURL: import.meta.env.VITE_API_URL,
  withCredentials: true,
};

const options = {
  caseFunctions: {
    snake: (str: string) =>
      str.replace(/[A-Z]/g, (letter) => `-${letter.toLowerCase()}`),
    camel: (str: string) => str.replace(/-([a-z])/g, (g) => g[1].toUpperCase()),
  },
};

// Create axios instance with case conversion middleware
const api = applyCaseMiddleware(axios.create(config), options);

// Add request interceptor
api.interceptors.request.use((config) => {
  const user = localStorage.getItem("user");
  if (user) {
    config.headers["Authorization"] = "Bearer " + JSON.parse(user).accessToken;
  }
  return config;
});

// Add response interceptor
api.interceptors.response.use(
  (response) => response,
  async (error) => {
    const originalRequest = error.config;
    const alreadyRefreshed = originalRequest.alreadyRefreshed;
    const user = localStorage.getItem("user");
    if (error.response?.data?.isBlocking) {
      localStorage.clear();
      console.error("You are blocked");
      window.location.href = "/login";
    } else if (
      error.response?.status === 401 &&
      !originalRequest._retry &&
      user &&
      new Date(JSON.parse(user).accessTokenExpiryTime) < new Date() &&
      !alreadyRefreshed
    ) {
      try {
        const response = await api.post(
          "api/v1/authentication/refresh-token?is-freelancer=" +
            (JSON.parse(user).role === "Freelancer"),
          {
            accessToken: JSON.parse(user).accessToken,
          },
          {
            withCredentials: true,
          }
        );
        if (response.status) {
          const userData: User = {
            ...JSON.parse(user),
            accessToken: response.data.data.accessToken,
            accessTokenExpiryTime: response.data.data.accessTokenExpiryTime,
          };
          localStorage.setItem("user", JSON.stringify(userData));
          console.log(response.data.message);
          originalRequest.alreadyRefreshed = true;
          return api(originalRequest);
        }
      } catch (error: any) {
        console.error(error.response?.data?.message);
      }
    }
    return Promise.reject(error);
  }
);

export default api;
