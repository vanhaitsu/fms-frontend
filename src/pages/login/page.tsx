import Logo from "@/components/Logo";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { useState } from "react";
import { useGoogleLogin } from "@react-oauth/google";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import useAuth from "@/hooks/useAuth";
import axios from "axios";
import { Navigate } from "react-router-dom";
import { loginSchema } from "@/lib/form";

const Login = () => {
  const [isLoading, setIsLoading] = useState(false);
  const auth = useAuth();

  const form = useForm<z.infer<typeof loginSchema>>({
    resolver: zodResolver(loginSchema),
  });

  const login = async (values: z.infer<typeof loginSchema>) => {
    setIsLoading(true);
    await auth.login("credentials", {
      email: values.email,
      password: values.password,
    });
    setIsLoading(false);
  };

  const loginGoogle = useGoogleLogin({
    flow: "auth-code",
    onSuccess: async (getCode) => {
      try {
        setIsLoading(true);
        const response = await axios.post(
          "https://oauth2.googleapis.com/token",
          {
            client_id: import.meta.env.VITE_GOOGLE_CLIENT_ID,
            client_secret: import.meta.env.VITE_GOOGLE_CLIENT_SECRET,
            code: getCode.code,
            redirect_uri: import.meta.env.VITE_FMS_URL,
            grant_type: "authorization_code",
          }
        );
        if (response.status) {
          auth.login("google", response.data.id_token);
        }
      } catch (error) {
        console.error(error);
      } finally {
        setIsLoading(false);
      }
    },
  });

  if (auth.authenticated) {
    return <Navigate to="/home" />;
  } else {
    return (
      <main className="h-full lg:grid lg:grid-cols-2">
        <div className="hidden bg-muted lg:block">
          <img
            src="https://images.unsplash.com/photo-1483366774565-c783b9f70e2c?q=80&w=2670&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
            alt="Login page banner"
            className="object-cover size-full"
          />
        </div>

        <div className="p-6 h-full flex flex-col justify-between">
          <Logo displayName />

          <div className="sm:w-96 w-full flex flex-col gap-4 mx-auto">
            <div>
              <h3 className="flex gap-2 items-center">Welcome back</h3>
              <p className="text-muted-foreground">
                Continue with your account
              </p>
            </div>

            <Form {...form}>
              <form
                id="form"
                className="space-y-4"
                onSubmit={form.handleSubmit(login)}
              >
                <FormField
                  control={form.control}
                  name="email"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Email</FormLabel>
                      <FormControl>
                        <Input
                          disabled={isLoading}
                          placeholder="abc@example.com"
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />

                <FormField
                  control={form.control}
                  name="password"
                  render={({ field }) => (
                    <FormItem>
                      <div className="flex items-center">
                        <FormLabel>Password</FormLabel>
                        <a
                          href="/forgot-password"
                          className="ml-auto inline-block text-sm font-normal underline"
                        >
                          Forgot your password?
                        </a>
                      </div>
                      <FormControl>
                        <Input
                          type="password"
                          disabled={isLoading}
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </form>
            </Form>

            <Button form="form" disabled={isLoading}>
              Login
            </Button>

            <div className="flex items-center text-xs w-full uppercase text-muted-foreground">
              <div className="flex-grow border-t"></div>
              <p className="flex-shrink mx-2">Or continue with</p>
              <div className="flex-grow border-t"></div>
            </div>

            <Button
              disabled={isLoading}
              variant="outline"
              onClick={() => loginGoogle()}
            >
              <img
                src="https://freesvg.org/img/1534129544.png"
                className="size-4 mr-2"
              />
              Google
            </Button>

            <div className="text-center text-sm">
              Don&apos;t have an account?{" "}
              <a href="/register" className="underline">
                Register
              </a>
            </div>
          </div>

          <p className="text-sm text-muted-foreground text-center">
            Copyright © 2024 FMS NextBean Edition
          </p>
        </div>
      </main>
    );
  }
};

export default Login;
