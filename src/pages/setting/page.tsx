import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import useAuth from "@/hooks/useAuth";
import StaffUpdateForm from "./components/StaffUpdateForm";
import ChangePasswordCard from "./components/ChangePasswordCard";
import FreelancerUpdateForm from "./components/FreelancerUpdateForm";
import { useState } from "react";
import { Freelancer } from "@/types";
import { ArrowRight, CircleAlert, CircleDollarSign } from "lucide-react";
import { formatNumberToDecimal } from "@/lib/utils";
import { Badge } from "@/components/ui/badge";

const Setting = () => {
  const auth = useAuth();
  const [freelancer, setFreelancer] = useState<Freelancer | null>(null);

  return (
    <main className="p-6">
      <div className="space-y-6 md:w-2/3 xl:w-1/2 mx-auto">
        <Card>
          <CardHeader>
            <CardTitle>Profile</CardTitle>
            <CardDescription>Manage your information</CardDescription>
          </CardHeader>
          <CardContent>
            {auth.user?.role === "Freelancer" ? (
              <FreelancerUpdateForm
                freelancer={freelancer}
                setFreelancer={setFreelancer}
              />
            ) : (
              <StaffUpdateForm />
            )}
          </CardContent>
        </Card>

        {auth.user?.role !== "Freelancer" && <ChangePasswordCard />}

        {auth.user?.role === "Freelancer" && (
          <>
            <Card>
              <CardHeader className="pb-2">
                <CardTitle className="text-sm">Skills</CardTitle>
                <CardDescription>
                  These are all your verified skills
                </CardDescription>
              </CardHeader>
              <CardContent className="flex-wrap gap-2 flex">
                {freelancer?.skills.map((type) =>
                  type.skillNames.map((name) => (
                    <Badge key={name} variant="outline">
                      {name}
                    </Badge>
                  ))
                )}
              </CardContent>

              <CardFooter className="border-t bg-muted/50 py-4">
                <a
                  href="#"
                  className="group/item text-primary text-sm flex gap-2 items-center justify-center sm:justify-start"
                >
                  Update your skill? Please contact{" "}
                  <ArrowRight className="size-4 group-hover/item:translate-x-1 transition" />
                </a>
              </CardFooter>
            </Card>

            <div className="grid grid-cols-1 sm:grid-cols-2 gap-6">
              <Card>
                <CardHeader className="pb-2">
                  <CardTitle className="text-sm flex items-center justify-between">
                    Warning{" "}
                    <CircleAlert
                      // strokeWidth={1}
                      className="size-4 shrink-0 text-muted-foreground"
                    />
                  </CardTitle>
                </CardHeader>
                <CardContent>
                  <h3>{freelancer?.warning}</h3>
                </CardContent>

                <CardFooter className="border-t bg-muted/50 py-4">
                  <a
                    href="#"
                    className="group/item text-primary text-sm flex gap-2 items-center justify-center sm:justify-start"
                  >
                    Report{" "}
                    <ArrowRight className="size-4 group-hover/item:translate-x-1 transition" />
                  </a>
                </CardFooter>
              </Card>

              <Card>
                <CardHeader className="pb-2">
                  <CardTitle className="text-sm flex items-center justify-between">
                    Wallet{" "}
                    <CircleDollarSign
                      // strokeWidth={1}
                      className="size-4 shrink-0 text-muted-foreground"
                    />
                  </CardTitle>
                </CardHeader>
                <CardContent>
                  <h3>
                    {formatNumberToDecimal(freelancer?.wallet)}{" "}
                    <span className="text-xl font-normal text-muted-foreground">
                      coins
                    </span>
                  </h3>
                </CardContent>

                <CardFooter className="border-t bg-muted/50 py-4">
                  <a
                    href="#"
                    className="group/item text-primary text-sm flex gap-2 items-center justify-center sm:justify-start"
                  >
                    Withdraw{" "}
                    <ArrowRight className="size-4 group-hover/item:translate-x-1 transition" />
                  </a>
                </CardFooter>
              </Card>
            </div>
          </>
        )}
      </div>
    </main>
  );
};

export default Setting;
