import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { accountChangePasswordSchema } from "@/lib/form";
import { useState } from "react";
import { toast } from "@/components/ui/use-toast";
import { CHANGE_PASSWORD } from "@/lib/api";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";

const ChangePasswordCard = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [isEditing, setIsEditing] = useState(false);

  const form = useForm<z.infer<typeof accountChangePasswordSchema>>({
    resolver: zodResolver(accountChangePasswordSchema),
  });

  const onSubmit = async (
    values: z.infer<typeof accountChangePasswordSchema>
  ) => {
    try {
      setIsLoading(true);
      const response = await CHANGE_PASSWORD(values);
      if (response.status) {
        toast({
          description: response.data.message,
        });
        form.reset();
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Card>
      <CardHeader>
        <CardTitle>Change password</CardTitle>
        <CardDescription>
          Password must be from 8 to 128 characters, contain at least one
          uppercase letter and one number
        </CardDescription>
      </CardHeader>
      <CardContent>
        <Form {...form}>
          <form
            id="change-password"
            onSubmit={form.handleSubmit(onSubmit)}
            className="grid gap-4 w-full"
          >
            <FormField
              control={form.control}
              name="oldPassword"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel>Old Password *</FormLabel>
                  <FormControl>
                    <Input
                      type="password"
                      disabled={isLoading || !isEditing}
                      value={field.value ?? ""}
                      onChange={field.onChange}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="newPassword"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel>New Password *</FormLabel>
                  <FormControl>
                    <Input
                      type="password"
                      disabled={isLoading || !isEditing}
                      value={field.value ?? ""}
                      onChange={field.onChange}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="confirmPassword"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel>Confirm Password *</FormLabel>
                  <FormControl>
                    <Input
                      type="password"
                      disabled={isLoading || !isEditing}
                      value={field.value ?? ""}
                      onChange={field.onChange}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </form>
        </Form>
      </CardContent>

      <CardFooter>
        {isEditing ? (
          <div className="space-x-2 ml-auto w-fit">
            <Button
              variant="outline"
              onClick={() => {
                setIsEditing(false);
                form.reset();
              }}
            >
              Cancel
            </Button>
            <Button type="submit" form="change-password">
              Save
            </Button>
          </div>
        ) : (
          <Button
            variant="outline"
            className="ml-auto flex"
            onClick={() => setIsEditing(true)}
          >
            Edit
          </Button>
        )}
      </CardFooter>
    </Card>
  );
};

export default ChangePasswordCard;
