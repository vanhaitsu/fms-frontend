import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { useEffect, useState } from "react";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import InputDate from "@/components/InputDate";
import { toast } from "@/components/ui/use-toast";
import { GET_ACCOUNT, UPDATE_ACCOUNT } from "@/lib/api";
import { accountUpdateSchema } from "@/lib/form";
import { Account } from "@/types";
import useAuth from "@/hooks/useAuth";
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { Badge } from "@/components/ui/badge";
import { Separator } from "@/components/ui/separator";
import { BriefcaseBusiness } from "lucide-react";
import { Button } from "@/components/ui/button";
import { useUpdateProfileStore } from "@/config/store";

const StaffUpdateForm = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [isEditing, setIsEditing] = useState(false);
  const [account, setAccount] = useState<Account | null>(null);
  const auth = useAuth();
  const update = useUpdateProfileStore();

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_ACCOUNT(auth.user!.userId);
      if (response.status) {
        setAccount(response.data.data);
        setIsLoading(false);
      }
    };
    fetch();
  }, []);

  const setValue = () => {
    form.setValue("firstName", account!.firstName);
    form.setValue("lastName", account!.lastName);
    form.setValue("email", account!.email);
    form.setValue("code", account!.code!.toString());
    form.setValue("gender", account!.gender === "Male" ? 1 : 2);
    form.setValue("role", account!.role === "Administrator" ? 0 : 1);
    form.setValue("dateOfBirth", new Date(account!.dateOfBirth));
    form.setValue("phoneNumber", account!.phoneNumber);
  };

  useEffect(() => {
    if (account) {
      setValue();
    }
  }, [account]);

  const form = useForm<z.infer<typeof accountUpdateSchema>>({
    resolver: zodResolver(accountUpdateSchema),
  });

  const onSubmit = async (values: z.infer<typeof accountUpdateSchema>) => {
    try {
      setIsLoading(true);
      const response = await UPDATE_ACCOUNT(values, auth.user!.userId);
      if (response.status) {
        toast({
          description: response.data.message,
        });
        setIsEditing(false);
        update.toggleStatus();
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="space-y-4">
      <div className="flex items-center gap-4">
        <Avatar className="size-16">
          <AvatarImage src={account?.image ?? "#"} />
          <AvatarFallback>
            {account?.firstName.toString().charAt(0).toUpperCase()}
          </AvatarFallback>
        </Avatar>

        <div>
          {/* <Badge variant="secondary" className="gap-2">
            <BriefcaseBusiness className="size-4" />
            <Separator orientation="vertical" className="h-3" />
            {account?.role}
          </Badge> */}
          <h4>{account?.code}</h4>
          <p className="text-muted-foreground">{account?.email}</p>
        </div>
      </div>

      <Form {...form}>
        <form
          id="update"
          onSubmit={form.handleSubmit(onSubmit)}
          className="grid grid-cols-1 sm:grid-cols-2 gap-4 w-full"
        >
          <FormField
            control={form.control}
            name="firstName"
            render={({ field }) => (
              <FormItem className="w-full">
                <FormLabel>First Name *</FormLabel>
                <FormControl>
                  <Input
                    disabled={isLoading || !isEditing}
                    value={field.value ?? ""}
                    onChange={field.onChange}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="lastName"
            render={({ field }) => (
              <FormItem className="w-full">
                <FormLabel>Last Name *</FormLabel>
                <FormControl>
                  <Input
                    disabled={isLoading || !isEditing}
                    value={field.value ?? ""}
                    onChange={field.onChange}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="gender"
            render={({ field }) => (
              <FormItem className="w-full">
                <FormLabel>Gender *</FormLabel>
                <FormControl>
                  <Select
                    disabled={isLoading || !isEditing}
                    onValueChange={(value) => field.onChange(Number(value))}
                    value={field.value != undefined ? String(field.value) : ""}
                  >
                    <SelectTrigger className="w-full">
                      <SelectValue placeholder="Gender" />
                    </SelectTrigger>
                    <SelectContent>
                      <SelectItem value="1">Male</SelectItem>
                      <SelectItem value="2">Female</SelectItem>
                    </SelectContent>
                  </Select>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="dateOfBirth"
            render={({ field }) => (
              <FormItem className="w-full">
                <FormLabel>Date of Birth *</FormLabel>
                <FormControl>
                  <InputDate
                    disabled={isLoading || !isEditing}
                    date={field.value}
                    setDate={field.onChange}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="phoneNumber"
            render={({ field }) => (
              <FormItem className="sm:col-span-2">
                <FormLabel>Phone Number *</FormLabel>
                <FormControl>
                  <Input
                    disabled={isLoading || !isEditing}
                    value={field.value ?? ""}
                    onChange={field.onChange}
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
        </form>
      </Form>

      {isEditing ? (
        <div className="space-x-2 ml-auto w-fit">
          <Button
            variant="outline"
            onClick={() => {
              setIsEditing(false);
              form.clearErrors();
              setValue();
            }}
          >
            Cancel
          </Button>
          <Button type="submit" form="update">
            Save
          </Button>
        </div>
      ) : (
        <Button
          variant="outline"
          className="ml-auto flex"
          onClick={() => setIsEditing(true)}
        >
          Edit
        </Button>
      )}
    </div>
  );
};

export default StaffUpdateForm;
