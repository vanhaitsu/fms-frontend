import { ArrowRight, Frown } from "lucide-react";

const NotFound = () => {
  return (
    <main className="flex justify-center items-center h-full gap-4 flex-col sm:flex-row">
      <div className="flex gap-2 items-center">
        <p className="text-6xl font-medium">4</p>
        <Frown className="size-16" />
        <p className="text-6xl font-medium">4</p>
      </div>

      <div>
        <h3>Page Not Found</h3>
        <a
          href="/home"
          className="group/item text-primary flex gap-2 items-center justify-center sm:justify-start"
        >
          Go to website{" "}
          <ArrowRight className="size-4 group-hover/item:translate-x-1 transition" />
        </a>
      </div>
    </main>
  );
};

export default NotFound;
