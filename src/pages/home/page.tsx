import PrivateComponent from "@/components/PrivateComponent";
import Dashboard from "./components/Dashboard";
import { Navigate } from "react-router-dom";
import useAuth from "@/hooks/useAuth";
import FreelancerDashboardCard from "./components/FreelancerDashboardCard";

const Home = () => {
  const auth = useAuth();

  return (
    <main className="p-6">
      <div className="space-y-6 w-full md:max-w-6xl mx-auto">
        <PrivateComponent isAllowed={["Freelancer"]}>
          {/* <Navigate to="projects" /> */}
          <FreelancerDashboardCard />
        </PrivateComponent>

        <PrivateComponent isAllowed={["Administrator", "Staff"]}>
          <Dashboard />
        </PrivateComponent>
      </div>
    </main>
  );
};

export default Home;
