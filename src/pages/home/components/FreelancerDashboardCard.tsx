import IsLoading from "@/components/IsLoading";
import { Badge } from "@/components/ui/badge";
import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card";
import useAuth from "@/hooks/useAuth";
import { GET_DASHBOARD } from "@/lib/api";
import { formatNumberToDecimal } from "@/lib/utils";
import { FreelancerDashboard } from "@/types";
import {
  DeliverableProductStatus,
  ProjectApplyStatus,
  ProjectStatus,
} from "@/types/initialSeed";
import {
  ChevronRight,
  CircleAlert,
  CircleCheck,
  CircleDollarSign,
  CircleDot,
  Folder,
  Handshake,
  LoaderCircle,
  Megaphone,
  Pin,
  XCircle,
} from "lucide-react";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const FreelancerDashboardCard = () => {
  const [dashboard, setDashboard] = useState<FreelancerDashboard | null>(null);
  const auth = useAuth();

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_DASHBOARD(auth.user!.role);
      if (response.status) {
        setDashboard(response.data.data);
      }
    };
    fetch();
  }, []);

  if (dashboard) {
    return (
      <div className="grid grid-cols-1 sm:grid-cols-12 gap-6">
        <h3 className="col-span-1 sm:col-span-full border-b pb-2">Dashboard</h3>

        {/* <Separator className="col-span-1 sm:col-span-full" /> */}

        <Card className="sm:col-span-3">
          <CardHeader className="pb-2">
            <CardTitle className="text-sm flex items-center justify-between">
              Warning <CircleAlert className="size-4 text-muted-foreground" />
            </CardTitle>
          </CardHeader>
          <CardContent>
            <h3>
              {dashboard.warning}{" "}
              {/* <span className="text-xl font-normal text-muted-foreground">
                projects
              </span> */}
            </h3>
          </CardContent>
        </Card>

        <Card className="sm:col-span-3">
          <CardHeader className="pb-2">
            <CardTitle className="text-sm flex items-center justify-between">
              Wallet{" "}
              <CircleDollarSign className="size-4 text-muted-foreground" />
            </CardTitle>
          </CardHeader>
          <CardContent>
            <h3>
              {formatNumberToDecimal(dashboard.wallet)}{" "}
              <span className="text-xl font-normal text-muted-foreground">
                coins
              </span>
            </h3>
          </CardContent>
        </Card>

        <Card className="sm:col-span-3">
          <CardHeader className="pb-2">
            <CardTitle className="text-sm flex items-center justify-between">
              Your ongoing projects{" "}
              <Folder className="size-4 text-muted-foreground" />
            </CardTitle>
          </CardHeader>
          <CardContent>
            <h3>
              {dashboard.numOfYourOngoingProject}{" "}
              <span className="text-xl font-normal text-muted-foreground">
                projects
              </span>
            </h3>
          </CardContent>
        </Card>

        <Card className="sm:col-span-3">
          <CardHeader className="pb-2">
            <CardTitle className="text-sm flex items-center justify-between">
              Remain tasks <Pin className="size-4 text-muted-foreground" />
            </CardTitle>
          </CardHeader>
          <CardContent>
            <h3>
              {dashboard.remainTasks}{" "}
              <span className="text-xl font-normal text-muted-foreground">
                products
              </span>
            </h3>
          </CardContent>
        </Card>

        <Card className="sm:col-span-6">
          <CardHeader className="pb-2">
            <CardTitle className="text-sm flex items-center justify-between">
              Your recent projects
            </CardTitle>
          </CardHeader>
          <CardContent className="space-y-4">
            {dashboard.recentProjects.map((item) => (
              <div>
                <Link to={`/home/projects/${item.project?.id}`} className="">
                  <div className="p-4 border rounded-lg hover:bg-muted/40 flex items-center gap-4 shadow-sm">
                    <div className="w-full">
                      <p className="font-semibold text-xs text-primary">
                        {item.project!.code}
                      </p>

                      <p className="font-medium">{item.project!.name}</p>

                      {/* <p className="text-sm text-muted-foreground line-clamp-2">
                        {item.project!.description}
                      </p> */}

                      {item.status === ProjectApplyStatus.Invited && (
                        <div className="text-xs font-medium text-primary flex gap-2 items-center mt-2">
                          <Megaphone className="size-4" /> You have been invited
                          to join this project.
                        </div>
                      )}

                      <Badge
                        variant={
                          item.project!.status === ProjectStatus.Done ||
                          item.project!.status === ProjectStatus.Closed
                            ? "default"
                            : "secondary"
                        }
                        className="w-full col-span-2 justify-center mt-2"
                      >
                        {ProjectStatus[item.project!.status]}
                      </Badge>
                    </div>
                  </div>
                </Link>
              </div>
            ))}

            {dashboard.recentProjects.length === 0 && (
              <div className="text-sm text-muted-foreground">No results</div>
            )}
          </CardContent>
        </Card>

        <Card className="sm:col-span-6 h-fit">
          <CardHeader className="pb-2">
            <CardTitle className="text-sm flex items-center justify-between">
              Recent products
            </CardTitle>
          </CardHeader>
          <CardContent className="space-y-4">
            {dashboard.recentProducts.map((item) => (
              <div>
                <Link to={`/home/projects/${item.projectId}`}>
                  <div className="p-4 border rounded-lg hover:bg-muted/40 flex items-center gap-4 shadow-sm">
                    {item.status === DeliverableProductStatus.Rejected && (
                      <XCircle className="size-4 text-destructive shrink-0" />
                    )}

                    {item.status === DeliverableProductStatus.Accepted && (
                      <CircleCheck className="size-4 text-primary shrink-0" />
                    )}

                    {item.status === DeliverableProductStatus.Checking && (
                      <CircleDot className="size-4 text-muted-foreground shrink-0" />
                    )}

                    <div className="grow">
                      <p className="font-semibold text-xs text-primary line-clamp-1">
                        {item.projectName}
                      </p>

                      <p className="font-medium">{item.name}</p>
                    </div>

                    <ChevronRight className="shrink-0 size-4 opacity-50" />
                  </div>
                </Link>
              </div>
            ))}

            {dashboard.recentProducts.length === 0 && (
              <div className="text-sm text-muted-foreground">No results</div>
            )}
          </CardContent>
        </Card>
      </div>
    );
  } else {
    return <IsLoading className="w-full" />;
  }
};

export default FreelancerDashboardCard;
