import Logo from "@/components/Logo";
import { Button, buttonVariants } from "@/components/ui/button";
import { FC, ReactNode, useEffect, useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import useAuth from "@/hooks/useAuth";
import { GET_USER } from "@/lib/api";
import { Account } from "@/types";
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { Sheet, SheetContent, SheetTrigger } from "@/components/ui/sheet";
import { ChevronUp, LogOut, PanelLeft, Settings } from "lucide-react";
import { useUpdateProfileStore } from "@/config/store";
import { Separator } from "@/components/ui/separator";

interface NavbarProps {
  linkList: {
    tittle?: string;
    url?: string;
    icon?: ReactNode;
    groupTittle?: string;
  }[];
}

const Navbar: FC<NavbarProps> = ({ linkList: linkList }) => {
  const auth = useAuth();
  const { pathname } = useLocation();
  const navigate = useNavigate();
  const [user, setUser] = useState<Account | null>(null);
  const [openState, setOpenState] = useState(false);
  const update = useUpdateProfileStore();

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_USER(auth.user!.userId, auth.user!.role);
      if (response.status) {
        setUser(response.data.data);
        if (auth.user!.role === "Freelancer") {
          sessionStorage.setItem(
            "skills",
            JSON.stringify(response.data.data.skills)
          );
        }
      }
    };
    fetch();
  }, [update.status]);

  const isOnRoute = (link: string) => {
    if (pathname === "/home" && link === "/home") {
      return true;
    } else if (pathname !== "/home" && link !== "/home") {
      return pathname.startsWith(link);
    }
    return false;
  };

  return (
    <>
      <aside className="hidden w-72 flex-col border-r bg-background lg:flex px-3 py-6 h-screen shrink-0 gap-6">
        <div className="px-3">
          <Logo displayName />
        </div>

        <nav className="flex flex-col font-medium text-sm grow">
          {linkList.map((item) =>
            item.groupTittle ? (
              <p className="text-[10px] px-3 uppercase font-semibold text-muted-foreground mt-4">
                {item.groupTittle}
              </p>
            ) : (
              <Link
                to={item.url ?? ""}
                className={`cursor-pointer flex items-center gap-2 rounded-md px-3 py-2 text-muted-foreground transition-all hover:text-primary ${
                  isOnRoute(item.url ?? "") && "bg-muted text-primary"
                }`}
              >
                {item.icon}
                {item.tittle}
              </Link>
            )
          )}
        </nav>

        <Separator />

        <DropdownMenu>
          <DropdownMenuTrigger
            className={`${buttonVariants({
              variant: "ghost",
            })} h-fit gap-2 w-full`}
          >
            <Avatar className="size-9">
              <AvatarImage src={user?.image} />
              <AvatarFallback>
                {user?.firstName.charAt(0).toUpperCase()}
              </AvatarFallback>
            </Avatar>

            <div className="w-full text-left text-wrap">
              <p>{user?.firstName}</p>
              <p className="text-xs text-muted-foreground">
                {user?.role ?? "Freelancer"}
              </p>
            </div>

            <ChevronUp className="size-4 shrink-0 opacity-50" />
          </DropdownMenuTrigger>

          <DropdownMenuContentComponent user={user} />
        </DropdownMenu>
      </aside>

      <header className="px-6 py-4 border-b sticky lg:hidden flex w-full top-0 bg-card justify-between items-center z-10">
        <Sheet open={openState} onOpenChange={setOpenState}>
          <SheetTrigger asChild>
            <Button size="icon" variant="outline">
              <PanelLeft className="size-4" />
              <span className="sr-only">Toggle Menu</span>
            </Button>
          </SheetTrigger>
          <SheetContent side="left" className="sm:max-w-xs">
            <nav className="grid gap-6 text-lg font-medium">
              <Logo displayName />

              {linkList.map((item) =>
                item.groupTittle ? (
                  <p className="text-sm px-3 uppercase font-semibold text-muted-foreground mt-4">
                    {item.groupTittle}
                  </p>
                ) : (
                  <Link
                    to={item.url ?? ""}
                    onClick={() => {
                      setOpenState(false);
                    }}
                    className="cursor-pointer flex items-center gap-4 px-3 text-muted-foreground hover:text-foreground"
                  >
                    {item.icon}
                    {item.tittle}
                  </Link>
                )
              )}
            </nav>
          </SheetContent>
        </Sheet>

        <DropdownMenu>
          <DropdownMenuTrigger>
            <Avatar>
              <AvatarImage src={user?.image} />
              <AvatarFallback>
                {user?.firstName.charAt(0).toUpperCase()}
              </AvatarFallback>
            </Avatar>
          </DropdownMenuTrigger>
          <DropdownMenuContentComponent user={user} />
        </DropdownMenu>
      </header>
    </>
  );
};

export default Navbar;

interface DropdownMenuContentComponentProps {
  user: Account | null;
}

const DropdownMenuContentComponent: FC<DropdownMenuContentComponentProps> = ({
  user,
}) => {
  const auth = useAuth();

  return (
    <DropdownMenuContent className="lg:w-[--radix-dropdown-menu-trigger-width]">
      <DropdownMenuLabel>
        My Account
        <p className="text-sm font-normal text-muted-foreground">
          {user?.email}
        </p>
      </DropdownMenuLabel>
      <DropdownMenuSeparator />
      <Link to={"/home/setting"}>
        <DropdownMenuItem className="gap-2">
          <Settings className="size-4" />
          Setting
        </DropdownMenuItem>
      </Link>
      <DropdownMenuSeparator />
      <DropdownMenuItem onClick={() => auth.logout()} className="gap-2">
        <LogOut className="size-4" /> Logout
      </DropdownMenuItem>
    </DropdownMenuContent>
  );
};
