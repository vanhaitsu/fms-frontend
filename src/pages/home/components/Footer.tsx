import Logo from "@/components/Logo";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";

const Footer = () => {
  return (
    <footer className="p-6 border-t mt-auto bg-card space-y-8">
      <div className="flex flex-col sm:flex-row gap-4 justify-between items-center">
        <div className="gap-4 flex sm:flex-row items-center flex-col justify-center">
          <Logo className="w-fit" />
          <a className="hover:text-primary text-sm" href="#">
            About
          </a>
          <a className="hover:text-primary text-sm" href="#">
            Services
          </a>
          <a className="hover:text-primary text-sm" href="#">
            Terms and Conditions
          </a>
          <a className="hover:text-primary text-sm" href="#">
            Privacy Policy
          </a>
        </div>

        <div className="flex gap-2 w-full sm:w-fit">
          <Input placeholder="Enter your Email" className="w-full xl:w-96" />
          <Button>Subscribe</Button>
        </div>
      </div>

      <div className="flex flex-col sm:flex-row justify-between gap-2 items-center">
        <p className="text-sm text-muted-foreground">
          Copyright © 2024 FMS NextBean Edition
        </p>

        <div className="flex gap-2">
          <p className="text-sm text-muted-foreground font-medium">
            Get Mobile App
          </p>

          <a
            className="hover:text-primary text-sm text-muted-foreground"
            href="#"
          >
            iOS
          </a>

          <a
            className="hover:text-primary text-sm text-muted-foreground"
            href="#"
          >
            Android
          </a>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
