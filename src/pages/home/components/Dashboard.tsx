import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card";
import { Separator } from "@/components/ui/separator";
import { GET_DASHBOARD } from "@/lib/api";
import { formatNumberToDecimal } from "@/lib/utils";
import { AdministratorDashboard, StaffDashboard } from "@/types";
import {
  BriefcaseBusiness,
  ChevronRight,
  CircleCheck,
  CircleDollarSign,
  CircleDot,
  Folder,
  User,
  XCircle,
} from "lucide-react";
import { useEffect, useState } from "react";
import ProjectItem from "../pages/projects/components/ProjectItem";
import { Link } from "react-router-dom";
import { DeliverableProductStatus, ProjectStatus } from "@/types/initialSeed";
import { Badge } from "@/components/ui/badge";
import PrivateComponent from "@/components/PrivateComponent";
import useAuth from "@/hooks/useAuth";
import IsLoading from "@/components/IsLoading";

const Dashboard = () => {
  const [dashboard, setDashboard] = useState<AdministratorDashboard | null>(
    null
  );
  const auth = useAuth();

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_DASHBOARD(auth.user!.role);
      if (response.status) {
        setDashboard(response.data.data);
      }
    };
    fetch();
  }, []);

  if (dashboard) {
    return (
      <div className="grid grid-cols-1 sm:grid-cols-12 gap-6">
        <PrivateComponent isAllowed={["Administrator"]}>
          <h3 className="col-span-1 sm:col-span-full border-b pb-2">
            System overview
          </h3>

          <Card className="sm:col-span-3">
            <CardHeader className="pb-2">
              <CardTitle className="text-sm flex items-center justify-between">
                Total freelancer{" "}
                <BriefcaseBusiness className="size-4 text-muted-foreground" />
              </CardTitle>
            </CardHeader>
            <CardContent>
              <h3>
                {dashboard.numOfFreelancer}{" "}
                <span className="text-xl font-normal text-muted-foreground">
                  users
                </span>
              </h3>
            </CardContent>
          </Card>

          <Card className="sm:col-span-3">
            <CardHeader className="pb-2">
              <CardTitle className="text-sm flex items-center justify-between">
                Total account <User className="size-4 text-muted-foreground" />
              </CardTitle>
            </CardHeader>
            <CardContent>
              <h3>
                {dashboard.numOfAccount}{" "}
                <span className="text-xl font-normal text-muted-foreground">
                  users
                </span>
              </h3>
            </CardContent>
          </Card>

          <Card className="sm:col-span-3">
            <CardHeader className="pb-2">
              <CardTitle className="text-sm flex items-center justify-between">
                Total project{" "}
                <Folder className="size-4 text-muted-foreground" />
              </CardTitle>
            </CardHeader>
            <CardContent>
              <h3>
                {dashboard.numOfProject}{" "}
                <span className="text-xl font-normal text-muted-foreground">
                  projects
                </span>
              </h3>
            </CardContent>
          </Card>

          <Card className="sm:col-span-3">
            <CardHeader className="pb-2">
              <CardTitle className="text-sm flex items-center justify-between">
                Total paid{" "}
                <CircleDollarSign className="size-4 text-muted-foreground" />
              </CardTitle>
            </CardHeader>
            <CardContent>
              <h3>
                {formatNumberToDecimal(dashboard.totalPaid)}{" "}
                <span className="text-xl font-normal text-muted-foreground">
                  coins
                </span>
              </h3>
            </CardContent>
          </Card>
        </PrivateComponent>

        <h3 className="col-span-1 sm:col-span-full border-b pb-2">Dashboard</h3>

        {/* <Separator className="col-span-1 sm:col-span-full" /> */}

        <Card className="sm:col-span-4">
          <CardHeader className="pb-2">
            <CardTitle className="text-sm flex items-center justify-between">
              Your ongoing projects{" "}
              <Folder className="size-4 text-muted-foreground" />
            </CardTitle>
          </CardHeader>
          <CardContent>
            <h3>
              {dashboard.numOfYourOngoingProject}{" "}
              <span className="text-xl font-normal text-muted-foreground">
                projects
              </span>
            </h3>
          </CardContent>
        </Card>

        <Card className="sm:col-span-4">
          <CardHeader className="pb-2">
            <CardTitle className="text-sm flex items-center justify-between">
              Waiting for checking{" "}
              <CircleDot className="size-4 text-muted-foreground" />
            </CardTitle>
          </CardHeader>
          <CardContent>
            <h3>
              {dashboard.numOfWaitingChecking}{" "}
              <span className="text-xl font-normal text-muted-foreground">
                products
              </span>
            </h3>
          </CardContent>
        </Card>

        <Card className="sm:col-span-4">
          <CardHeader className="pb-2">
            <CardTitle className="text-sm flex items-center justify-between">
              Your total paid{" "}
              <CircleDollarSign className="size-4 text-muted-foreground" />
            </CardTitle>
          </CardHeader>
          <CardContent>
            <h3>
              {formatNumberToDecimal(dashboard.yourTotalPaid)}{" "}
              <span className="text-xl font-normal text-muted-foreground">
                coins
              </span>
            </h3>
          </CardContent>
        </Card>

        <Card className="sm:col-span-6">
          <CardHeader className="pb-2">
            <CardTitle className="text-sm flex items-center justify-between">
              Your recent projects
            </CardTitle>
          </CardHeader>
          <CardContent className="space-y-4">
            {dashboard.recentProjects.map((item) => (
              <div>
                <Link to={`/home/projects/${item.id}`} className="">
                  <div className="p-4 border rounded-lg hover:bg-muted/40 flex items-center gap-4 shadow-sm">
                    <div className="w-full">
                      <p className="font-semibold text-xs text-primary">
                        {item.code}
                      </p>

                      <p className="font-medium">{item.name}</p>

                      {/* <p className="text-sm text-muted-foreground line-clamp-2">
                        {item.description}
                      </p> */}

                      <Badge
                        variant={
                          item.status === ProjectStatus.Done ||
                          item.status === ProjectStatus.Closed
                            ? "default"
                            : "secondary"
                        }
                        className="w-full col-span-2 justify-center mt-2"
                      >
                        {ProjectStatus[item.status]}
                      </Badge>
                    </div>
                  </div>
                </Link>
              </div>
            ))}

            {dashboard.recentProjects.length === 0 && (
              <div className="text-sm text-muted-foreground">No results</div>
            )}
          </CardContent>
        </Card>

        <Card className="sm:col-span-6 h-fit">
          <CardHeader className="pb-2">
            <CardTitle className="text-sm flex items-center justify-between">
              Recent products
            </CardTitle>
          </CardHeader>
          <CardContent className="space-y-4">
            {dashboard.recentProducts.map((item) => (
              <div>
                <Link to={`/home/projects/${item.projectId}`}>
                  <div className="p-4 border rounded-lg hover:bg-muted/40 flex items-center gap-4 shadow-sm">
                    {item.status === DeliverableProductStatus.Rejected && (
                      <XCircle className="size-4 text-destructive shrink-0" />
                    )}

                    {item.status === DeliverableProductStatus.Accepted && (
                      <CircleCheck className="size-4 text-primary shrink-0" />
                    )}

                    {item.status === DeliverableProductStatus.Checking && (
                      <CircleDot className="size-4 text-muted-foreground shrink-0" />
                    )}

                    <div className="grow">
                      <p className="font-semibold text-xs text-primary line-clamp-1">
                        {item.projectName}
                      </p>

                      <p className="font-medium">{item.name}</p>
                    </div>

                    <ChevronRight className="shrink-0 size-4 opacity-50" />
                  </div>
                </Link>
              </div>
            ))}

            {dashboard.recentProducts.length === 0 && (
              <div className="text-sm text-muted-foreground">No results</div>
            )}
          </CardContent>
        </Card>
      </div>
    );
  } else {
    return <IsLoading className="w-full" />;
  }
};

export default Dashboard;
