import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import {
  GET_PROJECTS_BY_FILTERS,
  GET_PROJECT_CATEGORIES_BY_FILTER,
  GET_PROJECT_CATEGORIES_ID_BY_SKILLS_NAME,
} from "@/lib/api";
import { PaginationData, Project, ProjectCategory, SkillGroup } from "@/types";
import { CircleDollarSign, Filter, Plus } from "lucide-react";
import { useEffect, useState } from "react";
import useQuery from "@/hooks/useQuery";
import useDebounce from "@/hooks/useDebounce";
import {
  ProjectStatus,
  ProjectVisibility,
  initialPagination,
} from "@/types/initialSeed";
import DataTablePagination from "@/components/DataTablePagination";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import {
  Drawer,
  DrawerContent,
  DrawerFooter,
  DrawerTrigger,
} from "@/components/ui/drawer";
import { Badge } from "@/components/ui/badge";
import { formatNumberToDecimal } from "@/lib/utils";
import ProjectFilter from "./components/ProjectFilter";
import { useNavigate } from "react-router-dom";
import PrivateComponent from "@/components/PrivateComponent";
import { Separator } from "@/components/ui/separator";
import useAuth from "@/hooks/useAuth";
import ProjectItem from "./components/ProjectItem";
import IsLoading from "@/components/IsLoading";

const Projects = () => {
  const query = useQuery();
  const debouncedQuery = useDebounce(query.getQuery().toString(), 500);
  const [paginationData, setPaginationData] =
    useState<PaginationData>(initialPagination);
  const [projects, setProjects] = useState<Project[]>([]);
  const [projectCategories, setProjectCategories] = useState<ProjectCategory[]>(
    []
  );
  const navigate = useNavigate();
  const auth = useAuth();
  const [skills, setSkills] = useState("");
  const [isLoading, setIsLoading] = useState(true); // State for loading indicator

  useEffect(() => {
    const fetch = async () => {
      const sessionSkills: SkillGroup[] = JSON.parse(
        sessionStorage.getItem("skills") ?? ""
      );

      let skillList: string[] = [];
      if (sessionSkills) {
        sessionSkills.forEach((item) => {
          skillList.push(item.skillType);
        });
      }

      const response = await GET_PROJECT_CATEGORIES_ID_BY_SKILLS_NAME(
        skillList
      );
      if (response.status) {
        let skillQuery = "";
        response.data.data.forEach((item: any) => {
          skillQuery = skillQuery + "&project-category-id=" + item.id;
        });
        setSkills(skillQuery);
      }
    };

    if (auth.user?.role === "Freelancer") {
      fetch();
    }
  }, []);

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_PROJECTS_BY_FILTERS(
        debouncedQuery +
          skills +
          (auth.user?.role === "Freelancer"
            ? "&visibility=" +
              ProjectVisibility.Public +
              "&status=" +
              ProjectStatus.Pending
            : "")
      );
      if (response.status) {
        setProjects(response.data);
        setPaginationData(
          JSON.parse(response.headers["xPagination"]) as PaginationData
        );
      }
      setIsLoading(false); // Update loading state here when fetch is complete
    };

    setIsLoading(true); // Set loading state before fetching
    if (
      (auth.user?.role === "Freelancer" && skills.length !== 0) ||
      auth.user?.role !== "Freelancer"
    ) {
      fetch();
    }
  }, [debouncedQuery, skills]);

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_PROJECT_CATEGORIES_BY_FILTER();
      if (response.status) {
        setProjectCategories(response.data);
      }
    };
    fetch();
  }, []);

  return (
    <main className="p-6 space-y-6">
      <section className="flex sm:flex-row flex-col gap-2">
        <div className="flex gap-2">
          <Input
            placeholder="Search"
            onChange={(e) => {
              query.setQuery("search", e.target.value);
            }}
            value={query.getQueryValue("search")}
            className="bg-background w-full sm:w-96"
          />

          <Popover>
            <PopoverTrigger className="hidden sm:block">
              <Button
                variant="outline"
                disabled={projectCategories.length === 0}
                className="gap-2"
              >
                <Filter className="size-4" />
                Filter
              </Button>
            </PopoverTrigger>
            <PopoverContent>
              <ProjectFilter projectCategories={projectCategories} />
            </PopoverContent>
          </Popover>

          <Drawer>
            <DrawerTrigger>
              <Button
                variant="outline"
                size="icon"
                disabled={projectCategories.length === 0}
                className="flex sm:hidden"
              >
                <Filter className="size-4" />
              </Button>
            </DrawerTrigger>
            <DrawerContent>
              <DrawerFooter>
                <ProjectFilter projectCategories={projectCategories} />
              </DrawerFooter>
            </DrawerContent>
          </Drawer>
        </div>

        <PrivateComponent isAllowed={["Administrator", "Staff"]}>
          <Button
            className="gap-2 w-full sm:ml-auto sm:w-fit"
            onClick={() => navigate("create-project")}
          >
            <Plus className="size-4" />
            Add
          </Button>
        </PrivateComponent>
      </section>

      <Card>
        <CardHeader>
          <CardTitle>Projects</CardTitle>
          <CardDescription>View all active projects here</CardDescription>
        </CardHeader>
        <CardContent className="space-y-4">
          {projects.map((item) => (
            <ProjectItem project={item} />
            // <div
            //   onClick={() => navigate(item.id)}
            //   className="p-4 border rounded-lg hover:shadow transition cursor-pointer"
            // >
            //   <p className="font-medium">
            //     {item.code} - {item.name}
            //   </p>
            //   <p className="text-sm text-muted-foreground line-clamp-4 sm:line-clamp-2">
            //     {item.description}
            //   </p>
            //   <div className="flex items-center gap-2 mt-2 flex-col sm:flex-row">
            //     <Badge>{item.projectCategoryName}</Badge>
            //     <p className="text-xs font-medium">
            //       {item.duration}-day delivery
            //     </p>

            //     <Badge variant="outline" className="gap-1">
            //       <CircleDollarSign className="size-3" />
            //       {formatNumberToDecimal(item.price, 0)}
            //     </Badge>

            //     <PrivateComponent isAllowed={["Administrator", "Staff"]}>
            //       <Badge variant="secondary" className="w-fit sm:ml-auto gap-2">
            //         {ProjectVisibility[item.visibility]}
            //         <Separator orientation="vertical" className="h-3" />
            //         {ProjectStatus[item.status]}
            //       </Badge>
            //     </PrivateComponent>
            //   </div>
            // </div>
          ))}

          {projects.length === 0 && !isLoading && (
            <div className="text-sm text-muted-foreground">No results</div>
          )}

          {isLoading && <IsLoading />}
        </CardContent>
        <CardFooter>
          <DataTablePagination paginationData={paginationData} />
        </CardFooter>
      </Card>
    </main>
  );
};

export default Projects;
