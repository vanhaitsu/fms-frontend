import { Button } from "@/components/ui/button";
import { toast } from "@/components/ui/use-toast";
import { useUpdateProjectApplyStore } from "@/config/store";
import useAuth from "@/hooks/useAuth";
import {
  CREATE_PROJECT_APPLY,
  DELETE_PROJECT_APPLY,
  GET_PROJECT_APPLY_BY_FILTER,
  UPDATE_PROJECT_APPLY,
} from "@/lib/api";
import { Project, ProjectApply } from "@/types";
import { ProjectApplyStatus, ProjectStatus } from "@/types/initialSeed";
import { format } from "date-fns";
import { CircleDot, XCircle } from "lucide-react";
import { FC, useEffect, useState } from "react";
import Countdown from "./Coundown";
import { Separator } from "@/components/ui/separator";
import { useNavigate } from "react-router-dom";

interface ProjectApplyFreelancerSideProps {
  project: Project;
  approved: boolean;
  setApproved: (e: boolean) => void;
  setIsEnded: (e: boolean) => void;
}

const ProjectApplyFreelancerSide: FC<ProjectApplyFreelancerSideProps> = ({
  project,
  approved,
  setApproved,
  setIsEnded,
}) => {
  const [applies, setApplies] = useState<ProjectApply[]>([]);
  const auth = useAuth();
  const navigate = useNavigate();

  const apply = async () => {
    try {
      const response = await CREATE_PROJECT_APPLY({
        projectId: project.id,
        freelancerId: auth.user!.userId,
      });
      if (response.status) {
        toast({
          description: response.data.message,
        });
        fetch();
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    }
  };

  const updateApplyStatus = async (id: string, status: ProjectApplyStatus) => {
    const response = await UPDATE_PROJECT_APPLY(id, status);
    if (response.status) {
      fetch();
    }
  };

  const deleteApply = async (id: string, redirect?: boolean) => {
    const response = await DELETE_PROJECT_APPLY(id);
    if (response.status) {
      if (redirect) {
        navigate("/home/projects");
      } else {
        fetch();
      }
    }
  };

  const fetch = async (query?: string) => {
    const response = await GET_PROJECT_APPLY_BY_FILTER(
      `project-id=${project.id}&${query}&freelancer-id=${auth.user!.userId}`
    );
    if (response.status) {
      await setApplies(response.data);
      if (
        response.data.length !== 0 &&
        response.data[0].freelancerId === auth.user!.userId &&
        response.data[0].status === ProjectApplyStatus.Accepted
      ) {
        setApproved(true);

        if (response.data[0].endDate) {
          setIsEnded(new Date(response.data[0].endDate) < new Date());
        }
      }
    }
  };

  useEffect(() => {
    fetch();
  }, []);

  return (
    <>
      {applies.length !== 0 && applies[0].freelancerId === auth.user!.userId ? (
        <div className="space-y-2">
          {applies[0].status === ProjectApplyStatus.Rejected && (
            <>
              <div className="flex gap-2 items-center text-destructive ">
                <XCircle className="size-4" />
                <p className="font-medium">Rejected</p>
              </div>
              <p>
                This application has been rejected. If you think this is a
                mistake? Try applying again.
              </p>
              <Button
                variant="outline"
                className="w-full"
                onClick={() =>
                  updateApplyStatus(applies[0].id, ProjectApplyStatus.Checking)
                }
              >
                Apply this Project
              </Button>
            </>
          )}

          {applies[0].status === ProjectApplyStatus.Checking && (
            <>
              <div className="flex gap-2 items-center">
                <CircleDot className="size-4 " />
                <p className="font-medium">Checking</p>
              </div>
              <p>Your application is being reviewed by the project manager</p>

              <Button
                variant="outline"
                className="w-full"
                onClick={() => deleteApply(applies[0].id)}
              >
                Cancel this Application
              </Button>
            </>
          )}

          {applies[0].status === ProjectApplyStatus.Invited && (
            <>
              {/* <div className="flex gap-2 items-center text-muted-foreground">
                <CircleDot className="size-4 " />
                <p className="font-medium">Invited</p>
              </div> */}
              <p>You have been invited to join this project.</p>

              <div className="flex space-x-2">
                <Button
                  variant="outline"
                  className="w-full"
                  onClick={() => deleteApply(applies[0].id, true)}
                >
                  Reject
                </Button>
                <Button
                  className="w-full"
                  onClick={() =>
                    updateApplyStatus(
                      applies[0].id,
                      ProjectApplyStatus.Accepted
                    )
                  }
                >
                  Approve
                </Button>
              </div>
            </>
          )}

          {applies[0].status === ProjectApplyStatus.Accepted && (
            <div className="space-y-4">
              {project.status === ProjectStatus.Closed ||
              project.status === ProjectStatus.Done ? (
                <p>This project is closed</p>
              ) : (
                <Countdown dateTime={applies[0].endDate!} />
              )}

              <Separator />

              <div className="space-y-2">
                <p>
                  Freelancer:{" "}
                  <span className="font-medium">
                    {applies[0].freelancerFirstName}{" "}
                    {applies[0].freelancerLastName}
                  </span>{" "}
                  (you)
                </p>
                <p>
                  Due date:{" "}
                  <span className="font-medium">
                    {format(new Date(applies[0].endDate!), "dd/MM/yyyy")}
                  </span>
                </p>
              </div>
            </div>
          )}
        </div>
      ) : (
        <Button className="w-full" onClick={() => apply()}>
          Apply this Project
        </Button>
      )}
    </>
  );
};

export default ProjectApplyFreelancerSide;
