import { FC, useState } from "react";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import { Button } from "@/components/ui/button";
import InputDate from "@/components/InputDate";
import { Label } from "@/components/ui/label";
import {
  useUpdateProfileStore,
  useUpdateProjectApplyStore,
} from "@/config/store";
import { UPDATE_PROJECT_APPLY } from "@/lib/api";

interface UpdateDeadlinePopoverProps {
  id: string;
  endDate: Date;
}

const UpdateDeadlinePopover: FC<UpdateDeadlinePopoverProps> = ({
  id,
  endDate,
}) => {
  const [date, setDate] = useState<Date>(new Date(endDate));
  const update = useUpdateProjectApplyStore();
  const updateProject = useUpdateProfileStore();

  const updateEndDate = async () => {
    console.log(date);
    const response = await UPDATE_PROJECT_APPLY(id, undefined, date);
    if (response.status) {
      update.toggleStatus();
      updateProject.toggleStatus();
    }
  };

  const addDays = (number: number) => {
    const newDate = new Date(date); // Clone the current date
    newDate.setDate(newDate.getDate() + number); // Add days
    setDate(newDate); // Update the state with the new date
  };

  return (
    <Popover>
      <PopoverTrigger
        onClick={() => setDate(new Date(endDate))}
        className="text-sm hover:underline text-primary font-medium"
      >
        Change
      </PopoverTrigger>
      <PopoverContent className="space-y-2 w-fit">
        <Label>Change end date</Label>
        <InputDate
          // showCalendar={false}
          date={date}
          setDate={setDate}
          startDate={new Date()}
        />
        <div className="space-x-2">
          <Button size="icon" variant="secondary" onClick={() => addDays(1)}>
            +1
          </Button>
          <Button size="icon" variant="secondary" onClick={() => addDays(2)}>
            +2
          </Button>
          <Button size="icon" variant="secondary" onClick={() => addDays(3)}>
            +3
          </Button>
          <Button size="icon" variant="secondary" onClick={() => addDays(7)}>
            +7
          </Button>
          <Button size="icon" variant="secondary" onClick={() => addDays(10)}>
            +10
          </Button>
        </div>
        <Button className="w-full" onClick={() => updateEndDate()}>
          Save
        </Button>
      </PopoverContent>
    </Popover>
  );
};

export default UpdateDeadlinePopover;
