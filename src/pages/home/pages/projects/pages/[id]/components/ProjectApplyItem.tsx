import { Button } from "@/components/ui/button";
import { DELETE_PROJECT_APPLY, GET_PROJECT_APPLY_BY_FILTER } from "@/lib/api";
import { Project, ProjectApply } from "@/types";
import {
  ProjectApplyStatus,
  ProjectStatus,
  ProjectVisibility,
} from "@/types/initialSeed";
import { FC, useEffect, useState } from "react";
import ProjectApplyDialog from "./ProjectApplyDialog";
import Countdown from "./Coundown";
import { format } from "date-fns";
import PrivateComponent from "@/components/PrivateComponent";
import { CalendarClock } from "lucide-react";
import UpdateDeadlinePopover from "./UpdateDeadlinePopover";
import { useUpdateProjectApplyStore } from "@/config/store";
import { Separator } from "@/components/ui/separator";

interface ProjectApplyItemProps {
  project: Project;
}

const ProjectApplyItem: FC<ProjectApplyItemProps> = ({ project }) => {
  const [applies, setApplies] = useState<ProjectApply[]>([]);
  const update = useUpdateProjectApplyStore();

  const fetch = async (query?: string) => {
    const response = await GET_PROJECT_APPLY_BY_FILTER(
      `project-id=${project.id}&${query}`
    );
    if (response.status) {
      setApplies(response.data);
    }
  };

  const deleteApply = async (id: string) => {
    const response = await DELETE_PROJECT_APPLY(id);
    if (response.status) {
      fetch();
    }
  };

  useEffect(() => {
    fetch(`status=${ProjectApplyStatus.Accepted}`);
  }, [update.status]);

  return (
    <div>
      {(applies.length !== 0 && applies[0].status) ===
      ProjectApplyStatus.Accepted ? (
        <div className="space-y-4">
          {project.status === ProjectStatus.Closed ||
          project.status === ProjectStatus.Done ? (
            <p className="text-muted-foreground">This project is closed</p>
          ) : (
            <Countdown dateTime={applies[0].endDate!} />
          )}

          <Separator />

          <div className="space-y-2">
            <p>
              Freelancer:{" "}
              <span className="font-medium">
                {applies[0].freelancerFirstName} {applies[0].freelancerLastName}
              </span>
            </p>
            <p>
              Due date:{" "}
              <span className="font-medium">
                {format(new Date(applies[0].endDate!), "dd/MM/yyyy")}
              </span>{" "}
              {applies[0].project?.status !== ProjectStatus.Done &&
                applies[0].project?.status !== ProjectStatus.Closed && (
                  <UpdateDeadlinePopover
                    id={applies[0]!.id}
                    endDate={
                      applies[0].endDate! > new Date()
                        ? new Date()
                        : applies[0].endDate!
                    }
                  />
                )}
            </p>
            {new Date(applies[0].endDate!) < new Date() &&
              applies[0].project?.status !== ProjectStatus.Done &&
              applies[0].project?.status !== ProjectStatus.Closed && (
                <Button
                  className="w-full"
                  variant="destructive"
                  onClick={() => deleteApply(applies[0].id)}
                >
                  Remove this Freelancer
                </Button>
              )}
          </div>
        </div>
      ) : (
        <PrivateComponent isAllowed={["Administrator", "Staff"]}>
          <ProjectApplyDialog project={project} />
        </PrivateComponent>
      )}
    </div>
  );
};

export default ProjectApplyItem;
