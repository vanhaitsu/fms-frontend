import { useUpdateProjectDeliverableStore } from "@/config/store";
import {
  CREATE_DELIVERABLE_PRODUCT,
  DELETE_DELIVERABLE_PRODUCT,
  DELETE_PROJECT_DELIVERABLE,
  GET_DELIVERABLE_PROJECT_BY_FILTER,
  UPDATE_DELIVERABLE_PROJECT,
} from "@/lib/api";
import { DeliverableProduct, Project, ProjectDeliverable } from "@/types";
import {
  DeliverableProductStatus,
  ProjectDeliverableStatus,
  ProjectStatus,
} from "@/types/initialSeed";
import { ChangeEvent, FC, useEffect, useRef, useState } from "react";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Button } from "@/components/ui/button";
import {
  Check,
  CircleCheck,
  CircleDot,
  Download,
  Trash,
  X,
  XCircle,
} from "lucide-react";
import { Textarea } from "@/components/ui/textarea";
import PrivateComponent from "@/components/PrivateComponent";
import {
  ref,
  uploadBytesResumable,
  getDownloadURL,
  deleteObject,
} from "firebase/storage";
import { storage } from "@/App";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import { PopoverClose } from "@radix-ui/react-popover";
import IsLoading from "@/components/IsLoading";

interface ProjectDeliverableItemProps {
  projectDeliverable: ProjectDeliverable;
  project: Project;
  approved: boolean;
  isEnded: boolean;
}

const ProjectDeliverableItem: FC<ProjectDeliverableItemProps> = ({
  projectDeliverable,
  project,
  approved,
  isEnded,
}) => {
  const updateProjectDeliverable = useUpdateProjectDeliverableStore();
  const [products, setProducts] = useState<DeliverableProduct[]>([]);
  const [openState, setOpenState] = useState(false);
  const [feedback, setFeedback] = useState<string | undefined>(undefined);
  const [isLoading, setIsLoading] = useState(false);
  const [percent, setPercent] = useState(0);
  const fileInputRef = useRef<any>();
  const [openStateDelete, setOpenStateDelete] = useState(false);
  const [firstLoading, setFirstLoading] = useState(true);

  const getProducts = async () => {
    setIsLoading(true);
    const response = await GET_DELIVERABLE_PROJECT_BY_FILTER(
      "project-deliverable-id=" + projectDeliverable.id
    );
    if (response.status) {
      await setProducts(response.data);
      if (products) {
        setOpenState(true);
      }
    }
    setIsLoading(false);
    setFirstLoading(false);
  };

  const deleteDeliverable = async (id: string) => {
    setIsLoading(true);
    const response = await DELETE_PROJECT_DELIVERABLE(id);
    if (response.status) {
      updateProjectDeliverable.toggleStatus();
    }
    setIsLoading(false);
  };

  useEffect(() => {
    if (openState) {
      getProducts();
    }
  }, [updateProjectDeliverable.status]);

  const updateDeliverable = async (
    id: string,
    status: DeliverableProductStatus,
    feedback?: string
  ) => {
    const response = await UPDATE_DELIVERABLE_PROJECT(id, {
      status: status,
      feedback: feedback,
    });
    if (response.status) {
      updateProjectDeliverable.toggleStatus();
    }
  };

  const handleFile = async (e: ChangeEvent<HTMLInputElement>) => {
    setIsLoading(true);
    e.preventDefault();
    const file = e.target.files && e.target.files[0];
    if (!file) return;

    const storageRef = ref(
      storage,
      `/deliverable-product/${project.code}-${file.name}`
    );

    // progress can be paused and resumed. It also exposes progress updates.
    // Receives the storage reference and the file to upload.
    const uploadTask = uploadBytesResumable(storageRef, file);

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const percent = Math.round(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );

        // update progress
        setPercent(percent);
      },
      (err) => console.log(err),
      () => {
        // download url
        getDownloadURL(uploadTask.snapshot.ref).then((url) => {
          createProduct(url, file.name);
        });
      }
    );
  };

  const createProduct = async (url: string, name: string) => {
    const response = await CREATE_DELIVERABLE_PRODUCT({
      uRL: url,
      projectId: project.id,
      projectDeliverableId: projectDeliverable.id,
      name: name,
    });
    if (response.status) {
      getProducts();
    }
  };

  const deleteProduct = async (id: string) => {
    const response = await DELETE_DELIVERABLE_PRODUCT(id);
    if (response.status) {
      getProducts();
    }
  };

  const removeFile = async (item: DeliverableProduct) => {
    if (!item.uRL) {
      return; // Ensure URL exists before attempting to delete
    }

    setIsLoading(true);
    // Extract the filename from the URL or use item.name
    const filename = project.code + "-" + item.name; // Assuming `name` property holds the filename

    // Create a reference to the file
    const fileRef = ref(storage, `/deliverable-product/${filename}`);

    try {
      // Delete the file
      await deleteObject(fileRef);
      console.log("File deleted successfully");
      // Optionally, update the UI or perform any cleanup
      deleteProduct(item.id);
    } catch (error) {
      console.error("Error deleting file:", error);
      // Handle error as needed (e.g., show a message to the user)
    }
  };

  return (
    <>
      <Dialog open={openState && approved} onOpenChange={setOpenState}>
        <DialogTrigger asChild onClick={() => getProducts()}>
          <div className="flex gap-2 items-center hover:underline cursor-pointer">
            {projectDeliverable.status === ProjectDeliverableStatus.Accepted &&
              approved && <CircleCheck className="size-4 text-primary" />}

            {projectDeliverable.status === ProjectDeliverableStatus.Checking &&
              approved && (
                <CircleDot className="size-4 text-muted-foreground" />
              )}

            {projectDeliverable.status === ProjectDeliverableStatus.Rejected &&
              approved && <XCircle className="size-4 text-destructive" />}

            <p className="font-medium">{projectDeliverable.name}</p>
            <p className="text-muted-foreground">
              ({projectDeliverable.deliverableTypeName})
            </p>
          </div>
        </DialogTrigger>
        <DialogContent>
          <DialogHeader>
            <DialogTitle>{projectDeliverable.name}</DialogTitle>
          </DialogHeader>

          <div className="space-y-2">
            {products.length === 0 && !firstLoading && (
              <div className="text-sm text-muted-foreground">No results</div>
            )}
            {products.map((item) => (
              <div className="flex gap-2 items-center">
                {item.status === DeliverableProductStatus.Rejected && (
                  <XCircle className="size-4 text-destructive shrink-0" />
                )}

                {item.status === DeliverableProductStatus.Accepted && (
                  <CircleCheck className="size-4 text-primary shrink-0" />
                )}

                {item.status === DeliverableProductStatus.Checking && (
                  <CircleDot className="size-4 text-muted-foreground shrink-0" />
                )}

                <div className="w-full">
                  <p className="text-sm font-medium">{item.name}</p>
                  <Dialog>
                    <DialogTrigger
                      onClick={() => setFeedback(item.feedback)}
                      asChild
                    >
                      <p className="text-xs font-medium text-muted-foreground cursor-pointer hover:underline">
                        Feedback
                      </p>
                    </DialogTrigger>

                    <DialogContent>
                      <DialogHeader>
                        <DialogTitle>Feedback</DialogTitle>
                        <DialogDescription>{item.name}</DialogDescription>
                      </DialogHeader>

                      <PrivateComponent isAllowed={["Administrator", "Staff"]}>
                        <Textarea
                          value={feedback}
                          rows={5}
                          onChange={(e) => setFeedback(e.target.value)}
                          placeholder="What needs to be improved?"
                        />
                      </PrivateComponent>

                      <PrivateComponent isAllowed={["Freelancer"]}>
                        <p className="text-sm text-muted-foreground">
                          {feedback ?? "No feedback"}
                        </p>
                      </PrivateComponent>

                      <DialogFooter>
                        <DialogClose asChild disabled={isLoading}>
                          <Button variant="outline">Cancel</Button>
                        </DialogClose>
                        <PrivateComponent
                          isAllowed={["Administrator", "Staff"]}
                        >
                          <Button
                            disabled={isLoading}
                            onClick={() =>
                              updateDeliverable(item.id, item.status, feedback)
                            }
                          >
                            Save
                          </Button>
                        </PrivateComponent>
                      </DialogFooter>
                    </DialogContent>
                  </Dialog>
                </div>

                {project.status !== ProjectStatus.Closed &&
                  project.status !== ProjectStatus.Done &&
                  item.status !== DeliverableProductStatus.Accepted && (
                    <PrivateComponent isAllowed={["Freelancer"]}>
                      <Button
                        variant="outline"
                        size="icon"
                        className="shrink-0"
                        onClick={() => {
                          removeFile(item);
                        }}
                      >
                        <Trash className="size-4" />
                      </Button>
                    </PrivateComponent>
                  )}

                <PrivateComponent isAllowed={["Administrator", "Staff"]}>
                  {item.status === DeliverableProductStatus.Checking && (
                    <>
                      <Button
                        disabled={isLoading}
                        size="icon"
                        variant="outline"
                        className="shrink-0"
                        onClick={() =>
                          updateDeliverable(
                            item.id,
                            DeliverableProductStatus.Accepted
                          )
                        }
                      >
                        <Check className="size-4 text-primary" />
                      </Button>

                      <Button
                        disabled={isLoading}
                        size="icon"
                        variant="outline"
                        className="shrink-0"
                        onClick={() =>
                          updateDeliverable(
                            item.id,
                            DeliverableProductStatus.Rejected
                          )
                        }
                      >
                        <X className="size-4 text-destructive" />
                      </Button>
                    </>
                  )}
                </PrivateComponent>

                <Button
                  disabled={isLoading}
                  size="icon"
                  variant="outline"
                  className="shrink-0"
                >
                  <a href={item.uRL} target="_blank">
                    <Download className="size-4" />
                  </a>
                </Button>
              </div>
            ))}

            {firstLoading && <IsLoading />}
          </div>

          {project.status !== ProjectStatus.Closed &&
            project.status !== ProjectStatus.Done && (
              <DialogFooter>
                <PrivateComponent isAllowed={["Administrator", "Staff"]}>
                  <Popover>
                    <PopoverTrigger asChild>
                      <Button
                        disabled={isLoading}
                        variant="outline"
                        // onClick={() => setOpenStateDelete(true)}
                      >
                        Delete
                      </Button>
                    </PopoverTrigger>
                    <PopoverContent>
                      <p className="text-sm font-medium">Are your sure?</p>
                      <p className="text-sm text-muted-foreground">
                        This action cannot be undone.
                      </p>
                      <div className="mt-2 grid grid-cols-2 gap-2">
                        <PopoverClose asChild>
                          <Button
                            variant="outline"
                            className="w-full"
                            size="sm"
                          >
                            Cancel
                          </Button>
                        </PopoverClose>

                        <Button
                          variant="destructive"
                          onClick={() =>
                            deleteDeliverable(projectDeliverable.id)
                          }
                          className="w-full"
                          size="sm"
                        >
                          Continue
                        </Button>
                      </div>
                    </PopoverContent>
                  </Popover>
                </PrivateComponent>

                {!isEnded && (
                  <PrivateComponent isAllowed={["Freelancer"]}>
                    <input
                      type="file"
                      accept={projectDeliverable.deliverableTypeName}
                      ref={fileInputRef}
                      onChange={handleFile}
                      hidden
                    />
                    <Button
                      onClick={() => {
                        fileInputRef.current.value = "";
                        fileInputRef.current.click();
                      }}
                      disabled={isLoading}
                    >
                      Upload
                    </Button>
                  </PrivateComponent>
                )}
              </DialogFooter>
            )}
        </DialogContent>
      </Dialog>

      {/* <AlertDialog open={openStateDelete} onOpenChange={setOpenStateDelete}>
        <AlertDialogContent>
          <AlertDialogHeader>
            <AlertDialogTitle>Are you sure?</AlertDialogTitle>
            <AlertDialogDescription>
              This action cannot be undone.
            </AlertDialogDescription>
          </AlertDialogHeader>
          <AlertDialogFooter>
            <AlertDialogCancel disabled={isLoading}>Cancel</AlertDialogCancel>
            <Button
              variant="destructive"
              disabled={isLoading}
              onClick={() => deleteDeliverable(projectDeliverable.id)}
            >
              Continue
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialog> */}
    </>
  );
};

export default ProjectDeliverableItem;
