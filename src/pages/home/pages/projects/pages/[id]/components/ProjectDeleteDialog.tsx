import { FC, useState } from "react";
import { Button } from "@/components/ui/button";
import {
  AlertDialog,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
} from "@/components/ui/alert-dialog";
import { DELETE_PROJECT } from "@/lib/api";
import { toast } from "@/components/ui/use-toast";
import { useNavigate } from "react-router-dom";

interface ProjectDeleteDialogProps {
  openState: boolean;
  setOpenState: (e: boolean) => void;
  id: string;
}

const ProjectDeleteDialog: FC<ProjectDeleteDialogProps> = ({
  openState,
  setOpenState,
  id,
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const navigate = useNavigate();

  const fetch = async () => {
    try {
      setIsLoading(true);
      const response = await DELETE_PROJECT(id);
      if (response.status) {
        toast({
          description: response.data.message,
        });
        setOpenState(false);
        navigate("/home/projects");
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <AlertDialog open={openState} onOpenChange={setOpenState}>
      <AlertDialogContent>
        <AlertDialogHeader>
          <AlertDialogTitle>Are you sure?</AlertDialogTitle>
          <AlertDialogDescription>
            This action cannot be undone.
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter>
          <AlertDialogCancel disabled={isLoading}>Cancel</AlertDialogCancel>
          <Button
            variant="destructive"
            disabled={isLoading}
            onClick={() => fetch()}
          >
            Continue
          </Button>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
};

export default ProjectDeleteDialog;
