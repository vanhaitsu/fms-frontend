import { useCountdown } from "@/hooks/useCountdown";
import { FC } from "react";

interface CountdownProps {
  dateTime: Date;
}

const Countdown: FC<CountdownProps> = ({ dateTime }) => {
  const [days, hours, minutes, seconds] = useCountdown(new Date(dateTime));

  if (days + hours + minutes + seconds <= 0) {
    return (
      <p className="text-sm font-medium text-muted-foreground">
        The project's due date has ended
      </p>
    );
  } else {
    return (
      <div className="flex gap-2">
        <DateTimeDisplay title="Days" value={days} />
        <DateTimeDisplay title="Hours" value={hours} />
        <DateTimeDisplay title="Minutes" value={minutes} />
        <DateTimeDisplay title="Seconds" value={seconds} />
      </div>
    );
  }
};

export default Countdown;

interface DateTimeDisplayProps {
  title: string;
  value: number;
}

const DateTimeDisplay: FC<DateTimeDisplayProps> = ({ title, value }) => {
  return (
    <div className="border aspect-square w-full rounded-md flex flex-col items-center justify-center bg-muted/40">
      <p className="text-xl">{value}</p>
      <p className="text-[8px] uppercase font-semibold text-muted-foreground">
        {title}
      </p>
    </div>
  );
};
