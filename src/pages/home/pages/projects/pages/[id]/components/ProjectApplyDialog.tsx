import { Freelancer, PaginationData, Project, ProjectApply } from "@/types";
import { FC, useEffect, useState } from "react";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Button } from "@/components/ui/button";
import {
  ProjectApplyStatus,
  ProjectVisibility,
  initialPagination,
} from "@/types/initialSeed";
import {
  CREATE_PROJECT_APPLY,
  GET_FREELANCERS_BY_FILTERS,
  GET_PROJECT_APPLY_BY_FILTER,
  UPDATE_PROJECT_APPLY,
} from "@/lib/api";
import { useUpdateFreelancerStore } from "@/config/store";
import useQuery from "@/hooks/useQuery";
import useDebounce from "@/hooks/useDebounce";
import { toast } from "@/components/ui/use-toast";
import FreelancerCommandItem from "../../create-project/components/FreelancerCommandItem";
import DataTablePagination from "@/components/DataTablePagination";
import { Check, X } from "lucide-react";
import { Badge } from "@/components/ui/badge";

interface ProjectApplyDialogProps {
  project: Project;
}

const ProjectApplyDialog: FC<ProjectApplyDialogProps> = ({ project }) => {
  const updateFreelancer = useUpdateFreelancerStore();
  const query = useQuery();
  const debouncedQuery = useDebounce(
    query.getQuery().toString() +
      "&skill-type=" +
      encodeURIComponent(project.projectCategoryName ?? "") +
      "&projectId=" +
      project.id,
    500
  );
  const [paginationData, setPaginationData] =
    useState<PaginationData>(initialPagination);
  const [isInviting, setIsInviting] = useState(false);
  const [freelancers, setFreelancers] = useState<Freelancer[] | ProjectApply[]>(
    []
  );
  const fetch = async () => {
    let response;
    if (project.visibility === ProjectVisibility.Private) {
      response = await GET_FREELANCERS_BY_FILTERS(debouncedQuery);
    } else {
      response = await GET_PROJECT_APPLY_BY_FILTER(
        `${debouncedQuery}&project-id=${project.id}`
      );
    }
    if (response.status) {
      setFreelancers(response.data);
      setIsInviting(false);
      setPaginationData(
        JSON.parse(response.headers["xPagination"]) as PaginationData
      );
    }
  };

  useEffect(() => {
    fetch();
  }, [debouncedQuery, updateFreelancer.status]);

  const updateApplyStatus = async (id: string, status: ProjectApplyStatus) => {
    const response = await UPDATE_PROJECT_APPLY(id, status);
    if (response.status) {
      if (status === ProjectApplyStatus.Accepted) {
        location.reload();
      } else {
        fetch();
      }
    }
  };

  const invite = async (id: string) => {
    try {
      setIsInviting(true);
      const response = await CREATE_PROJECT_APPLY({
        projectId: project.id,
        freelancerId: id,
      });
      if (response.status) {
        toast({
          description: response.data.message,
        });
        updateFreelancer.toggleStatus();
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    }
  };

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button variant="outline" className="w-full">
          {project.visibility === ProjectVisibility.Private &&
            "Invite freelancers"}

          {project.visibility === ProjectVisibility.Public &&
            "See applied freelancers"}
        </Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Freelancer</DialogTitle>
        </DialogHeader>
        {freelancers.map((item) => (
          <div className="flex items-center gap-2 justify-between">
            <FreelancerCommandItem
              freelancer={
                project.visibility === ProjectVisibility.Private
                  ? (item! as Freelancer)
                  : undefined
              }
              projectApply={
                project.visibility === ProjectVisibility.Public
                  ? (item! as ProjectApply)
                  : undefined
              }
            />
            {project.visibility === ProjectVisibility.Private &&
              ((item as Freelancer).invited ? (
                <Button size="sm" disabled variant="outline">
                  Invited
                </Button>
              ) : (
                <Button
                  variant="secondary"
                  onClick={() => invite(item.id)}
                  disabled={isInviting}
                  size="sm"
                >
                  Invite
                </Button>
              ))}
            {project.visibility === ProjectVisibility.Public && (
              <>
                {(item as ProjectApply).status ===
                  ProjectApplyStatus.Checking && (
                  <div className="space-x-2">
                    <Button
                      size="icon"
                      variant="outline"
                      className="shrink-0"
                      onClick={() =>
                        updateApplyStatus(item.id, ProjectApplyStatus.Accepted)
                      }
                    >
                      <Check className="size-4 text-primary" />
                    </Button>

                    <Button
                      size="icon"
                      variant="outline"
                      className="shrink-0"
                      onClick={() =>
                        updateApplyStatus(item.id, ProjectApplyStatus.Rejected)
                      }
                    >
                      <X className="size-4 text-destructive" />
                    </Button>
                  </div>
                )}

                {(item as ProjectApply).status ===
                  ProjectApplyStatus.Rejected && (
                  <p className="text-sm font-medium text-muted-foreground">
                    Rejected
                  </p>
                )}
              </>
            )}
          </div>
        ))}

        {freelancers.length === 0 && (
          <div className="text-sm text-muted-foreground">No results</div>
        )}

        <DialogFooter>
          <DataTablePagination paginationData={paginationData} />
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default ProjectApplyDialog;
