import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { useEffect, useState } from "react";
import { toast } from "@/components/ui/use-toast";
import { Project, ProjectCategory } from "@/types";
import {
  GET_PROJECT,
  GET_PROJECT_CATEGORIES_BY_FILTER,
  UPDATE_PROJECT,
} from "@/lib/api";
import { projectUpdateSchema } from "@/lib/form";
import { useParams } from "react-router-dom";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Textarea } from "@/components/ui/textarea";
import { useNavigate } from "react-router-dom";
import { Button } from "@/components/ui/button";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { convertEnumToList, formatNumberToDecimal } from "@/lib/utils";
import { ProjectStatus, ProjectVisibility } from "@/types/initialSeed";
import { ChevronLeft, Lightbulb } from "lucide-react";
import { Alert, AlertDescription, AlertTitle } from "@/components/ui/alert";
import IsLoading from "@/components/IsLoading";

const UpdateProject = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [projectCategories, setProjectCategories] = useState<ProjectCategory[]>(
    []
  );
  const [project, setProject] = useState<Project | null>(null);
  const { id } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_PROJECT_CATEGORIES_BY_FILTER();
      if (response.status) {
        setProjectCategories(response.data);
      }
    };
    fetch();
  }, []);

  const form = useForm<z.infer<typeof projectUpdateSchema>>({
    resolver: zodResolver(projectUpdateSchema),
  });

  useEffect(() => {
    const getProject = async () => {
      const response = await GET_PROJECT(id!);
      if (response.status) {
        setProject(response.data.data);
      }
    };
    getProject();
  }, []);

  useEffect(() => {
    if (project) {
      form.setValue("code", project.code!);
      form.setValue("name", project.name);
      form.setValue("description", project.description);
      form.setValue("price", project.price);
      form.setValue("deposit", project.deposit);
      form.setValue("projectCategoryId", project.projectCategoryId!);
      form.setValue("visibility", project.visibility!);
      form.setValue("duration", project.duration);
    }
  }, [project]);

  const onSubmit = async (values: z.infer<typeof projectUpdateSchema>) => {
    try {
      setIsLoading(true);
      const response = await UPDATE_PROJECT(id!, values);
      if (response.status) {
        toast({
          description: response.data.message,
        });
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <main className="p-6">
      <div className="lg:w-3/4 w-full mx-auto space-y-6">
        <div className="flex gap-4 items-center">
          <Button
            variant="outline"
            onClick={() => navigate(-1)}
            size="icon"
            className="shrink-0"
          >
            <ChevronLeft className="size-4" />
          </Button>

          <h3 className="w-full">Edit</h3>
        </div>

        {project ? (
          <>
            <Form {...form}>
              <form
                id="create"
                onSubmit={form.handleSubmit(onSubmit)}
                className="grid md:grid-cols-3 gap-6 "
              >
                <Card className="md:col-span-2 h-fit">
                  <CardHeader>
                    <CardTitle>Information</CardTitle>
                    <CardDescription>
                      Set up your Project information
                    </CardDescription>
                  </CardHeader>

                  <CardContent className="space-y-4">
                    <FormField
                      control={form.control}
                      name="code"
                      render={({ field }) => (
                        <FormItem className="w-full">
                          <FormLabel>Code *</FormLabel>
                          <FormControl>
                            <Input
                              disabled={isLoading}
                              value={field.value ?? ""}
                              onChange={field.onChange}
                            />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />

                    <FormField
                      control={form.control}
                      name="name"
                      render={({ field }) => (
                        <FormItem className="w-full">
                          <FormLabel>Name *</FormLabel>
                          <FormControl>
                            <Textarea
                              disabled={isLoading}
                              value={field.value ?? ""}
                              onChange={field.onChange}
                            />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />

                    <FormField
                      control={form.control}
                      name="description"
                      render={({ field }) => (
                        <FormItem className="w-full">
                          <FormLabel>Description *</FormLabel>
                          <FormControl>
                            <Textarea
                              rows={10}
                              disabled={isLoading}
                              value={field.value ?? ""}
                              onChange={field.onChange}
                            />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />
                  </CardContent>
                </Card>

                <Card className="h-fit">
                  <CardHeader>
                    <CardTitle>Details</CardTitle>
                    <CardDescription>
                      Set up your Project details
                    </CardDescription>
                  </CardHeader>

                  <CardContent className="space-y-4">
                    <FormField
                      control={form.control}
                      name="projectCategoryId"
                      render={({ field }) => (
                        <FormItem className="w-full">
                          <FormLabel>Category *</FormLabel>
                          <FormControl>
                            <Select
                              disabled={isLoading}
                              onValueChange={(value) => {
                                field.onChange(value);
                              }}
                              value={
                                field.value != undefined
                                  ? String(field.value)
                                  : ""
                              }
                            >
                              <SelectTrigger
                                disabled={projectCategories.length === 0}
                                className="w-full"
                              >
                                <SelectValue placeholder="Select" />
                              </SelectTrigger>
                              <SelectContent>
                                {projectCategories.map((item) => (
                                  <SelectItem value={item.id}>
                                    {item.name}
                                  </SelectItem>
                                ))}
                              </SelectContent>
                            </Select>
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />

                    <FormField
                      control={form.control}
                      name="visibility"
                      render={({ field }) => (
                        <FormItem className="w-full">
                          <FormLabel>Visibility *</FormLabel>
                          <FormControl>
                            <Select
                              disabled={
                                isLoading ||
                                project.status === ProjectStatus.Processing
                              }
                              onValueChange={(value) =>
                                field.onChange(Number(value))
                              }
                              value={
                                field.value != undefined
                                  ? String(field.value)
                                  : ""
                              }
                            >
                              <SelectTrigger className="w-full">
                                <SelectValue placeholder="Select" />
                              </SelectTrigger>
                              <SelectContent>
                                {convertEnumToList(ProjectVisibility).map(
                                  (item, index) => (
                                    <SelectItem value={index.toString()}>
                                      {item}
                                    </SelectItem>
                                  )
                                )}
                              </SelectContent>
                            </Select>
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />

                    <FormField
                      control={form.control}
                      name="price"
                      render={({ field }) => (
                        <FormItem className="w-full">
                          <FormLabel>Price *</FormLabel>
                          <FormControl>
                            <Input
                              type="number"
                              disabled={isLoading}
                              value={field.value ?? ""}
                              onChange={field.onChange}
                              min={project.deposit + 1}
                            />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />

                    <FormField
                      control={form.control}
                      name="deposit"
                      render={({ field }) => (
                        <FormItem className="w-full">
                          <FormLabel>Deposit *</FormLabel>
                          <FormControl>
                            <Input
                              type="number"
                              disabled={isLoading}
                              value={field.value ?? ""}
                              onChange={field.onChange}
                            />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />

                    {/* <Alert>
                      <Lightbulb className="size-4" />
                      <AlertTitle>
                        Deposit: {formatNumberToDecimal(project.deposit)}
                      </AlertTitle>
                      <AlertDescription>
                        New price must be larger than deposit.
                      </AlertDescription>
                    </Alert> */}

                    {project.status === ProjectStatus.Pending && (
                      <FormField
                        control={form.control}
                        name="duration"
                        render={({ field }) => (
                          <FormItem className="w-full">
                            <FormLabel>Duration *</FormLabel>
                            <FormControl>
                              <Input
                                type="number"
                                disabled={isLoading}
                                placeholder="Day"
                                value={field.value ?? ""}
                                onChange={field.onChange}
                              />
                            </FormControl>
                            <FormMessage />
                          </FormItem>
                        )}
                      />
                    )}
                  </CardContent>
                </Card>
              </form>
            </Form>

            <div className="flex justify-end gap-2">
              <Button
                variant="outline"
                disabled={isLoading}
                onClick={() => navigate(-1)}
              >
                Cancel
              </Button>

              <Button form="create" disabled={isLoading} type="submit">
                Save
              </Button>
            </div>
          </>
        ) : (
          <IsLoading className="w-full" />
        )}
      </div>
    </main>
  );
};

export default UpdateProject;
