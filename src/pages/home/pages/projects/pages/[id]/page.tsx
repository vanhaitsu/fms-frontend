import { Link, useLocation, useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Button } from "@/components/ui/button";
import { ChevronLeft, CircleDollarSign, MoreVertical, X } from "lucide-react";
import { useEffect, useState } from "react";
import { Project, ProjectDeliverable } from "@/types";
import {
  CLOSE_PROJECT,
  DELETE_PROJECT_DELIVERABLE,
  GET_PROJECT,
  GET_PROJECT_DELIVERABLES_BY_FILTER,
} from "@/lib/api";
import { Badge } from "@/components/ui/badge";
import { formatNumberToDecimal } from "@/lib/utils";
import ProjectDeliverableCreateDialog from "../invite-and-delivery/components/ProjectDeliverableCreateDialog";
import {
  useUpdateProfileStore,
  useUpdateProjectDeliverableStore,
} from "@/config/store";
import { ProjectStatus, ProjectVisibility } from "@/types/initialSeed";
import PrivateComponent from "@/components/PrivateComponent";
import { Separator } from "@/components/ui/separator";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { toast } from "@/components/ui/use-toast";
import ProjectDeleteDialog from "./components/ProjectDeleteDialog";
import ProjectDeliverableItem from "./components/ProjectDeliverableItem";
import ProjectApplyItem from "./components/ProjectApplyItem";
import ProjectApplyFreelancerSide from "./components/ProjectApplyFreelancerSide";
import useAuth from "@/hooks/useAuth";
import TitleItem from "../../components/TitleItem";
import {
  AlertDialog,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "@/components/ui/alert-dialog";
import IsLoading from "@/components/IsLoading";

const ProjectId = () => {
  const [project, setProject] = useState<Project | null>(null);
  const [projectDeliverables, setProjectDeliverables] = useState<
    ProjectDeliverable[]
  >([]);
  const updateProjectDeliverable = useUpdateProjectDeliverableStore();
  const { id } = useParams();
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(true);
  const [deleteDialog, setDeleteDialog] = useState(false);
  const auth = useAuth();
  const [approved, setApproved] = useState(
    auth.user?.role === "Freelancer" ? false : true
  );
  const [isEnded, setIsEnded] = useState(false);
  const { pathname } = useLocation();
  const update = useUpdateProfileStore();
  const [openState, setOpenState] = useState(false);
  const [action, setAction] = useState<ProjectStatus>(ProjectStatus.Closed);

  const closeProject = async () => {
    try {
      setIsLoading(true);
      const response = await CLOSE_PROJECT(id!, action);
      if (response.status) {
        toast({
          description: response.data.message,
        });
        getProject();
        setOpenState(false);
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  const getProject = async () => {
    const response = await GET_PROJECT(id!);
    if (response.status) {
      setProject(response.data.data);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getProject();
  }, [update.status]);

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_PROJECT_DELIVERABLES_BY_FILTER(
        `projectId=${id}`
      );
      if (response.status) {
        setProjectDeliverables(response.data);
      }
    };
    fetch();
  }, [updateProjectDeliverable.status]);

  return (
    <main className="p-6 lg:w-3/4 w-full mx-auto space-y-6">
      <div className="flex gap-4 items-center">
        <Link
          to={
            pathname.includes("applied-projects")
              ? "/home/applied-projects"
              : "/home/projects"
          }
        >
          <Button variant="outline" size="icon" className="shrink-0">
            <ChevronLeft className="size-4" />
          </Button>
        </Link>

        <h3 className="w-full">Details</h3>

        <PrivateComponent
          isAllowed={["Staff", "Administrator"]}
          accountId={project?.accountId}
        >
          <div className="ml-auto">
            <DropdownMenu>
              <DropdownMenuTrigger asChild disabled={isLoading}>
                {project?.status !== ProjectStatus.Closed &&
                  project?.status !== ProjectStatus.Done &&
                  !isLoading && (
                    <Button size="icon" variant="outline">
                      <MoreVertical className="size-4" />
                    </Button>
                  )}
              </DropdownMenuTrigger>
              <DropdownMenuContent>
                <Link to={"update-project"}>
                  <DropdownMenuItem>Edit</DropdownMenuItem>
                </Link>
                <DropdownMenuSeparator />

                {project?.status === ProjectStatus.Pending ? (
                  <DropdownMenuItem
                    className="text-destructive"
                    onClick={() => setDeleteDialog(true)}
                  >
                    Delete
                  </DropdownMenuItem>
                ) : (
                  <>
                    <DropdownMenuItem
                      onClick={() => {
                        setOpenState(true);
                        setAction(ProjectStatus.Closed);
                      }}
                    >
                      Stop
                    </DropdownMenuItem>
                    <DropdownMenuItem
                      onClick={() => {
                        setOpenState(true);
                        setAction(ProjectStatus.Done);
                      }}
                    >
                      Done
                    </DropdownMenuItem>
                  </>
                )}
              </DropdownMenuContent>
            </DropdownMenu>
          </div>

          <AlertDialog open={openState} onOpenChange={setOpenState}>
            <AlertDialogContent>
              <AlertDialogHeader>
                <AlertDialogTitle>Are you sure?</AlertDialogTitle>
                <AlertDialogDescription>
                  {action === ProjectStatus.Closed && "Stop this project? "}
                  {action === ProjectStatus.Done && "Finish this project? "}
                  This action cannot be undone.
                </AlertDialogDescription>
              </AlertDialogHeader>
              <AlertDialogFooter>
                <AlertDialogCancel disabled={isLoading}>
                  Cancel
                </AlertDialogCancel>
                <Button
                  variant={
                    action === ProjectStatus.Done ? "default" : "destructive"
                  }
                  disabled={isLoading}
                  onClick={() => closeProject()}
                >
                  Continue
                </Button>
              </AlertDialogFooter>
            </AlertDialogContent>
          </AlertDialog>
        </PrivateComponent>

        <ProjectDeleteDialog
          openState={deleteDialog}
          setOpenState={setDeleteDialog}
          id={id!}
        />
      </div>

      {project && projectDeliverables ? (
        <div className="grid md:grid-cols-3 gap-6">
          <div className="md:col-span-2 space-y-6">
            <Card>
              <CardHeader>
                <CardTitle>
                  <p className="text-base text-primary">{project.code}</p>
                  {project.name}
                </CardTitle>
                <CardDescription>
                  Created by {project.accountFirstName}{" "}
                  {project.accountLastName}
                </CardDescription>
              </CardHeader>
              {/* <CardContent className="space-y-2">
                <div className="flex items-center gap-2 mt-2 flex-col sm:flex-row">
                  <Badge>{project.projectCategoryName}</Badge>
                  <p className="text-xs font-medium">
                    {project.duration}-day delivery
                  </p>

                  <Badge variant="outline" className="gap-1">
                    <CircleDollarSign className="size-3" />
                    {formatNumberToDecimal(project.price, 0)}
                  </Badge>

                  <PrivateComponent isAllowed={["Administrator", "Staff"]}>
                    <Badge
                      variant="secondary"
                      className="w-fit sm:ml-auto gap-2"
                    >
                      {ProjectVisibility[project.visibility]}
                      <Separator orientation="vertical" className="h-3" />
                      {ProjectStatus[project.status]}
                    </Badge>
                  </PrivateComponent>
                </div>
              </CardContent> */}
            </Card>

            <Card>
              <CardHeader className="pb-2">
                <CardTitle className="text-base">Description</CardTitle>
              </CardHeader>
              <CardContent className="whitespace-pre-wrap text-sm">
                {project.description}
              </CardContent>
            </Card>
          </div>

          <div className="space-y-6">
            <Card>
              <CardHeader className="pb-2">
                <CardTitle className="text-base">Details</CardTitle>
              </CardHeader>
              <CardContent>
                <div className="grid grid-cols-2 shrink-0 gap-4 w-full">
                  <TitleItem
                    title="Category"
                    value={project.projectCategoryName}
                  />
                  <TitleItem
                    title="Duration"
                    value={project.duration + "-day delivery"}
                  />
                  <TitleItem
                    title="Price"
                    value={formatNumberToDecimal(project.price)}
                  />
                  <PrivateComponent isAllowed={["Administrator", "Staff"]}>
                    <TitleItem
                      title="Visibility"
                      value={ProjectVisibility[project.visibility]}
                    />
                  </PrivateComponent>

                  {/* {projectApply && (
                    <PrivateComponent isAllowed={["Freelancer"]}>
                      <TitleItem
                        title="Apply status"
                        value={ProjectApplyStatus[projectApply?.status]}
                      />
                    </PrivateComponent>
                  )} */}

                  <Badge
                    variant={
                      project.status === ProjectStatus.Done ||
                      project.status === ProjectStatus.Closed
                        ? "default"
                        : "secondary"
                    }
                    className="w-full col-span-2 justify-center"
                  >
                    {ProjectStatus[project.status]}
                  </Badge>
                </div>
              </CardContent>
            </Card>

            {((auth.user?.role !== "Freelancer" &&
              auth.user?.userId === project.accountId) ||
              auth.user?.role === "Freelancer") && (
              <>
                <Card>
                  <CardHeader className="pb-2">
                    <CardTitle className="text-base">Project apply</CardTitle>
                  </CardHeader>
                  <CardContent className="text-sm">
                    <PrivateComponent isAllowed={["Administrator", "Staff"]}>
                      <ProjectApplyItem project={project} />
                    </PrivateComponent>

                    <PrivateComponent isAllowed={["Freelancer"]}>
                      <ProjectApplyFreelancerSide
                        project={project}
                        approved={approved}
                        setApproved={setApproved}
                        setIsEnded={setIsEnded}
                      />
                    </PrivateComponent>
                  </CardContent>
                </Card>

                <Card>
                  <CardHeader className="pb-2">
                    <CardTitle className="text-base">
                      Project deliverable
                    </CardTitle>
                  </CardHeader>
                  <CardContent className="space-y-2 text-sm">
                    {projectDeliverables.map((item) => (
                      <ProjectDeliverableItem
                        projectDeliverable={item}
                        project={project}
                        approved={approved}
                        isEnded={isEnded}
                      />
                    ))}
                    {projectDeliverables.length === 0 && (
                      <div className="text-muted-foreground">No results</div>
                    )}
                    <PrivateComponent isAllowed={["Administrator", "Staff"]}>
                      {project.status !== ProjectStatus.Closed &&
                        project.status !== ProjectStatus.Done && (
                          <ProjectDeliverableCreateDialog id={id!} />
                        )}
                    </PrivateComponent>
                  </CardContent>
                </Card>
              </>
            )}
          </div>
        </div>
      ) : (
        <IsLoading className="w-full" />
      )}
    </main>
  );
};

export default ProjectId;
