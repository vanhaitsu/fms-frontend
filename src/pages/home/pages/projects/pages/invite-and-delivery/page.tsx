import {
  useCreateProjectStore,
  useUpdateFreelancerStore,
  useUpdateProjectDeliverableStore,
} from "@/config/store";
import { Navigate, useNavigate } from "react-router-dom";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Button } from "@/components/ui/button";
import { X } from "lucide-react";
import { useEffect, useState } from "react";
import { Freelancer, PaginationData, ProjectDeliverable } from "@/types";
import {
  CREATE_PROJECT_APPLY,
  DELETE_PROJECT_DELIVERABLE,
  GET_FREELANCERS_BY_FILTERS,
  GET_PROJECT_DELIVERABLES_BY_FILTER,
} from "@/lib/api";
import ProjectDeliverableCreateDialog from "./components/ProjectDeliverableCreateDialog";
import useQuery from "@/hooks/useQuery";
import useDebounce from "@/hooks/useDebounce";
import { ProjectVisibility, initialPagination } from "@/types/initialSeed";
import FreelancerCommandItem from "../create-project/components/FreelancerCommandItem";
import DataTablePagination from "@/components/DataTablePagination";
import { Input } from "@/components/ui/input";
import { toast } from "@/components/ui/use-toast";

const InviteAndDelivery = () => {
  const save = useCreateProjectStore();
  const [projectDeliverables, setProjectDeliverables] = useState<
    ProjectDeliverable[]
  >([]);
  const updateProjectDeliverable = useUpdateProjectDeliverableStore();
  const [freelancers, setFreelancers] = useState<Freelancer[]>([]);
  const updateFreelancer = useUpdateFreelancerStore();
  const query = useQuery();
  const debouncedQuery = useDebounce(
    query.getQuery().toString() +
      "&skill-type=" +
      encodeURIComponent(save.category ?? "") +
      "&projectId=" +
      save.id,
    500
  );
  const [paginationData, setPaginationData] =
    useState<PaginationData>(initialPagination);
  const [isInviting, setIsInviting] = useState(false);
  const navigate = useNavigate();

  const fetch = async (id: string) => {
    const response = await DELETE_PROJECT_DELIVERABLE(id);
    if (response.status) {
      updateProjectDeliverable.toggleStatus();
    }
  };

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_FREELANCERS_BY_FILTERS(debouncedQuery);
      if (response.status) {
        setFreelancers(response.data);
        setIsInviting(false);
        setPaginationData(
          JSON.parse(response.headers["xPagination"]) as PaginationData
        );
      }
    };
    fetch();
  }, [debouncedQuery, updateFreelancer.status]);

  const invite = async (id: string) => {
    try {
      setIsInviting(true);
      const response = await CREATE_PROJECT_APPLY({
        projectId: save.id,
        freelancerId: id,
      });
      if (response.status) {
        toast({
          description: response.data.message,
        });
        updateFreelancer.toggleStatus();
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    }
  };

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_PROJECT_DELIVERABLES_BY_FILTER(
        `projectId=${save.id}`
      );
      if (response.status) {
        setProjectDeliverables(response.data);
      }
    };
    fetch();
  }, [updateProjectDeliverable.status]);

  if (save.id === null) {
    return <Navigate to="/home/projects" />;
  } else {
    return (
      <main className="p-6 space-y-6 md:w-2/3 xl:w-1/2 mx-auto">
        <Card>
          <CardHeader>
            <CardTitle>Project Deliverable</CardTitle>
            <CardDescription>
              Required deliveries of the project
            </CardDescription>
          </CardHeader>
          <CardContent className="space-y-2">
            {projectDeliverables.map((item) => (
              <div className="flex gap-2 items-center">
                <p className="font-medium">{item.name}</p>
                <p className="text-muted-foreground">
                  ({item.deliverableTypeName})
                </p>
                <X
                  onClick={() => fetch(item.id)}
                  className="size-4 text-muted-foreground hover:text-destructive"
                />
              </div>
            ))}
            <ProjectDeliverableCreateDialog id={save.id} />
          </CardContent>
        </Card>

        {save.visibility !== ProjectVisibility.Public && !save.invited && (
          <Card>
            <CardHeader>
              <CardTitle>Choose your Freelancers</CardTitle>
              <CardDescription>
                Invite candidates for your project
              </CardDescription>
            </CardHeader>
            <CardContent className="space-y-4">
              <Input
                placeholder="Search"
                onChange={(e) => {
                  query.setQuery("search", e.target.value);
                }}
                value={query.getQueryValue("search")}
                className="w-full"
              />

              {freelancers.map((item) => (
                <div className="flex items-center gap-2 justify-between">
                  <FreelancerCommandItem freelancer={item} />
                  {item.invited ? (
                    <Button size="sm" disabled variant="outline">
                      Invited
                    </Button>
                  ) : (
                    <Button
                      variant="secondary"
                      onClick={() => invite(item.id)}
                      disabled={isInviting}
                      size="sm"
                    >
                      Invite
                    </Button>
                  )}
                </div>
              ))}

              {freelancers.length === 0 && (
                <div className="text-sm text-muted-foreground">No results</div>
              )}
            </CardContent>
            <CardFooter>
              <DataTablePagination paginationData={paginationData} />
            </CardFooter>
          </Card>
        )}

        <div className="flex justify-end gap-2">
          <Button
            variant="outline"
            onClick={() => navigate(`/home/projects/${save.id}`)}
          >
            Skip
          </Button>

          <Button onClick={() => navigate(`/home/projects/${save.id}`)}>
            Finish
          </Button>
        </div>
      </main>
    );
  }
};

export default InviteAndDelivery;
