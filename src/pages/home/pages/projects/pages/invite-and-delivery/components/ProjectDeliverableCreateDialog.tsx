import { FC, useEffect, useState } from "react";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { projectDeliverableCreateSchema } from "@/lib/form";
import {
  CREATE_PROJECT_DELIVERABLE,
  GET_DELIVERABLE_TYPES_BY_FILTER,
} from "@/lib/api";
import { toast } from "@/components/ui/use-toast";
import { Plus } from "lucide-react";
import {
  useUpdateDeliverableTypeStore,
  useUpdateProjectDeliverableStore,
} from "@/config/store";
import { DeliverableType } from "@/types";
import {
  Command,
  CommandEmpty,
  CommandGroup,
  CommandInput,
  CommandItem,
  CommandList,
} from "@/components/ui/command";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import useDebounce from "@/hooks/useDebounce";
import { CaretSortIcon } from "@radix-ui/react-icons";
import DeliverableTypeCreateDialog from "../../../../deliverable-type/components/DeliverableTypeCreateDialog";

interface ProjectDeliverableCreateDialogProps {
  id: string;
}

const ProjectDeliverableCreateDialog: FC<
  ProjectDeliverableCreateDialogProps
> = ({ id }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [selectedType, setSelectedType] = useState<DeliverableType | null>(
    null
  );
  const [types, setTypes] = useState<DeliverableType[]>([]);
  const [popoverOpenState, setPopoverOpenState] = useState(false);
  const [search, setSearch] = useState<string>("");
  const debouncedQuery = useDebounce(search, 500);
  const update = useUpdateProjectDeliverableStore();
  const updateType = useUpdateDeliverableTypeStore();
  const [createTypeOpenState, setCreateTypePopoverOpenState] = useState(false);

  const form = useForm<z.infer<typeof projectDeliverableCreateSchema>>({
    resolver: zodResolver(projectDeliverableCreateSchema),
  });

  const onSubmit = async (
    values: z.infer<typeof projectDeliverableCreateSchema>
  ) => {
    try {
      setIsLoading(true);
      const response = await CREATE_PROJECT_DELIVERABLE({
        ...values,
        projectId: id,
      });
      if (response.status) {
        toast({
          description: response.data.message,
        });
        form.reset();
        update.toggleStatus();
        setSelectedType(null);
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  const fetch = async () => {
    const response = await GET_DELIVERABLE_TYPES_BY_FILTER(
      `search=${debouncedQuery}&page-size=5`
    );
    if (response.status) {
      setTypes(response.data);
    }
  };

  useEffect(() => {
    fetch();
  }, [debouncedQuery, updateType.status]);

  return (
    <Dialog
      onOpenChange={(openState) => {
        if (openState === true) {
          form.reset();
          setSelectedType(null);
        }
      }}
    >
      <DialogTrigger asChild>
        <Button variant="outline" className="gap-2 w-full">
          <Plus className="size-4" />
          Add
        </Button>
      </DialogTrigger>
      <DialogContent className="w-96">
        <DialogHeader>
          <DialogTitle>Add new project deliverable</DialogTitle>
        </DialogHeader>

        <Form {...form}>
          <form
            id="create-deliverable"
            onSubmit={form.handleSubmit(onSubmit)}
            className="space-y-4"
          >
            <FormField
              control={form.control}
              name="name"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel>Name *</FormLabel>
                  <FormControl>
                    <Input
                      disabled={isLoading}
                      value={field.value ?? ""}
                      onChange={field.onChange}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </form>
          <FormField
            control={form.control}
            name="deliverableTypeId"
            render={() => (
              <FormItem className="w-full">
                <FormLabel>Type *</FormLabel>
                <FormControl>
                  <Popover
                    open={popoverOpenState}
                    onOpenChange={(openState) => {
                      setPopoverOpenState(openState);
                      setSearch("");
                    }}
                  >
                    <PopoverTrigger
                      asChild
                      className="w-full"
                      onClick={() => fetch()}
                    >
                      <Button
                        type="button"
                        variant="outline"
                        className="hover:bg-transparent gap-2 font-normal px-3 justify-start h-9"
                      >
                        <div className="flex w-full justify-between items-center">
                          {selectedType ? selectedType.name : "Select"}
                          <CaretSortIcon className="shrink-0 size-4 opacity-50" />
                        </div>
                      </Button>
                    </PopoverTrigger>
                    <PopoverContent className="p-0 min-w-[var(--radix-popover-trigger-width)]">
                      <Command shouldFilter={false}>
                        <CommandInput
                          placeholder="Search"
                          onValueChange={(e) => setSearch(e)}
                        />
                        <CommandList>
                          <CommandEmpty>No results found</CommandEmpty>
                          <CommandGroup>
                            <Button
                              variant="link"
                              size="sm"
                              className="w-full"
                              onClick={() =>
                                setCreateTypePopoverOpenState(true)
                              }
                            >
                              Create
                            </Button>

                            {types.map((item) => (
                              <CommandItem
                                onSelect={() => {
                                  setSelectedType(item);
                                  form.setValue("deliverableTypeId", item.id);
                                  setSearch("");
                                  setPopoverOpenState(false);
                                }}
                              >
                                <div>
                                  <p className="font-medium">{item.name}</p>
                                  <p className="text-muted-foreground text-xs line-clamp-3">
                                    {item.description}
                                  </p>
                                </div>
                              </CommandItem>
                            ))}
                          </CommandGroup>
                        </CommandList>
                      </Command>
                    </PopoverContent>
                  </Popover>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
        </Form>

        <DeliverableTypeCreateDialog
          openState={createTypeOpenState}
          setOpenState={setCreateTypePopoverOpenState}
        />

        <DialogFooter>
          <Button type="submit" form="create-deliverable">
            Save
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default ProjectDeliverableCreateDialog;
