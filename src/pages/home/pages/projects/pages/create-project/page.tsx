import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm, useWatch } from "react-hook-form";
import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { useEffect, useState } from "react";
import { toast } from "@/components/ui/use-toast";
import { projectCreateSchema } from "@/lib/form";
import { CREATE_PROJECT, GET_PROJECT_CATEGORIES_BY_FILTER } from "@/lib/api";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Textarea } from "@/components/ui/textarea";
import { useNavigate } from "react-router-dom";
import { Button } from "@/components/ui/button";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { convertEnumToList } from "@/lib/utils";
import { ProjectVisibility } from "@/types/initialSeed";
import { ChevronLeft } from "lucide-react";
import { Freelancer, ProjectCategory } from "@/types";
import AssignToSpecificFreelancerDialog from "./components/AssignToSpecificFreelancerDialog";
import useAuth from "@/hooks/useAuth";
import { useCreateProjectStore } from "@/config/store";

const CreateProject = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [projectCategories, setProjectCategories] = useState<ProjectCategory[]>(
    []
  );
  const [freelancer, setFreelancer] = useState<Freelancer | null>(null);
  const [selectedCategoryName, setSelectedCategoryName] = useState<string>("");
  const navigate = useNavigate();
  const auth = useAuth();
  const store = useCreateProjectStore();

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_PROJECT_CATEGORIES_BY_FILTER();
      if (response.status) {
        setProjectCategories(response.data);
      }
    };
    fetch();
  }, []);

  const form = useForm<z.infer<typeof projectCreateSchema>>({
    resolver: zodResolver(projectCreateSchema),
  });

  const visibility = useWatch({
    control: form.control,
    name: "visibility",
  });

  const onSubmit = async (values: z.infer<typeof projectCreateSchema>) => {
    try {
      setIsLoading(true);
      const response = await CREATE_PROJECT({
        ...values,
        freelancerId: freelancer?.id,
        accountId: auth.user?.userId,
      });
      if (response.status) {
        toast({
          description: response.data.message,
        });
        store.setId(response.data.data.id);
        store.setInvited(freelancer !== null);
        store.setVisibility(values.visibility);
        store.setCategory(
          projectCategories.find((e) => e.id == values.projectCategoryId)!.name
        );
        navigate("invite-and-delivery");
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <main className="p-6">
      <div className="lg:w-3/4 w-full mx-auto space-y-6">
        <div className="flex gap-4 items-center">
          <Button
            variant="outline"
            onClick={() => navigate("/home/projects")}
            size="icon"
            className="shrink-0"
          >
            <ChevronLeft className="size-4" />
          </Button>

          <h3 className="w-full">Add new Project</h3>
        </div>

        <Form {...form}>
          <form
            id="create"
            onSubmit={form.handleSubmit(onSubmit)}
            className="grid md:grid-cols-3 gap-6"
          >
            <Card className="md:col-span-2 h-fit">
              <CardHeader>
                <CardTitle>Information</CardTitle>
                <CardDescription>
                  Set up your Project information
                </CardDescription>
              </CardHeader>

              <CardContent className="space-y-4">
                <FormField
                  control={form.control}
                  name="code"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Code *</FormLabel>
                      <FormControl>
                        <Input
                          disabled={isLoading}
                          value={field.value ?? ""}
                          onChange={field.onChange}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />

                <FormField
                  control={form.control}
                  name="name"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Name *</FormLabel>
                      <FormControl>
                        <Textarea
                          disabled={isLoading}
                          value={field.value ?? ""}
                          onChange={field.onChange}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />

                <FormField
                  control={form.control}
                  name="description"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Description *</FormLabel>
                      <FormControl>
                        <Textarea
                          rows={10}
                          disabled={isLoading}
                          value={field.value ?? ""}
                          onChange={field.onChange}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </CardContent>
            </Card>

            <Card className="h-fit">
              <CardHeader>
                <CardTitle>Details</CardTitle>
                <CardDescription>Set up your Project details</CardDescription>
              </CardHeader>

              <CardContent className="space-y-4">
                <FormField
                  control={form.control}
                  name="projectCategoryId"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Category *</FormLabel>
                      <FormControl>
                        <Select
                          disabled={isLoading}
                          onValueChange={(value) => {
                            field.onChange(value);
                            const selectedCategory = projectCategories.find(
                              (category) => category.id === value
                            );
                            setSelectedCategoryName(
                              selectedCategory?.name || ""
                            );
                          }}
                          value={
                            field.value != undefined ? String(field.value) : ""
                          }
                        >
                          <SelectTrigger
                            disabled={projectCategories.length === 0}
                            className="w-full"
                          >
                            <SelectValue placeholder="Select" />
                          </SelectTrigger>
                          <SelectContent>
                            {projectCategories.map((item) => (
                              <SelectItem value={item.id}>
                                {item.name}
                              </SelectItem>
                            ))}
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />

                <FormField
                  control={form.control}
                  name="visibility"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Visibility *</FormLabel>
                      <FormControl>
                        <Select
                          disabled={isLoading}
                          onValueChange={(value) =>
                            field.onChange(Number(value))
                          }
                          value={
                            field.value != undefined ? String(field.value) : ""
                          }
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Select" />
                          </SelectTrigger>
                          <SelectContent>
                            {convertEnumToList(ProjectVisibility).map(
                              (item, index) => (
                                <SelectItem value={index.toString()}>
                                  {item}
                                </SelectItem>
                              )
                            )}
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />

                {visibility == ProjectVisibility.Private && (
                  <AssignToSpecificFreelancerDialog
                    freelancer={freelancer}
                    setFreelancer={setFreelancer}
                    projectCategoryName={selectedCategoryName}
                  />
                )}

                <FormField
                  control={form.control}
                  name="price"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Price *</FormLabel>
                      <FormControl>
                        <Input
                          type="number"
                          disabled={isLoading}
                          value={field.value ?? ""}
                          onChange={field.onChange}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="deposit"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Deposit *</FormLabel>
                      <FormControl>
                        <Input
                          type="number"
                          disabled={isLoading}
                          value={field.value ?? ""}
                          onChange={field.onChange}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="duration"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Duration *</FormLabel>
                      <FormControl>
                        <Input
                          type="number"
                          disabled={isLoading}
                          placeholder="Day"
                          value={field.value ?? ""}
                          onChange={field.onChange}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </CardContent>
            </Card>
          </form>
        </Form>

        <div className="flex justify-end gap-2">
          {/* <Button
            onClick={() => {
              form.setValue("code", new Date().toString());
              form.setValue("name", "name");
              form.setValue("description", "WEB101");
              form.setValue(
                "projectCategoryId",
                "BC2A445E-44B8-4610-DE19-08DC813F95F4".toLowerCase()
              );
              form.setValue("visibility", 0);
              form.setValue("price", 10000);
              form.setValue("deposit", 5000);
              form.setValue("duration", 3);
            }}
          >
            Fake data
          </Button> */}

          <Button
            variant="outline"
            disabled={isLoading}
            onClick={() => navigate("/home/projects")}
          >
            Cancel
          </Button>

          <Button form="create" disabled={isLoading} type="submit">
            Next
          </Button>
        </div>
      </div>
    </main>
  );
};

export default CreateProject;
