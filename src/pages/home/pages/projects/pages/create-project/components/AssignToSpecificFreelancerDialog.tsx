import { Button } from "@/components/ui/button";
import { Freelancer } from "@/types";
import { FC, useEffect, useState } from "react";
import {
  Command,
  CommandEmpty,
  CommandGroup,
  CommandInput,
  CommandItem,
  CommandList,
} from "@/components/ui/command";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import useDebounce from "@/hooks/useDebounce";
import { GET_FREELANCERS_BY_FILTERS } from "@/lib/api";
import FreelancerCommandItem from "./FreelancerCommandItem";
import { UserPlus } from "lucide-react";

interface AssignToSpecificFreelancerDialogProps {
  freelancer: Freelancer | null;
  setFreelancer: (e: Freelancer | null) => void;
  projectCategoryName: string;
}

const AssignToSpecificFreelancerDialog: FC<
  AssignToSpecificFreelancerDialogProps
> = ({ freelancer, setFreelancer, projectCategoryName }) => {
  const [search, setSearch] = useState<string>("");
  const debouncedQuery = useDebounce(search, 500);
  const [freelancers, setFreelancers] = useState<Freelancer[]>([]);
  const [openState, setOpenState] = useState(false);

  const fetch = async () => {
    const response = await GET_FREELANCERS_BY_FILTERS(
      `search=${debouncedQuery}&skill-type=${encodeURIComponent(
        projectCategoryName
      )}&page-size=5`
    );
    if (response.status) {
      setFreelancers(response.data);
    }
  };

  useEffect(() => {
    fetch();
  }, [debouncedQuery]);

  return (
    <Popover
      open={openState}
      onOpenChange={(openState) => {
        setOpenState(openState);
        setSearch("");
      }}
    >
      <PopoverTrigger
        asChild
        disabled={projectCategoryName.length === 0}
        className="w-full"
        onClick={() => fetch()}
      >
        {freelancer === null ? (
          <Button type="button" variant="outline" className="w-full gap-2">
            <UserPlus className="size-4" />
            Assign
          </Button>
        ) : (
          <Button variant="outline" className="h-fit justify-start text-start">
            <FreelancerCommandItem freelancer={freelancer} />
          </Button>
        )}
      </PopoverTrigger>
      <PopoverContent className="p-0">
        <Command shouldFilter={false}>
          <CommandInput
            placeholder="Search"
            onValueChange={(e) => setSearch(e)}
          />
          <CommandList>
            <CommandEmpty>No results found</CommandEmpty>
            <CommandGroup>
              {freelancers.map((item) => (
                <CommandItem
                  onSelect={() => {
                    setFreelancer(item);
                    setSearch("");
                    setOpenState(false);
                  }}
                >
                  <FreelancerCommandItem freelancer={item} />
                </CommandItem>
              ))}

              {freelancer !== null && (
                <Button
                  variant="link"
                  size="sm"
                  className="w-full"
                  onClick={() => {
                    setFreelancer(null);
                  }}
                >
                  Clear
                </Button>
              )}
            </CommandGroup>
          </CommandList>
        </Command>
      </PopoverContent>
    </Popover>
  );
};

export default AssignToSpecificFreelancerDialog;
