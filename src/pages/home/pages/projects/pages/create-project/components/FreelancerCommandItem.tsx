import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { Freelancer, ProjectApply } from "@/types";
import { FC } from "react";

interface FreelancerCommandItemProps {
  freelancer?: Freelancer;
  projectApply?: ProjectApply;
}

const FreelancerCommandItem: FC<FreelancerCommandItemProps> = ({
  freelancer,
  projectApply,
}) => {
  if (freelancer !== undefined && projectApply === undefined) {
    return (
      <div className="flex items-center gap-2 text-sm">
        <Avatar>
          <AvatarImage src={freelancer?.image ?? "#"} />
          <AvatarFallback>
            {freelancer!.firstName.charAt(0).toUpperCase()}
          </AvatarFallback>
        </Avatar>
        <div>
          <p className="font-medium">
            {freelancer!.firstName} {freelancer!.lastName}
          </p>
          <p className="text-muted-foreground font-normal">
            {freelancer!.email}
          </p>
        </div>
      </div>
    );
  } else if (freelancer === undefined && projectApply !== undefined) {
    return (
      <div className="flex items-center gap-2 text-sm">
        <Avatar>
          <AvatarImage src={projectApply?.image ?? "#"} />
          <AvatarFallback>
            {projectApply!.freelancerFirstName.charAt(0).toUpperCase()}
          </AvatarFallback>
        </Avatar>
        <div>
          <p className="font-medium">
            {projectApply!.freelancerFirstName}{" "}
            {projectApply!.freelancerLastName}
          </p>
          {/* <p className="text-muted-foreground">
            {projectApply!.freelancerEmail}
          </p> */}
        </div>
      </div>
    );
  }
};

export default FreelancerCommandItem;
