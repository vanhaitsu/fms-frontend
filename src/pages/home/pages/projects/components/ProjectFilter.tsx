import PrivateComponent from "@/components/PrivateComponent";
import { Button } from "@/components/ui/button";
import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Switch } from "@/components/ui/switch";
import useAuth from "@/hooks/useAuth";
import useQuery from "@/hooks/useQuery";
import { convertEnumToList } from "@/lib/utils";
import { ProjectCategory } from "@/types";
import { ProjectStatus, ProjectVisibility } from "@/types/initialSeed";
import {
  ArrowDownNarrowWide,
  ArrowDownWideNarrow,
  RotateCcw,
} from "lucide-react";
import { FC } from "react";

interface ProjectFilterProps {
  projectCategories: ProjectCategory[];
}

const ProjectFilter: FC<ProjectFilterProps> = ({ projectCategories }) => {
  const query = useQuery();
  const auth = useAuth();

  return (
    <div className="flex flex-col gap-4">
      <PrivateComponent isAllowed={["Administrator", "Staff"]}>
        <div className="flex items-center justify-between space-x-2">
          <Label htmlFor="my-project-only">View my projects only</Label>
          <Switch
            checked={!!query.getQueryValue("account-id")}
            onCheckedChange={(checkState) =>
              checkState
                ? query.setQuery("account-id", auth.user!.userId)
                : query.setQuery("account-id", "all")
            }
            id="my-project-only"
          />
        </div>

        <Select
          onValueChange={(e) => {
            query.setQuery("project-category-id", e);
          }}
          value={query.getQueryValue("project-category-id")}
        >
          <SelectTrigger className="bg-background w-full">
            <SelectValue placeholder="Category" />
          </SelectTrigger>
          <SelectContent>
            {projectCategories.map((item) => (
              <SelectItem value={item.id}>{item.name}</SelectItem>
            ))}
            <SelectItem value="all">All</SelectItem>
          </SelectContent>
        </Select>

        <Select
          onValueChange={(e) => {
            query.setQuery("status", e);
          }}
          value={query.getQueryValue("status")}
        >
          <SelectTrigger className="bg-background w-full">
            <SelectValue placeholder="Status" />
          </SelectTrigger>
          <SelectContent>
            {convertEnumToList(ProjectStatus).map((item, index) => (
              <SelectItem value={index.toString()}>{item}</SelectItem>
            ))}
            <SelectItem value="all">All</SelectItem>
          </SelectContent>
        </Select>

        <Select
          onValueChange={(e) => {
            query.setQuery("visibility", e);
          }}
          value={query.getQueryValue("visibility")}
        >
          <SelectTrigger className="bg-background w-full">
            <SelectValue placeholder="Visibility" />
          </SelectTrigger>
          <SelectContent>
            {convertEnumToList(ProjectVisibility).map((item, index) => (
              <SelectItem value={index.toString()}>{item}</SelectItem>
            ))}
            <SelectItem value="all">All</SelectItem>
          </SelectContent>
        </Select>
      </PrivateComponent>

      <div className="flex gap-2">
        <Select
          onValueChange={(e) => {
            query.setQuery("order", e);
          }}
          value={query.getQueryValue("order")}
        >
          <SelectTrigger className="bg-background w-full">
            <SelectValue placeholder="Sort" />
          </SelectTrigger>
          <SelectContent>
            <SelectItem value="name">Name</SelectItem>
            <SelectItem value="price">Price</SelectItem>
            <SelectItem value="code">Code</SelectItem>
            <SelectItem value="all">No sort</SelectItem>
          </SelectContent>
        </Select>

        <Button
          onClick={() => query.setSortDirection()}
          variant="outline"
          size="icon"
          className="shrink-0"
        >
          {query.getQueryValue("order-by-descending") === null ||
          query.getQueryValue("order-by-descending") === "true" ? (
            <ArrowDownWideNarrow className="size-4" />
          ) : (
            <ArrowDownNarrowWide className="size-4" />
          )}
        </Button>
      </div>

      <Button
        onClick={() => {
          query.resetQuery(["pageSize"]);
        }}
        variant="outline"
        className="gap-2"
      >
        <RotateCcw className="size-4" />
        Reset
      </Button>
    </div>
  );
};

export default ProjectFilter;
