import { FC } from "react";

interface TitleItemProps {
  title: string;
  value?: string | React.ReactNode;
  className?: string;
}

const TitleItem: FC<TitleItemProps> = ({ title, value, className }) => {
  return (
    <div className={`shrink-0 ${className}`}>
      <div>
        <p className="text-xs text-muted-foreground">{title}</p>
        <p className="font-medium text-sm">{value}</p>
      </div>
    </div>
  );
};

export default TitleItem;
