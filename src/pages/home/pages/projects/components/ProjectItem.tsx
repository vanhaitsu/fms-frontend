import PrivateComponent from "@/components/PrivateComponent";
import { Badge } from "@/components/ui/badge";
import { Separator } from "@/components/ui/separator";
import { formatNumberToDecimal } from "@/lib/utils";
import { Project, ProjectApply } from "@/types";
import {
  ProjectApplyStatus,
  ProjectStatus,
  ProjectVisibility,
} from "@/types/initialSeed";
import { FC } from "react";
import { Link } from "react-router-dom";
import TitleItem from "./TitleItem";
import { Megaphone } from "lucide-react";

interface ProjectItemProps {
  project: Project;
  projectApply?: ProjectApply;
}

const ProjectItem: FC<ProjectItemProps> = ({ project, projectApply }) => {
  return (
    <div>
      <Link to={project.id}>
        <div className="p-4 border rounded-lg hover:bg-muted/40 flex flex-col md:flex-row items-center gap-4 shadow-sm">
          <div className="w-full">
            <p className="font-semibold text-xs text-primary">{project.code}</p>

            <p className="font-medium">{project.name}</p>

            <p className="text-sm text-muted-foreground line-clamp-3">
              {project.description}
            </p>

            <PrivateComponent isAllowed={["Freelancer"]}>
              {projectApply &&
                projectApply.status === ProjectApplyStatus.Invited && (
                  <div className="text-xs font-medium text-primary flex gap-2 items-center mt-2">
                    <Megaphone className="size-4" /> You have been invited to
                    join this project.
                  </div>
                )}
            </PrivateComponent>
          </div>

          <Separator
            orientation="vertical"
            className="md:h-28 hidden md:block"
          />

          <Separator className="md:hidden" />

          <div className="grid grid-cols-2 shrink-0 gap-4 md:w-80 w-full">
            <TitleItem title="Category" value={project.projectCategoryName} />
            <TitleItem
              title="Duration"
              value={project.duration + "-day delivery"}
            />
            <TitleItem
              title="Price"
              value={formatNumberToDecimal(project.price)}
            />
            <PrivateComponent isAllowed={["Administrator", "Staff"]}>
              <TitleItem
                title="Visibility"
                value={ProjectVisibility[project.visibility]}
              />
            </PrivateComponent>

            {projectApply && (
              <PrivateComponent isAllowed={["Freelancer"]}>
                <TitleItem
                  title="Apply status"
                  value={ProjectApplyStatus[projectApply?.status]}
                />
              </PrivateComponent>
            )}

            <Badge
              variant={
                project.status === ProjectStatus.Done ||
                project.status === ProjectStatus.Closed
                  ? "default"
                  : "secondary"
              }
              className="w-full col-span-2 justify-center"
            >
              {ProjectStatus[project.status]}
            </Badge>
          </div>
        </div>
      </Link>
    </div>
  );
};

export default ProjectItem;
