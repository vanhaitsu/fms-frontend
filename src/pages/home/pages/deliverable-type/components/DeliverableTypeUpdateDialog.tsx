import { useUpdateDeliverableTypeStore } from "@/config/store";
import { FC, useEffect, useState } from "react";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { deliverableTypeCreateSchema } from "@/lib/form";
import { GET_DELIVERABLE_TYPE, UPDATE_DELIVERABLE_TYPE } from "@/lib/api";
import { toast } from "@/components/ui/use-toast";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Button } from "@/components/ui/button";
import { Ellipsis } from "lucide-react";
import { Input } from "@/components/ui/input";
import { Textarea } from "@/components/ui/textarea";
import { DeliverableType } from "@/types";
import DeliverableTypeDeleteDialog from "./DeliverableTypeDeleteDialog";

interface DeliverableTypeUpdateDialogProps {
  id: string;
}

const DeliverableTypeUpdateDialog: FC<DeliverableTypeUpdateDialogProps> = ({
  id,
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const update = useUpdateDeliverableTypeStore();
  const [type, setType] = useState<DeliverableType | null>(null);
  const [openState, setOpenState] = useState(false);

  const fetch = async () => {
    const response = await GET_DELIVERABLE_TYPE(id);
    if (response.status) {
      setType(response.data.data);
    }
  };

  const form = useForm<z.infer<typeof deliverableTypeCreateSchema>>({
    resolver: zodResolver(deliverableTypeCreateSchema),
  });

  useEffect(() => {
    if (type) {
      form.setValue("name", type.name);
      form.setValue("description", type.description);
      setOpenState(true);
    }
  }, [type]);

  const onSubmit = async (
    values: z.infer<typeof deliverableTypeCreateSchema>
  ) => {
    try {
      setIsLoading(true);
      const response = await UPDATE_DELIVERABLE_TYPE(
        {
          ...values,
          name: "." + values.name.toLowerCase().replace(".", ""), // todo validate file name format
        },
        id
      );
      if (response.status) {
        toast({
          description: response.data.message,
        });
        update.toggleStatus();
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <>
      <Button
        onClick={() => fetch()}
        size="icon"
        variant="ghost"
        className="size-8"
      >
        <Ellipsis className="size-4" />
      </Button>

      <Dialog open={openState} onOpenChange={setOpenState}>
        <DialogContent className="w-96">
          <DialogHeader>
            <DialogTitle>Edit deliverable type</DialogTitle>
          </DialogHeader>
          <Form {...form}>
            <form
              id="create-type"
              onSubmit={form.handleSubmit(onSubmit)}
              className="space-y-4"
            >
              <FormField
                control={form.control}
                name="name"
                render={({ field }) => (
                  <FormItem className="w-full">
                    <FormLabel>Name *</FormLabel>
                    <FormControl>
                      <Input
                        disabled={isLoading}
                        value={field.value ?? ""}
                        onChange={field.onChange}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />

              <FormField
                control={form.control}
                name="description"
                render={({ field }) => (
                  <FormItem className="w-full">
                    <FormLabel>Description *</FormLabel>
                    <FormControl>
                      <Textarea
                        rows={3}
                        disabled={isLoading}
                        value={field.value ?? ""}
                        onChange={field.onChange}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </form>
          </Form>

          <DialogFooter>
            <DeliverableTypeDeleteDialog
              id={id}
              isDeleted={type ? type.isDeleted : true}
              isLoadingParent={isLoading}
              onSuccess={() => setOpenState(false)}
            />

            <Button disabled={isLoading} type="submit" form="create-type">
              Save
            </Button>
          </DialogFooter>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default DeliverableTypeUpdateDialog;
