import { Account, DeliverableType } from "@/types";
import { ColumnDef } from "@tanstack/react-table";
import { Badge } from "@/components/ui/badge";
import DeliverableTypeUpdateDialog from "./DeliverableTypeUpdateDialog";

export const DeliverableTypeColumns: ColumnDef<DeliverableType>[] = [
  {
    accessorKey: "id",
    header: "Id",
  },
  {
    accessorKey: "name",
    header: "Name",
  },
  {
    accessorKey: "description",
    header: "Description",
  },
  {
    id: "action",
    cell: ({ row }) => <DeliverableTypeUpdateDialog id={row.getValue("id")} />,
  },
];
