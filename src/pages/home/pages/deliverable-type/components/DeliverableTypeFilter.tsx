import useQuery from "@/hooks/useQuery";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";

const DeliverableTypeFilter = () => {
  const query = useQuery();

  return (
    <div className="flex flex-col gap-4">
      <Select
        onValueChange={(e) => {
          query.setQuery("is-deleted", e);
        }}
        value={query.getQueryValue("is-deleted")}
      >
        <SelectTrigger className="bg-background w-full">
          <SelectValue placeholder="State" />
        </SelectTrigger>
        <SelectContent>
          <SelectItem value="false">Active</SelectItem>
          <SelectItem value="true">Inactive</SelectItem>
          <SelectItem value="all">All</SelectItem>
        </SelectContent>
      </Select>
    </div>
  );
};

export default DeliverableTypeFilter;
