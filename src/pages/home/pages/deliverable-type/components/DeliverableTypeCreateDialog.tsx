import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { FC, useState } from "react";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { deliverableTypeCreateSchema } from "@/lib/form";
import { CREATE_DELIVERABLE_TYPE } from "@/lib/api";
import { toast } from "@/components/ui/use-toast";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Textarea } from "@/components/ui/textarea";
import { useUpdateDeliverableTypeStore } from "@/config/store";

interface DeliverableTypeCreateDialogProps {
  openState: boolean;
  setOpenState: (e: boolean) => void;
  autoClose?: boolean;
}

const DeliverableTypeCreateDialog: FC<DeliverableTypeCreateDialogProps> = ({
  openState,
  setOpenState,
  autoClose = true,
}) => {
  const [isLoading, setIsLoading] = useState(false);
  // const [openState, setOpenState] = useState(false);
  const update = useUpdateDeliverableTypeStore();

  const form = useForm<z.infer<typeof deliverableTypeCreateSchema>>({
    resolver: zodResolver(deliverableTypeCreateSchema),
  });

  const onSubmit = async (
    values: z.infer<typeof deliverableTypeCreateSchema>
  ) => {
    try {
      setIsLoading(true);
      const response = await CREATE_DELIVERABLE_TYPE({
        ...values,
        name: "." + values.name.toLowerCase().replace(".", ""), // todo validate file name format
      });
      if (response.status) {
        toast({
          description: response.data.message,
        });
        form.reset();
        update.toggleStatus();
        if (autoClose) {
          setOpenState(false);
        }
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Dialog
      open={openState}
      onOpenChange={(open) => {
        setOpenState(open);
        form.reset();
      }}
    >
      {/* <DialogTrigger asChild>
        <Button variant="link" size="sm" className="w-full">
          Create
        </Button>
      </DialogTrigger> */}
      <DialogContent className="w-96">
        <DialogHeader>
          <DialogTitle>Add new deliverable type</DialogTitle>
        </DialogHeader>
        <Form {...form}>
          <form
            id="create-type"
            onSubmit={form.handleSubmit(onSubmit)}
            className="space-y-4"
          >
            <FormField
              control={form.control}
              name="name"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel>Name *</FormLabel>
                  <FormControl>
                    <Input
                      disabled={isLoading}
                      value={field.value ?? ""}
                      onChange={field.onChange}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="description"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel>Description *</FormLabel>
                  <FormControl>
                    <Textarea
                      rows={3}
                      disabled={isLoading}
                      value={field.value ?? ""}
                      onChange={field.onChange}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </form>
        </Form>

        <DialogFooter>
          <Button type="submit" form="create-type">
            Save
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default DeliverableTypeCreateDialog;
