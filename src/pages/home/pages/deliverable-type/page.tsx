import { DataTable } from "@/components/DataTable";
import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import {
  GET_ACCOUNTS_BY_FILTERS,
  GET_DELIVERABLE_TYPES_BY_FILTER,
} from "@/lib/api";
import { DeliverableType, PaginationData } from "@/types";
import { Filter, Plus } from "lucide-react";
import { useEffect, useState } from "react";
import useQuery from "@/hooks/useQuery";
import useDebounce from "@/hooks/useDebounce";
import { initialPagination } from "@/types/initialSeed";
import DataTablePagination from "@/components/DataTablePagination";
import {
  useUpdateAccountStore,
  useUpdateDeliverableTypeStore,
} from "@/config/store";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import {
  Drawer,
  DrawerContent,
  DrawerFooter,
  DrawerTrigger,
} from "@/components/ui/drawer";
import { DeliverableTypeColumns } from "./components/DeliverableTypeColumns";
import DeliverableTypeFilter from "./components/DeliverableTypeFilter";
import DeliverableTypeCreateDialog from "./components/DeliverableTypeCreateDialog";

const DeliverableTypes = () => {
  const query = useQuery();
  const debouncedQuery = useDebounce(query.getQuery().toString(), 500);
  const [paginationData, setPaginationData] =
    useState<PaginationData>(initialPagination);
  const [types, setTypes] = useState<DeliverableType[]>([]);
  const [openState, setOpenState] = useState(false);
  const update = useUpdateDeliverableTypeStore();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_DELIVERABLE_TYPES_BY_FILTER(debouncedQuery);
      if (response.status) {
        setTypes(response.data);
        setPaginationData(
          JSON.parse(response.headers["xPagination"]) as PaginationData
        );
      }
      setIsLoading(false);
    };
    fetch();
  }, [debouncedQuery, update.status]);

  return (
    <>
      <main className="p-6 space-y-6">
        <section className="flex sm:flex-row flex-col gap-2">
          <div className="flex gap-2">
            <Input
              placeholder="Search"
              onChange={(e) => {
                query.setQuery("search", e.target.value);
              }}
              value={query.getQueryValue("search")}
              className="bg-background w-full sm:w-96"
            />

            {/* <Popover>
              <PopoverTrigger className="hidden sm:block">
                <Button variant="outline" className="gap-2">
                  <Filter className="size-4" />
                  Filter
                </Button>
              </PopoverTrigger>
              <PopoverContent>
                <DeliverableTypeFilter />
              </PopoverContent>
            </Popover>

            <Drawer>
              <DrawerTrigger>
                <Button
                  variant="outline"
                  size="icon"
                  className="flex sm:hidden"
                >
                  <Filter className="size-4" />
                </Button>
              </DrawerTrigger>
              <DrawerContent>
                <DrawerFooter>
                  <DeliverableTypeFilter />
                </DrawerFooter>
              </DrawerContent>
            </Drawer> */}
          </div>

          <Button
            className="gap-2 w-full sm:ml-auto sm:w-fit"
            onClick={() => setOpenState(true)}
          >
            <Plus className="size-4" />
            Add
          </Button>
        </section>

        <Card>
          <CardHeader>
            <CardTitle>Deliverable types</CardTitle>
            <CardDescription>Manage all deliverable types here</CardDescription>
          </CardHeader>
          <CardContent>
            <DataTable
              columns={DeliverableTypeColumns}
              data={types}
              visibilityState={{ id: false }}
              isLoading={isLoading}
            />
          </CardContent>
          <CardFooter>
            <DataTablePagination paginationData={paginationData} />
          </CardFooter>
        </Card>
      </main>

      <DeliverableTypeCreateDialog
        openState={openState}
        setOpenState={setOpenState}
        autoClose={false}
      />
    </>
  );
};

export default DeliverableTypes;
