import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import {
  GET_PROJECTS_BY_FILTERS,
  GET_PROJECT_APPLY_BY_FILTER,
  GET_PROJECT_CATEGORIES_BY_FILTER,
  GET_PROJECT_CATEGORIES_ID_BY_SKILLS_NAME,
} from "@/lib/api";
import {
  PaginationData,
  Project,
  ProjectApply,
  ProjectCategory,
  SkillGroup,
} from "@/types";
import { CircleDollarSign, Filter, Plus } from "lucide-react";
import { useEffect, useState } from "react";
import useQuery from "@/hooks/useQuery";
import useDebounce from "@/hooks/useDebounce";
import {
  ProjectApplyStatus,
  ProjectStatus,
  ProjectVisibility,
  initialPagination,
} from "@/types/initialSeed";
import DataTablePagination from "@/components/DataTablePagination";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import {
  Drawer,
  DrawerContent,
  DrawerFooter,
  DrawerTrigger,
} from "@/components/ui/drawer";
import { Badge } from "@/components/ui/badge";
import { formatNumberToDecimal } from "@/lib/utils";
import { useNavigate } from "react-router-dom";
import PrivateComponent from "@/components/PrivateComponent";
import { Separator } from "@/components/ui/separator";
import useAuth from "@/hooks/useAuth";
import ProjectFilter from "../projects/components/ProjectFilter";
import ProjectApplyFilter from "./components/ProjectApplyFilter";
import ProjectItem from "../projects/components/ProjectItem";
import IsLoading from "@/components/IsLoading";

const AppliedProjects = () => {
  const query = useQuery();
  const debouncedQuery = useDebounce(query.getQuery().toString(), 500);
  const [paginationData, setPaginationData] =
    useState<PaginationData>(initialPagination);
  const [projects, setProjects] = useState<ProjectApply[]>([]);
  const [projectCategories, setProjectCategories] = useState<ProjectCategory[]>(
    []
  );
  const navigate = useNavigate();
  const auth = useAuth();
  const [skills, setSkills] = useState("");
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchSkills = async () => {
      const sessionSkills: SkillGroup[] = JSON.parse(
        sessionStorage.getItem("skills") ?? ""
      );

      let skillList: string[] = [];
      if (sessionSkills) {
        sessionSkills.forEach((item) => {
          skillList.push(item.skillType);
        });
      }

      const response = await GET_PROJECT_CATEGORIES_ID_BY_SKILLS_NAME(
        skillList
      );
      if (response.status) {
        let skillQuery = "";
        response.data.data.forEach((item: any) => {
          skillQuery = skillQuery + "&project-category-id=" + item.project.id;
        });
        setSkills(skillQuery);
      }
    };

    if (auth.user?.role === "Freelancer") {
      fetchSkills();
    }
  }, []);

  useEffect(() => {
    const fetchProjects = async () => {
      const response = await GET_PROJECT_APPLY_BY_FILTER(
        debouncedQuery + "&freelancer-id=" + auth.user?.userId
      );
      if (response.status) {
        setProjects(response.data);
        setPaginationData(
          JSON.parse(response.headers["xPagination"]) as PaginationData
        );
      }
      setIsLoading(false); // Update loading state here
    };

    setIsLoading(true); // Set loading state before fetching
    fetchProjects();
  }, [debouncedQuery, skills]);

  useEffect(() => {
    const fetchCategories = async () => {
      const response = await GET_PROJECT_CATEGORIES_BY_FILTER();
      if (response.status) {
        setProjectCategories(response.data);
      }
    };

    fetchCategories();
  }, []);

  return (
    <main className="p-6 space-y-6">
      <section className="flex sm:flex-row flex-col gap-2">
        <div className="flex gap-2">
          <Input
            placeholder="Search"
            onChange={(e) => {
              query.setQuery("search", e.target.value);
            }}
            value={query.getQueryValue("search")}
            className="bg-background w-full sm:w-96"
          />

          <Popover>
            <PopoverTrigger className="hidden sm:block">
              <Button
                variant="outline"
                disabled={projectCategories.length === 0}
                className="gap-2"
              >
                <Filter className="size-4" />
                Filter
              </Button>
            </PopoverTrigger>
            <PopoverContent>
              <ProjectApplyFilter projectCategories={projectCategories} />
            </PopoverContent>
          </Popover>

          <Drawer>
            <DrawerTrigger>
              <Button
                variant="outline"
                size="icon"
                disabled={projectCategories.length === 0}
                className="flex sm:hidden"
              >
                <Filter className="size-4" />
              </Button>
            </DrawerTrigger>
            <DrawerContent>
              <DrawerFooter>
                <ProjectApplyFilter projectCategories={projectCategories} />
              </DrawerFooter>
            </DrawerContent>
          </Drawer>
        </div>

        <PrivateComponent isAllowed={["Administrator", "Staff"]}>
          <Button
            className="gap-2 w-full sm:ml-auto sm:w-fit"
            onClick={() => navigate("create-project")}
          >
            <Plus className="size-4" />
            Add
          </Button>
        </PrivateComponent>
      </section>

      <Card>
        <CardHeader>
          <CardTitle>Applied projects</CardTitle>
          <CardDescription>View all your applied projects here</CardDescription>
        </CardHeader>
        <CardContent className="space-y-4">
          {projects.map((item) => (
            <ProjectItem
              project={item.project!}
              projectApply={item}
              key={item.id}
            />
          ))}

          {projects.length === 0 && !isLoading && (
            <div className="text-sm text-muted-foreground">No results</div>
          )}

          {isLoading && <IsLoading />}
        </CardContent>
        <CardFooter>
          <DataTablePagination paginationData={paginationData} />
        </CardFooter>
      </Card>
    </main>
  );
};

export default AppliedProjects;
