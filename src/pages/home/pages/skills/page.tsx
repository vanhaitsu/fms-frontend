import { DataTable } from "@/components/DataTable";
import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import { GET_ACCOUNTS_BY_FILTERS, GET_SKILLS_BY_FILTERS } from "@/lib/api";
import { Account, PaginationData, Skill } from "@/types";
import { Filter } from "lucide-react";
import { useEffect, useState } from "react";
import useQuery from "@/hooks/useQuery";
import useDebounce from "@/hooks/useDebounce";
import { initialPagination } from "@/types/initialSeed";
import DataTablePagination from "@/components/DataTablePagination";
import { useUpdateAccountStore, useUpdateSkillStore } from "@/config/store";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import {
  Drawer,
  DrawerContent,
  DrawerFooter,
  DrawerTrigger,
} from "@/components/ui/drawer";
import { SkillColumns } from "./components/SkillColumns";
import SkillCreateDialog from "./components/SkillCreateDialog";

const Skills = () => {
  const query = useQuery();
  const debouncedQuery = useDebounce(query.getQuery().toString(), 500);
  const [paginationData, setPaginationData] =
    useState<PaginationData>(initialPagination);
  const [skills, setSkills] = useState<Skill[]>([]);
  const update = useUpdateSkillStore();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_SKILLS_BY_FILTERS(debouncedQuery);
      if (response.status) {
        setSkills(response.data);
        setPaginationData(
          JSON.parse(response.headers["xPagination"]) as PaginationData
        );
      }
      setIsLoading(false);
    };
    fetch();
  }, [debouncedQuery, update.status]);

  return (
    <main className="p-6 space-y-6">
      <section className="flex sm:flex-row flex-col gap-2">
        <div className="flex gap-2">
          <Input
            placeholder="Search"
            onChange={(e) => {
              query.setQuery("search", e.target.value);
            }}
            value={query.getQueryValue("search")}
            className="bg-background w-full sm:w-96"
          />

          {/* <Popover>
            <PopoverTrigger className="hidden sm:block">
              <Button variant="outline" className="gap-2">
                <Filter className="size-4" />
                Filter
              </Button>
            </PopoverTrigger>
            <PopoverContent></PopoverContent>
          </Popover>

          <Drawer>
            <DrawerTrigger>
              <Button variant="outline" size="icon" className="flex sm:hidden">
                <Filter className="size-4" />
              </Button>
            </DrawerTrigger>
            <DrawerContent>
              <DrawerFooter></DrawerFooter>
            </DrawerContent>
          </Drawer> */}
        </div>

        <SkillCreateDialog />
      </section>

      <Card>
        <CardHeader>
          <CardTitle>Skills</CardTitle>
          <CardDescription>
            Manage all skills of freelancer here
          </CardDescription>
        </CardHeader>
        <CardContent>
          <DataTable
            columns={SkillColumns}
            data={skills}
            visibilityState={{ id: false }}
            isLoading={isLoading}
          />
        </CardContent>
        <CardFooter>
          <DataTablePagination paginationData={paginationData} />
        </CardFooter>
      </Card>
    </main>
  );
};

export default Skills;
