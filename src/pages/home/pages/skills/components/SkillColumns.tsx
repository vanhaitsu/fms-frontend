import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { Account, Skill } from "@/types";
import { ColumnDef } from "@tanstack/react-table";
import { Badge } from "@/components/ui/badge";
import SkillUpdateDialog from "./SkillUpdateDialog";

export const SkillColumns: ColumnDef<Skill>[] = [
  {
    accessorKey: "id",
    header: "Id",
  },
  {
    accessorKey: "name",
    header: "Name",
  },
  {
    accessorKey: "type",
    header: "Type",
  },
  {
    accessorKey: "description",
    header: "Description",
  },
  {
    id: "action",
    cell: ({ row }) => <SkillUpdateDialog id={row.getValue("id")} />,
  },
];
