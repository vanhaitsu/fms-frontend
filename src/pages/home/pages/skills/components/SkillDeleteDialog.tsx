import { Button } from "@/components/ui/button";
import {
  AlertDialog,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "@/components/ui/alert-dialog";
import { FC, useState } from "react";
import {
  DELETE_ACCOUNT,
  DELETE_DELIVERABLE_PRODUCT,
  DELETE_DELIVERABLE_TYPE,
  DELETE_SKILL,
  RESTORE_ACCOUNT,
} from "@/lib/api";
import { toast } from "@/components/ui/use-toast";
import {
  useUpdateAccountStore,
  useUpdateDeliverableTypeStore,
  useUpdateSkillStore,
} from "@/config/store";
import useAuth from "@/hooks/useAuth";

interface SkillDeleteDialogProps {
  id: string;
  isDeleted: boolean;
  isLoadingParent?: boolean;
  onSuccess: () => void;
}

const SkillDeleteDialog: FC<SkillDeleteDialogProps> = ({
  id,
  isDeleted,
  isLoadingParent,
  onSuccess,
}) => {
  const [openState, setOpenState] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const update = useUpdateSkillStore();
  const auth = useAuth();

  const fetch = async () => {
    try {
      setIsLoading(true);
      const response = await DELETE_SKILL(id);
      if (response.status) {
        toast({
          description: response.data.message,
        });
        setOpenState(false);
        onSuccess();
        update.toggleStatus();
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  if (id !== auth.user?.userId) {
    return (
      <AlertDialog open={openState} onOpenChange={setOpenState}>
        <AlertDialogTrigger asChild>
          <Button
            variant="outline"
            className="gap-2 mr-auto"
            disabled={isLoadingParent}
          >
            {isDeleted ? "Restore" : "Delete"}
          </Button>
        </AlertDialogTrigger>
        <AlertDialogContent>
          <AlertDialogHeader>
            <AlertDialogTitle>Are you sure?</AlertDialogTitle>
            <AlertDialogDescription>
              This action cannot be undone.
            </AlertDialogDescription>
          </AlertDialogHeader>
          <AlertDialogFooter>
            <AlertDialogCancel disabled={isLoading}>Cancel</AlertDialogCancel>
            <Button
              variant={isDeleted ? "default" : "destructive"}
              disabled={isLoading}
              onClick={() => fetch()}
            >
              Continue
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialog>
    );
  }
};

export default SkillDeleteDialog;
