import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Plus } from "lucide-react";
import { useEffect, useState } from "react";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { useUpdateSkillStore } from "@/config/store";
import { skillCreateSchema } from "@/lib/form";
import { CREATE_SKILL, GET_PROJECT_CATEGORIES_BY_FILTER } from "@/lib/api";
import { toast } from "@/components/ui/use-toast";
import { ProjectCategory } from "@/types";
import { Textarea } from "@/components/ui/textarea";

const SkillCreateDialog = () => {
  const [isLoading, setIsLoading] = useState(false);
  const update = useUpdateSkillStore();
  const [categories, setCategory] = useState<ProjectCategory[]>([]);

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_PROJECT_CATEGORIES_BY_FILTER();
      if (response.status) {
        setCategory(response.data);
      }
    };
    fetch();
  }, []);

  const form = useForm<z.infer<typeof skillCreateSchema>>({
    resolver: zodResolver(skillCreateSchema),
  });

  const onSubmit = async (values: z.infer<typeof skillCreateSchema>) => {
    try {
      setIsLoading(true);
      const response = await CREATE_SKILL([values]);
      if (response.status) {
        toast({
          description: response.data.message,
        });
        form.reset();
        update.toggleStatus();
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Dialog
      onOpenChange={(openState) => {
        if (openState === true) {
          form.reset();
        }
      }}
    >
      <DialogTrigger className="sm:ml-auto w-full sm:w-fit">
        <Button className="gap-2 w-full">
          <Plus className="size-4" />
          Add
        </Button>
      </DialogTrigger>

      <DialogContent className="w-96">
        <DialogHeader>
          <DialogTitle>Add new skill</DialogTitle>
        </DialogHeader>
        <Form {...form}>
          <form
            id="create"
            onSubmit={form.handleSubmit(onSubmit)}
            className="space-y-4"
          >
            <FormField
              control={form.control}
              name="name"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel>Name *</FormLabel>
                  <FormControl>
                    <Input
                      disabled={isLoading}
                      value={field.value ?? ""}
                      onChange={field.onChange}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="type"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel>Category *</FormLabel>
                  <FormControl>
                    <Select
                      disabled={isLoading}
                      onValueChange={(value) => {
                        field.onChange(value);
                      }}
                      value={
                        field.value != undefined ? String(field.value) : ""
                      }
                    >
                      <SelectTrigger
                        disabled={categories.length === 0}
                        className="w-full"
                      >
                        <SelectValue placeholder="Select" />
                      </SelectTrigger>
                      <SelectContent>
                        {categories.map((item) => (
                          <SelectItem value={item.name}>{item.name}</SelectItem>
                        ))}
                      </SelectContent>
                    </Select>
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="description"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel>Description *</FormLabel>
                  <FormControl>
                    <Textarea
                      rows={3}
                      disabled={isLoading}
                      value={field.value ?? ""}
                      onChange={field.onChange}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </form>
        </Form>
        <DialogFooter>
          <Button form="create" type="submit" disabled={isLoading}>
            Save
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default SkillCreateDialog;
