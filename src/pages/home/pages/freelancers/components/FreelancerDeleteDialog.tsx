import { Button } from "@/components/ui/button";
import {
  AlertDialog,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "@/components/ui/alert-dialog";
import { FC, useState } from "react";
import { DELETE_FREELANCER, RESTORE_FREELANCER } from "@/lib/api";
import { toast } from "@/components/ui/use-toast";
import { useUpdateFreelancerStore } from "@/config/store";

interface FreelancerDeleteDialogProps {
  id: string;
  status: string;
  isLoadingParent?: boolean;
  onSuccess: () => void;
}

const FreelancerDeleteDialog: FC<FreelancerDeleteDialogProps> = ({
  id,
  status,
  isLoadingParent,
  onSuccess,
}) => {
  const [openState, setOpenState] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const update = useUpdateFreelancerStore();

  const fetch = async () => {
    try {
      setIsLoading(true);
      const response =
        status === "NotAvailable"
          ? await RESTORE_FREELANCER([id])
          : await DELETE_FREELANCER([id]);
      if (response.status) {
        toast({
          description: response.data.message,
        });
        setOpenState(false);
        onSuccess();
        update.toggleStatus();
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <AlertDialog open={openState} onOpenChange={setOpenState}>
      <AlertDialogTrigger asChild>
        <Button
          variant="outline"
          className="gap-2 mr-auto"
          disabled={isLoadingParent}
        >
          {status === "NotAvailable" ? "Restore" : "Delete"}
        </Button>
      </AlertDialogTrigger>
      <AlertDialogContent>
        <AlertDialogHeader>
          <AlertDialogTitle>Are you sure?</AlertDialogTitle>
          <AlertDialogDescription>
            This action cannot be undone.
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter>
          <AlertDialogCancel disabled={isLoading}>Cancel</AlertDialogCancel>
          <Button
            variant={status === "not-available" ? "default" : "destructive"}
            disabled={isLoading}
            onClick={() => fetch()}
          >
            Continue
          </Button>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
};

export default FreelancerDeleteDialog;
