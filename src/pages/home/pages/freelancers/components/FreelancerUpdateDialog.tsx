import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import { Ellipsis, Plus, X } from "lucide-react";
import { useEffect, useState } from "react";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import InputDate from "@/components/InputDate";
import { toast } from "@/components/ui/use-toast";
import {
  GET_FREELANCER,
  GET_SKILLS_GROUP_BY_TYPE,
  UPDATE_FREELANCER,
} from "@/lib/api";
import { freelancerUpdateSchema } from "@/lib/form";
import { Freelancer, SkillGroup } from "@/types";
import {
  DropdownMenu,
  DropdownMenuCheckboxItem,
  DropdownMenuContent,
  DropdownMenuSub,
  DropdownMenuSubContent,
  DropdownMenuSubTrigger,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { useUpdateFreelancerStore } from "@/config/store";
import { Badge } from "@/components/ui/badge";
import FreelancerDeleteDialog from "./FreelancerDeleteDialog";

const FreelancerUpdateDialog = ({ id }: { id: string }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [freelancer, setFreelancer] = useState<Freelancer | null>(null);
  const [openState, setOpenState] = useState(false);
  const [skills, setSkills] = useState<SkillGroup[]>([]);
  const [skillGroup, setSkillGroup] = useState<SkillGroup[]>([]);
  const update = useUpdateFreelancerStore();

  const fetch = async () => {
    const response = await GET_FREELANCER(id);
    if (response.status) {
      setFreelancer(response.data.data);
    }

    const skillsResponse = await GET_SKILLS_GROUP_BY_TYPE();
    if (skillsResponse.status) {
      setSkillGroup(skillsResponse.data);
    }
  };

  const form = useForm<z.infer<typeof freelancerUpdateSchema>>({
    resolver: zodResolver(freelancerUpdateSchema),
  });

  const onSubmit = async (values: z.infer<typeof freelancerUpdateSchema>) => {
    try {
      setIsLoading(true);
      const response = await UPDATE_FREELANCER(
        {
          ...values,
          skills: skills,
        },
        id
      );
      if (response.status) {
        toast({
          description: response.data.message,
        });
        update.toggleStatus();
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  const handleSkillSelect = (skillType: string, skillName: string) => {
    setSkills((prevSkills) => {
      const skillGroup = prevSkills.find(
        (group) => group.skillType === skillType
      );

      if (skillGroup) {
        // Add the skill name to the existing group if it's not already present
        if (!skillGroup.skillNames.includes(skillName)) {
          return prevSkills.map((group) =>
            group.skillType === skillType
              ? { ...group, skillNames: [...group.skillNames, skillName] }
              : group
          );
        }
        return prevSkills; // Skill name already present, no change
      } else {
        // Add a new skill group
        return [...prevSkills, { skillType, skillNames: [skillName] }];
      }
    });
  };

  const handleSkillRemove = (skillType: string, skillName: string) => {
    setSkills(
      (prevSkills) =>
        prevSkills
          .map((group) =>
            group.skillType === skillType
              ? {
                  ...group,
                  skillNames: group.skillNames.filter(
                    (name) => name !== skillName
                  ),
                }
              : group
          )
          .filter((group) => group.skillNames.length > 0) // Remove empty skill groups
    );
  };

  const isSkillSelected = (skillType: string, skillName: string) => {
    const skillGroup = skills.find((group) => group.skillType === skillType);
    return skillGroup?.skillNames.includes(skillName) ?? false;
  };

  useEffect(() => {
    if (freelancer) {
      form.setValue("firstName", freelancer.firstName);
      form.setValue("lastName", freelancer.lastName);
      form.setValue("email", freelancer.email);
      form.setValue("code", freelancer.code!.toString());
      form.setValue("gender", freelancer.gender === "Male" ? 1 : 2);
      form.setValue("dateOfBirth", new Date(freelancer.dateOfBirth));
      form.setValue("phoneNumber", freelancer.phoneNumber);
      form.setValue("warning", freelancer.warning);
      setSkills(freelancer.skills);
      setOpenState(true);
    }
  }, [freelancer]);

  return (
    <>
      <Button
        onClick={() => fetch()}
        size="icon"
        variant="ghost"
        className="size-8"
      >
        <Ellipsis className="size-4" />
      </Button>

      <Dialog open={openState} onOpenChange={setOpenState}>
        <DialogContent>
          <DialogHeader>
            <DialogTitle>Edit freelancer</DialogTitle>
          </DialogHeader>

          <Form {...form}>
            <form
              id="create"
              onSubmit={form.handleSubmit(onSubmit)}
              className="space-y-4"
            >
              <div className="flex gap-4 w-full">
                <FormField
                  control={form.control}
                  name="firstName"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>First Name *</FormLabel>
                      <FormControl>
                        <Input
                          disabled={isLoading}
                          value={field.value ?? ""}
                          onChange={field.onChange}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />

                <FormField
                  control={form.control}
                  name="lastName"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Last Name *</FormLabel>
                      <FormControl>
                        <Input
                          disabled={isLoading}
                          value={field.value ?? ""}
                          onChange={field.onChange}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>

              <div className="flex gap-4 w-full">
                <FormField
                  control={form.control}
                  name="gender"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Gender *</FormLabel>
                      <FormControl>
                        <Select
                          disabled={isLoading}
                          onValueChange={(value) =>
                            field.onChange(Number(value))
                          }
                          value={
                            field.value != undefined ? String(field.value) : ""
                          }
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Gender" />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectItem value="1">Male</SelectItem>
                            <SelectItem value="2">Female</SelectItem>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />

                <FormField
                  control={form.control}
                  name="dateOfBirth"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Date of Birth *</FormLabel>
                      <FormControl>
                        <InputDate
                          disabled={isLoading}
                          date={field.value}
                          setDate={field.onChange}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>

              <div className="flex gap-4 w-full">
                <FormField
                  control={form.control}
                  name="code"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Code *</FormLabel>
                      <FormControl>
                        <Input
                          disabled
                          value={field.value ?? ""}
                          onChange={field.onChange}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />

                <FormField
                  control={form.control}
                  name="warning"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Warning *</FormLabel>
                      <FormControl>
                        <Select
                          disabled={isLoading}
                          onValueChange={(value) =>
                            field.onChange(Number(value))
                          }
                          value={
                            field.value != undefined ? String(field.value) : ""
                          }
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Warning" />
                          </SelectTrigger>
                          <SelectContent>
                            {["0", "1", "2", "3"].map((item) => (
                              <SelectItem value={item}>{item}</SelectItem>
                            ))}
                            <SelectItem value="4">Banned</SelectItem>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>

              <FormField
                control={form.control}
                name="phoneNumber"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Phone Number *</FormLabel>
                    <FormControl>
                      <Input
                        disabled={isLoading}
                        value={field.value ?? ""}
                        onChange={field.onChange}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />

              <FormField
                control={form.control}
                name="email"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Email *</FormLabel>
                    <FormControl>
                      <Input
                        disabled={isLoading}
                        value={field.value ?? ""}
                        onChange={field.onChange}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />

              <div className="space-y-2">
                <p className="font-medium text-sm">Skills</p>

                <div className="flex-wrap gap-2 flex">
                  {skills.map((type) =>
                    type.skillNames.map((name) => (
                      <Badge
                        key={name}
                        variant="outline"
                        className="gap-2 h-fit font-normal"
                        onClick={() => handleSkillRemove(type.skillType, name)}
                      >
                        {name}
                        <X className="size-4 text-muted-foreground hover:text-destructive" />
                      </Badge>
                    ))
                  )}

                  <DropdownMenu>
                    <DropdownMenuTrigger
                      disabled={isLoading}
                      className="flex gap-1 text-xs font-medium items-center text-primary"
                    >
                      Add
                      <Plus className="size-4" />
                    </DropdownMenuTrigger>

                    <DropdownMenuContent>
                      {skillGroup.map((type, index) => (
                        <DropdownMenuSub key={type.skillType}>
                          <DropdownMenuSubTrigger>
                            {type.skillType}
                          </DropdownMenuSubTrigger>
                          <DropdownMenuSubContent>
                            {skillGroup[index].skillNames.map((name) => (
                              <DropdownMenuCheckboxItem
                                key={name}
                                checked={isSkillSelected(type.skillType, name)}
                                onCheckedChange={() =>
                                  handleSkillSelect(type.skillType, name)
                                }
                              >
                                {name}
                              </DropdownMenuCheckboxItem>
                            ))}
                          </DropdownMenuSubContent>
                        </DropdownMenuSub>
                      ))}
                    </DropdownMenuContent>
                  </DropdownMenu>
                </div>
              </div>
            </form>
          </Form>

          <DialogFooter>
            <FreelancerDeleteDialog
              id={id}
              status={freelancer ? freelancer.status : ""}
              isLoadingParent={isLoading}
              onSuccess={() => setOpenState(false)}
            />

            <Button form="create" type="submit" disabled={isLoading}>
              Save
            </Button>
          </DialogFooter>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default FreelancerUpdateDialog;
