import { Button } from "@/components/ui/button";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import useQuery from "@/hooks/useQuery";
import { SkillGroup } from "@/types";
import {
  ArrowDownNarrowWide,
  ArrowDownWideNarrow,
  RotateCcw,
} from "lucide-react";
import { FC, useEffect, useState } from "react";

interface FreelancerFilterProps {
  SkillGroup: SkillGroup[];
}

const FreelancerFilter: FC<FreelancerFilterProps> = ({ SkillGroup }) => {
  const [skillNames, setSkillNames] = useState<string[]>([]);
  const query = useQuery();

  useEffect(() => {
    const selectedSkillType = query.getQueryValue("skill-type");
    const selectedSkillGroup = SkillGroup.find(
      (group) => group.skillType === selectedSkillType
    );
    if (selectedSkillGroup) {
      setSkillNames(selectedSkillGroup.skillNames);
    } else {
      setSkillNames([]);
    }
  }, [query.getQueryValue("skill-type"), SkillGroup]);

  return (
    <div className="flex flex-col gap-4">
      <Select
        onValueChange={(e) => {
          query.setQuery("skill-type", e);
          query.setQuery("skill-name", "");
        }}
        value={query.getQueryValue("skill-type")}
      >
        <SelectTrigger className="bg-background w-full">
          <SelectValue placeholder="Skill Type" />
        </SelectTrigger>
        <SelectContent>
          {SkillGroup.map((item, index) => (
            <SelectItem key={index} value={item.skillType}>
              {item.skillType}
            </SelectItem>
          ))}
          <SelectItem value="all">All</SelectItem>
        </SelectContent>
      </Select>

      <Select
        onValueChange={(e) => {
          query.setQuery("skill-name", e);
        }}
        value={query.getQueryValue("skill-name")}
        disabled={skillNames.length === 0}
      >
        <SelectTrigger className="bg-background w-full">
          <SelectValue placeholder="Skill Name" />
        </SelectTrigger>
        <SelectContent>
          {skillNames.map((item, index) => (
            <SelectItem key={index} value={item}>
              {item}
            </SelectItem>
          ))}
          <SelectItem value="all">All</SelectItem>
        </SelectContent>
      </Select>

      <Select
        onValueChange={(e) => {
          query.setQuery("gender", e);
        }}
        value={query.getQueryValue("gender")}
      >
        <SelectTrigger className="bg-background w-full">
          <SelectValue placeholder="Gender" />
        </SelectTrigger>
        <SelectContent>
          <SelectItem value="1">Male</SelectItem>
          <SelectItem value="2">Female</SelectItem>
          <SelectItem value="all">All</SelectItem>
        </SelectContent>
      </Select>

      <Select
        onValueChange={(e) => {
          query.setQuery("status", e);
        }}
        value={query.getQueryValue("status")}
      >
        <SelectTrigger className="bg-background w-full">
          <SelectValue placeholder="State" />
        </SelectTrigger>
        <SelectContent>
          <SelectItem value="Available">Available</SelectItem>
          <SelectItem value="NotAvailable">Not Available</SelectItem>
          <SelectItem value="all">All</SelectItem>
        </SelectContent>
      </Select>

      <div className="flex gap-2">
        <Select
          onValueChange={(e) => {
            query.setQuery("order", e);
          }}
          value={query.getQueryValue("order")}
        >
          <SelectTrigger className="bg-background w-full">
            <SelectValue placeholder="Sort" />
          </SelectTrigger>
          <SelectContent>
            <SelectItem value="first-name">First Name</SelectItem>
            <SelectItem value="last-name">Last Name</SelectItem>
            <SelectItem value="code">Code</SelectItem>
            <SelectItem value="date-of-birth">Date of Birth</SelectItem>
            <SelectItem value="all">No sort</SelectItem>
          </SelectContent>
        </Select>

        <Button
          onClick={() => query.setSortDirection()}
          variant="outline"
          size="icon"
          className="shrink-0"
        >
          {query.getQueryValue("order-by-descending") === null ||
          query.getQueryValue("order-by-descending") === "true" ? (
            <ArrowDownWideNarrow className="size-4" />
          ) : (
            <ArrowDownNarrowWide className="size-4" />
          )}
        </Button>
      </div>

      <Button
        onClick={() => {
          query.resetQuery(["pageSize"]);
        }}
        variant="outline"
        className="gap-2"
      >
        <RotateCcw className="size-4" />
        Reset
      </Button>
    </div>
  );
};

export default FreelancerFilter;
