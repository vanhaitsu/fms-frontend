import { Button } from "@/components/ui/button";
import { toast } from "@/components/ui/use-toast";
import { FileSpreadsheet } from "lucide-react";
import { ChangeEvent, MouseEvent, useRef, useState } from "react";
import * as XLSX from "xlsx";
import moment from "moment";
import { IMPORT_FREELANCERS } from "@/lib/api";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { DataTable } from "@/components/DataTable";
import { FreelancerImportColumns } from "./FreelancerImportColumns";
import { ScrollArea } from "@/components/ui/scroll-area";

type Skill = {
  "skill-type": string;
  "skill-names": string[];
};

export type FreelancerImport = {
  "first-name": string;
  "last-name": string;
  email: string;
  "phone-number": string;
  "date-of-birth": string;
  gender: number;
  code: string;
  address: string;
  skills: Skill[];
};

type FreelancerImportRaw = {
  Code: string;
  LastName: string;
  FirstName: string;
  Email: string;
  Address?: string;
  DateOfBirth: string;
  PhoneNumber: string;
  Gender: string;
  Skills?: string;
};

export const parseSkills = (skillsString?: string): Skill[] => {
  if (!skillsString) return [];
  const skillEntries = skillsString.split(";").map((entry) => entry.trim());
  return skillEntries.reduce((acc, entry) => {
    if (entry) {
      const [skillType, skillNames] = entry.split(":").map((s) => s.trim());
      const skillNamesArray = skillNames
        ? skillNames.split(",").map((skill) => skill.trim())
        : [];
      acc.push({ "skill-type": skillType, "skill-names": skillNamesArray });
    }
    return acc;
  }, [] as Skill[]);
};

const FreelancerImportButton = () => {
  const [freelancers, setFreelancers] = useState<FreelancerImport[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const fileInputRef = useRef<any>(null);

  const handleFile = async (e: ChangeEvent<HTMLInputElement>) => {
    const selectedFile = e.target.files && e.target.files[0];
    if (!selectedFile) return;

    const arrayBuffer = await selectedFile.arrayBuffer();
    const workbook = XLSX.read(arrayBuffer, { type: "array" });
    const worksheetName = workbook.SheetNames[0];
    const worksheet = workbook.Sheets[worksheetName];
    const data = XLSX.utils.sheet_to_json(worksheet, {
      raw: false,
      dateNF: "dd-mm-yyyy",
    }) as FreelancerImportRaw[];

    const parsedData = data.map((item) => ({
      ...item,
      skills: parseSkills(item.Skills),
      "first-name": item.FirstName,
      "last-name": item.LastName,
      email: item.Email,
      "phone-number": item.PhoneNumber,
      "date-of-birth": moment(item.DateOfBirth, "DD-MM-YYYY").format(
        "YYYY-MM-DD"
      ),
      gender: item.Gender === "Male" ? 0 : 1,
      code: item.Code,
      address: item.Address || "",
      wallet: 0,
      warning: 0,
    }));

    const duplicateEmails = parsedData
      .map((item) => item.email)
      .filter((email, index, self) => self.indexOf(email) !== index);

    const duplicateCodes = parsedData
      .map((item) => item.code)
      .filter((code, index, self) => self.indexOf(code) !== index);

    if (duplicateEmails.length > 0 || duplicateCodes.length > 0) {
      toast({
        variant: "destructive",
        title: "Duplicate Found",
        description: `Duplicate emails: ${duplicateEmails.join(
          ", "
        )}, Duplicate codes: ${duplicateCodes.join(", ")}`,
      });
    } else {
      // console.log(parsedData);
      setFreelancers(parsedData);
      setOpen(true);
    }
  };

  const handleSubmit = async (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    setIsLoading(true);
    try {
      const response = await IMPORT_FREELANCERS(freelancers);
      if (response.status === 200) {
        toast({
          title: "Success",
          description: "Import accounts successfully",
        });
      }
    } catch {
      toast({
        variant: "destructive",
        title: "Error",
        description: "Something went wrong",
      });
    } finally {
      setIsLoading(false);
      setOpen(false);
    }
  };

  return (
    <>
      <input
        type="file"
        accept=".xlsx"
        onChange={handleFile}
        ref={fileInputRef}
        hidden
      />

      <Button
        variant="outline"
        className="sm:ml-auto gap-2 w-full sm:w-fit"
        onClick={() => {
          if (fileInputRef.current.value) {
            fileInputRef.current.value = "";
            setFreelancers([]);
          }
          fileInputRef.current.click();
        }}
      >
        <FileSpreadsheet className="size-4" />
        Import
      </Button>

      <Dialog open={open} onOpenChange={setOpen}>
        <DialogContent className="sm:max-w-screen-xl">
          <DialogHeader>
            <DialogTitle>Import freelancers</DialogTitle>
          </DialogHeader>
          <ScrollArea className="h-[500px]">
            <DataTable data={freelancers} columns={FreelancerImportColumns} />
          </ScrollArea>

          <DialogFooter>
            <DialogClose asChild>
              <Button variant="outline" disabled={isLoading}>
                Cancel
              </Button>
            </DialogClose>

            <Button onClick={handleSubmit} disabled={isLoading}>
              Save
            </Button>
          </DialogFooter>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default FreelancerImportButton;
