import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { Badge } from "@/components/ui/badge";
import { Freelancer, SkillGroup } from "@/types";
import { ColumnDef } from "@tanstack/react-table";
import FreelancerUpdateDialog from "./FreelancerUpdateDialog";

export const FreelancerColumns: ColumnDef<Freelancer>[] = [
  {
    accessorKey: "id",
    header: "Id",
  },
  {
    accessorKey: "image",
    header: "Image",
    cell: ({ row }) => (
      <Avatar>
        <AvatarImage src={row.getValue("image") ?? "#"} />
        <AvatarFallback>
          {row.getValue("firstName")!.toString().charAt(0).toUpperCase()}
        </AvatarFallback>
      </Avatar>
    ),
  },
  {
    accessorKey: "firstName",
    header: "First Name",
  },
  {
    accessorKey: "lastName",
    header: "Last Name",
  },
  {
    accessorKey: "code",
    header: "Code",
  },
  {
    accessorKey: "email",
    header: "Email",
  },
  {
    accessorKey: "warning",
    header: "Warning",
    // cell: ({ row }) => {
    //   const warning: number = row.getValue("warning");

    //   const warningColor = (warning: number): string => {
    //     switch (warning) {
    //       case 1:
    //         return "text-emerald-400";
    //       case 2:
    //         return "text-amber-400";
    //       case 3:
    //         return "text-rose-400";
    //       default:
    //         return "";
    //     }
    //   };

    //   return <p className={`${warningColor(warning)}`}>{warning}</p>;
    // },
  },
  {
    accessorKey: "skills",
    header: "Skill Type",
    cell: ({ row }) => (
      <div className="flex-wrap max-w-60 w-fit gap-2 flex">
        {(row.getValue("skills") as SkillGroup[]).map((item: SkillGroup) => (
          <Badge variant="outline">{item.skillType}</Badge>
        ))}
      </div>
    ),
  },
  {
    id: "action",
    cell: ({ row }) => <FreelancerUpdateDialog id={row.getValue("id")} />,
  },
];
