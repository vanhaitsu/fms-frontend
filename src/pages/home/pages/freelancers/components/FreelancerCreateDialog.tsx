import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Plus, X } from "lucide-react";
import { FC, useState } from "react";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import InputDate from "@/components/InputDate";
import { toast } from "@/components/ui/use-toast";
import { IMPORT_FREELANCERS, REGISTER_ACCOUNT } from "@/lib/api";
import { freelancerCreateSchema } from "@/lib/form";
import { useUpdateFreelancerStore } from "@/config/store";
import { SkillGroup } from "@/types";
import {
  DropdownMenu,
  DropdownMenuCheckboxItem,
  DropdownMenuContent,
  DropdownMenuSub,
  DropdownMenuSubContent,
  DropdownMenuSubTrigger,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { Badge } from "@/components/ui/badge";

interface FreelancerCreateDialogProps {
  skillGroup: SkillGroup[];
}

const FreelancerCreateDialog: FC<FreelancerCreateDialogProps> = ({
  skillGroup: SkillGroup,
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [skills, setSkills] = useState<SkillGroup[]>([]);
  const update = useUpdateFreelancerStore();

  const form = useForm<z.infer<typeof freelancerCreateSchema>>({
    resolver: zodResolver(freelancerCreateSchema),
  });

  const onSubmit = async (values: z.infer<typeof freelancerCreateSchema>) => {
    try {
      setIsLoading(true);
      const response = await IMPORT_FREELANCERS([
        {
          ...values,
          skills: skills,
        },
      ]);
      if (response.status) {
        toast({
          description: response.data.message,
        });
        form.reset();
        setSkills([]);
        update.toggleStatus();
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  const handleSkillSelect = (skillType: string, skillName: string) => {
    setSkills((prevSkills) => {
      const skillGroup = prevSkills.find(
        (group) => group.skillType === skillType
      );

      if (skillGroup) {
        // Add the skill name to the existing group if it's not already present
        if (!skillGroup.skillNames.includes(skillName)) {
          return prevSkills.map((group) =>
            group.skillType === skillType
              ? { ...group, skillNames: [...group.skillNames, skillName] }
              : group
          );
        }
        return prevSkills; // Skill name already present, no change
      } else {
        // Add a new skill group
        return [...prevSkills, { skillType, skillNames: [skillName] }];
      }
    });
  };

  const handleSkillRemove = (skillType: string, skillName: string) => {
    setSkills(
      (prevSkills) =>
        prevSkills
          .map((group) =>
            group.skillType === skillType
              ? {
                  ...group,
                  skillNames: group.skillNames.filter(
                    (name) => name !== skillName
                  ),
                }
              : group
          )
          .filter((group) => group.skillNames.length > 0) // Remove empty skill groups
    );
  };

  const isSkillSelected = (skillType: string, skillName: string) => {
    const skillGroup = skills.find((group) => group.skillType === skillType);
    return skillGroup?.skillNames.includes(skillName) ?? false;
  };

  return (
    <Dialog
      onOpenChange={(openState) => {
        if (openState === true) {
          form.reset();
          setSkills([]);
        }
      }}
    >
      <DialogTrigger className="w-full sm:w-fit">
        <Button className="gap-2 w-full">
          <Plus className="size-4" />
          Add
        </Button>
      </DialogTrigger>

      <DialogContent>
        <DialogHeader>
          <DialogTitle>Add new freelancer</DialogTitle>
        </DialogHeader>

        <Form {...form}>
          <form
            id="create"
            onSubmit={form.handleSubmit(onSubmit)}
            className="space-y-4"
          >
            <div className="flex gap-4 w-full">
              <FormField
                control={form.control}
                name="firstName"
                render={({ field }) => (
                  <FormItem className="w-full">
                    <FormLabel>First Name *</FormLabel>
                    <FormControl>
                      <Input
                        disabled={isLoading}
                        value={field.value ?? ""}
                        onChange={field.onChange}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />

              <FormField
                control={form.control}
                name="lastName"
                render={({ field }) => (
                  <FormItem className="w-full">
                    <FormLabel>Last Name *</FormLabel>
                    <FormControl>
                      <Input
                        disabled={isLoading}
                        value={field.value ?? ""}
                        onChange={field.onChange}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>

            <div className="flex gap-4 w-full">
              <FormField
                control={form.control}
                name="gender"
                render={({ field }) => (
                  <FormItem className="w-full">
                    <FormLabel>Gender *</FormLabel>
                    <FormControl>
                      <Select
                        disabled={isLoading}
                        onValueChange={(value) => field.onChange(Number(value))}
                        value={
                          field.value != undefined ? String(field.value) : ""
                        }
                      >
                        <SelectTrigger className="w-full">
                          <SelectValue placeholder="Select" />
                        </SelectTrigger>
                        <SelectContent>
                          <SelectItem value="1">Male</SelectItem>
                          <SelectItem value="2">Female</SelectItem>
                        </SelectContent>
                      </Select>
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />

              <FormField
                control={form.control}
                name="dateOfBirth"
                render={({ field }) => (
                  <FormItem className="w-full">
                    <FormLabel>Date of Birth *</FormLabel>
                    <FormControl>
                      <InputDate
                        disabled={isLoading}
                        date={field.value}
                        setDate={field.onChange}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>

            <FormField
              control={form.control}
              name="code"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel>Code *</FormLabel>
                  <FormControl>
                    <Input
                      disabled={isLoading}
                      value={field.value ?? ""}
                      onChange={field.onChange}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="phoneNumber"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Phone Number *</FormLabel>
                  <FormControl>
                    <Input
                      disabled={isLoading}
                      value={field.value ?? ""}
                      onChange={field.onChange}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="email"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Email *</FormLabel>
                  <FormControl>
                    <Input
                      disabled={isLoading}
                      value={field.value ?? ""}
                      onChange={field.onChange}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <div className="space-y-2">
              <p className="font-medium text-sm">Skills</p>

              <div className="flex-wrap gap-2 flex">
                {skills.map((type) =>
                  type.skillNames.map((name) => (
                    <Badge
                      key={name}
                      variant="outline"
                      className="gap-2 h-fit font-normal"
                      onClick={() => handleSkillRemove(type.skillType, name)}
                    >
                      {name}
                      <X className="size-4 text-muted-foreground hover:text-destructive" />
                    </Badge>
                  ))
                )}

                <DropdownMenu>
                  <DropdownMenuTrigger
                    disabled={isLoading}
                    className="flex gap-1 text-xs font-medium items-center text-primary"
                  >
                    Add
                    <Plus className="size-4" />
                  </DropdownMenuTrigger>

                  <DropdownMenuContent>
                    {SkillGroup.map((type, index) => (
                      <DropdownMenuSub key={type.skillType}>
                        <DropdownMenuSubTrigger>
                          {type.skillType}
                        </DropdownMenuSubTrigger>
                        <DropdownMenuSubContent>
                          {SkillGroup[index].skillNames.map((name) => (
                            <DropdownMenuCheckboxItem
                              key={name}
                              checked={isSkillSelected(type.skillType, name)}
                              onCheckedChange={() =>
                                handleSkillSelect(type.skillType, name)
                              }
                            >
                              {name}
                            </DropdownMenuCheckboxItem>
                          ))}
                        </DropdownMenuSubContent>
                      </DropdownMenuSub>
                    ))}
                  </DropdownMenuContent>
                </DropdownMenu>
              </div>
            </div>
          </form>
        </Form>

        <DialogFooter>
          <Button form="create" type="submit" disabled={isLoading}>
            Save
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default FreelancerCreateDialog;
