import { Badge } from "@/components/ui/badge";
import { Freelancer, SkillGroup } from "@/types";
import { ColumnDef } from "@tanstack/react-table";
import FreelancerUpdateDialog from "./FreelancerUpdateDialog";
import { FreelancerImport, parseSkills } from "./FreelancerImportButton";

export const FreelancerImportColumns: ColumnDef<FreelancerImport>[] = [
  {
    accessorKey: "Code",
    header: "Code",
  },
  {
    accessorKey: "FirstName",
    header: "First Name",
  },
  {
    accessorKey: "LastName",
    header: "Last Name",
  },
  {
    accessorKey: "Email",
    header: "Email",
  },
  {
    accessorKey: "Address",
    header: "Address",
  },
  {
    accessorKey: "DateOfBirth",
    header: "Date of Birth",
  },
  {
    accessorKey: "PhoneNumber",
    header: "Phone Number",
  },
  {
    accessorKey: "Gender",
    header: "Gender",
  },
  {
    accessorKey: "Skills",
    header: "Skills",
    cell: ({ row }) => (
      <p className="max-w-52 space-y-2">
        {parseSkills(row.getValue("Skills")).map((skill, skillIndex) => (
          <div key={skillIndex}>
            <span className="font-medium">{skill["skill-type"]}:</span>{" "}
            {skill["skill-names"].join(", ")}
          </div>
        ))}
      </p>
    ),
  },
];
