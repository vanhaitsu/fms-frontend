import { DataTable } from "@/components/DataTable";
import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import {
  GET_FREELANCERS_BY_FILTERS,
  GET_SKILLS_GROUP_BY_TYPE,
} from "@/lib/api";
import { Freelancer, PaginationData, SkillGroup } from "@/types";
import { FileSpreadsheet, Filter } from "lucide-react";
import { useEffect, useState } from "react";
import useQuery from "@/hooks/useQuery";
import useDebounce from "@/hooks/useDebounce";
import DataTablePagination from "@/components/DataTablePagination";
import { useUpdateFreelancerStore } from "@/config/store";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import {
  Drawer,
  DrawerContent,
  DrawerFooter,
  DrawerTrigger,
} from "@/components/ui/drawer";
import { FreelancerColumns } from "./components/FreelancerColumns";
import FreelancerFilter from "./components/FreelancerFilter";
import FreelancerCreateDialog from "./components/FreelancerCreateDialog";
import { initialPagination } from "@/types/initialSeed";
import FreelancerImportButton from "./components/FreelancerImportButton";

const Freelancers = () => {
  const query = useQuery();
  const debouncedQuery = useDebounce(query.getQuery().toString(), 500);
  const [paginationData, setPaginationData] =
    useState<PaginationData>(initialPagination);
  const [freelancers, setFreelancers] = useState<Freelancer[]>([]);
  const [skillGroup, setSkillGroup] = useState<SkillGroup[]>([]);
  const update = useUpdateFreelancerStore();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_FREELANCERS_BY_FILTERS(debouncedQuery);
      if (response.status) {
        setFreelancers(response.data);
        setPaginationData(
          JSON.parse(response.headers["xPagination"]) as PaginationData
        );
      }
      setIsLoading(false);
    };
    fetch();
  }, [debouncedQuery, update.status]);

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_SKILLS_GROUP_BY_TYPE();
      if (response.status) {
        setSkillGroup(response.data);
      }
    };
    fetch();
  }, []);

  return (
    <main className="p-6 space-y-6">
      <section className="flex sm:flex-row flex-col gap-2">
        <div className="flex gap-2">
          <Input
            placeholder="Search"
            onChange={(e) => {
              query.setQuery("search", e.target.value);
            }}
            value={query.getQueryValue("search")}
            className="bg-background w-full sm:w-96"
          />

          <Popover>
            <PopoverTrigger asChild>
              <Button
                variant="outline"
                className="gap-2 hidden sm:flex"
                disabled={skillGroup.length === 0}
              >
                <Filter className="size-4" />
                Filter
              </Button>
            </PopoverTrigger>
            <PopoverContent>
              <FreelancerFilter SkillGroup={skillGroup} />
            </PopoverContent>
          </Popover>

          <Drawer>
            <DrawerTrigger>
              <Button
                variant="outline"
                size="icon"
                disabled={skillGroup.length === 0}
                className="flex sm:hidden"
              >
                <Filter className="size-4" />
              </Button>
            </DrawerTrigger>
            <DrawerContent>
              <DrawerFooter>
                <FreelancerFilter SkillGroup={skillGroup} />
              </DrawerFooter>
            </DrawerContent>
          </Drawer>
        </div>

        <FreelancerImportButton />
        <FreelancerCreateDialog skillGroup={skillGroup} />
      </section>

      <Card>
        <CardHeader>
          <CardTitle>Freelancers</CardTitle>
          <CardDescription>Manage all freelancers here</CardDescription>
        </CardHeader>
        <CardContent>
          <DataTable
            columns={FreelancerColumns}
            data={freelancers}
            visibilityState={{ id: false }}
            isLoading={isLoading}
          />
        </CardContent>
        <CardFooter>
          <DataTablePagination paginationData={paginationData} />
        </CardFooter>
      </Card>
    </main>
  );
};

export default Freelancers;
