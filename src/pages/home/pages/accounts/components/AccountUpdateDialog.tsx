import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import { Ellipsis } from "lucide-react";
import { useEffect, useState } from "react";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import InputDate from "@/components/InputDate";
import { toast } from "@/components/ui/use-toast";
import { GET_ACCOUNT, UPDATE_ACCOUNT } from "@/lib/api";
import { accountUpdateSchema } from "@/lib/form";
import { Account } from "@/types";
import { useUpdateAccountStore } from "@/config/store";
import AccountDeleteDialog from "./AccountDeleteDialog";

const AccountUpdateDialog = ({ id }: { id: string }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [account, setAccount] = useState<Account | null>(null);
  const [openState, setOpenState] = useState(false);
  const update = useUpdateAccountStore();

  const fetch = async () => {
    const response = await GET_ACCOUNT(id);
    if (response.status) {
      setAccount(response.data.data);
    }
  };

  useEffect(() => {
    if (account) {
      form.setValue("firstName", account.firstName);
      form.setValue("lastName", account.lastName);
      form.setValue("email", account.email);
      form.setValue("code", account.code!.toString());
      form.setValue("gender", account.gender === "Male" ? 1 : 2);
      form.setValue("role", account.role === "Administrator" ? 0 : 1);
      form.setValue("dateOfBirth", new Date(account.dateOfBirth));
      form.setValue("phoneNumber", account.phoneNumber);
      setOpenState(true);
    }
  }, [account]);

  const form = useForm<z.infer<typeof accountUpdateSchema>>({
    resolver: zodResolver(accountUpdateSchema),
  });

  const onSubmit = async (values: z.infer<typeof accountUpdateSchema>) => {
    try {
      setIsLoading(true);
      const response = await UPDATE_ACCOUNT(values, id);
      if (response.status) {
        toast({
          description: response.data.message,
        });
        update.toggleStatus();
      }
    } catch (error: any) {
      toast({
        variant: "destructive",
        description: error.response.data.message,
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <>
      <Button
        onClick={() => fetch()}
        size="icon"
        variant="ghost"
        className="size-8"
      >
        <Ellipsis className="size-4" />
      </Button>

      <Dialog open={openState} onOpenChange={setOpenState}>
        <DialogContent>
          <DialogHeader>
            <DialogTitle>Edit account</DialogTitle>
          </DialogHeader>

          <Form {...form}>
            <form
              id="create"
              onSubmit={form.handleSubmit(onSubmit)}
              className="space-y-4"
            >
              <div className="flex gap-4 w-full">
                <FormField
                  control={form.control}
                  name="firstName"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>First Name *</FormLabel>
                      <FormControl>
                        <Input
                          disabled={isLoading}
                          value={field.value ?? ""}
                          onChange={field.onChange}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />

                <FormField
                  control={form.control}
                  name="lastName"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Last Name *</FormLabel>
                      <FormControl>
                        <Input
                          disabled={isLoading}
                          value={field.value ?? ""}
                          onChange={field.onChange}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>

              <div className="flex gap-4 w-full">
                <FormField
                  control={form.control}
                  name="gender"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Gender *</FormLabel>
                      <FormControl>
                        <Select
                          disabled={isLoading}
                          onValueChange={(value) =>
                            field.onChange(Number(value))
                          }
                          value={
                            field.value != undefined ? String(field.value) : ""
                          }
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Gender" />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectItem value="1">Male</SelectItem>
                            <SelectItem value="2">Female</SelectItem>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />

                <FormField
                  control={form.control}
                  name="dateOfBirth"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Date of Birth *</FormLabel>
                      <FormControl>
                        <InputDate
                          disabled={isLoading}
                          date={field.value}
                          setDate={field.onChange}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>

              <div className="flex gap-4 w-full">
                <FormField
                  control={form.control}
                  name="code"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Code *</FormLabel>
                      <FormControl>
                        <Input
                          disabled
                          value={field.value ?? ""}
                          onChange={field.onChange}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />

                <FormField
                  control={form.control}
                  name="role"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel>Role *</FormLabel>
                      <FormControl>
                        <Select
                          disabled={isLoading}
                          onValueChange={(value) =>
                            field.onChange(Number(value))
                          }
                          value={
                            field.value != undefined ? String(field.value) : ""
                          }
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue placeholder="Role" />
                          </SelectTrigger>
                          <SelectContent>
                            <SelectItem value="0">Administrator</SelectItem>
                            <SelectItem value="1">Staff</SelectItem>
                          </SelectContent>
                        </Select>
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>

              <FormField
                control={form.control}
                name="phoneNumber"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Phone Number *</FormLabel>
                    <FormControl>
                      <Input
                        disabled={isLoading}
                        value={field.value ?? ""}
                        onChange={field.onChange}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />

              <FormField
                control={form.control}
                name="email"
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>Email *</FormLabel>
                    <FormControl>
                      <Input
                        disabled={isLoading}
                        value={field.value ?? ""}
                        onChange={field.onChange}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </form>
          </Form>

          <DialogFooter>
            <AccountDeleteDialog
              id={id}
              isDeleted={account ? account.isDeleted : true}
              isLoadingParent={isLoading}
              onSuccess={() => setOpenState(false)}
            />

            <Button form="create" type="submit" disabled={isLoading}>
              Save
            </Button>
          </DialogFooter>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default AccountUpdateDialog;
