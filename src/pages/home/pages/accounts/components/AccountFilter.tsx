import { Button } from "@/components/ui/button";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import useQuery from "@/hooks/useQuery";
import {
  ArrowDownNarrowWide,
  ArrowDownWideNarrow,
  RotateCcw,
} from "lucide-react";

const AccountFilter = () => {
  const query = useQuery();

  return (
    <div className="flex flex-col gap-4">
      <Select
        onValueChange={(e) => {
          query.setQuery("role", e);
        }}
        value={query.getQueryValue("role")}
      >
        <SelectTrigger className="bg-background w-full">
          <SelectValue placeholder="Role" />
        </SelectTrigger>
        <SelectContent>
          <SelectItem value="0">Administrator</SelectItem>
          <SelectItem value="1">Staff</SelectItem>
          <SelectItem value="all">All</SelectItem>
        </SelectContent>
      </Select>

      <Select
        onValueChange={(e) => {
          query.setQuery("gender", e);
        }}
        value={query.getQueryValue("gender")}
      >
        <SelectTrigger className="bg-background w-full">
          <SelectValue placeholder="Gender" />
        </SelectTrigger>
        <SelectContent>
          <SelectItem value="1">Male</SelectItem>
          <SelectItem value="2">Female</SelectItem>
          <SelectItem value="all">All</SelectItem>
        </SelectContent>
      </Select>

      <Select
        onValueChange={(e) => {
          query.setQuery("is-deleted", e);
        }}
        value={query.getQueryValue("is-deleted")}
      >
        <SelectTrigger className="bg-background w-full">
          <SelectValue placeholder="State" />
        </SelectTrigger>
        <SelectContent>
          <SelectItem value="false">Active</SelectItem>
          <SelectItem value="true">Inactive</SelectItem>
          <SelectItem value="all">All</SelectItem>
        </SelectContent>
      </Select>

      <div className="flex gap-2">
        <Select
          onValueChange={(e) => {
            query.setQuery("order", e);
          }}
          value={query.getQueryValue("order")}
        >
          <SelectTrigger className="bg-background w-full">
            <SelectValue placeholder="Sort" />
          </SelectTrigger>
          <SelectContent>
            <SelectItem value="first-name">First Name</SelectItem>
            <SelectItem value="last-name">Last Name</SelectItem>
            <SelectItem value="code">Code</SelectItem>
            <SelectItem value="date-of-birth">Date of Birth</SelectItem>
            <SelectItem value="all">No sort</SelectItem>
          </SelectContent>
        </Select>

        <Button
          onClick={() => query.setSortDirection()}
          variant="outline"
          size="icon"
          className="shrink-0"
        >
          {query.getQueryValue("order-by-descending") === null ||
          query.getQueryValue("order-by-descending") === "true" ? (
            <ArrowDownWideNarrow className="size-4" />
          ) : (
            <ArrowDownNarrowWide className="size-4" />
          )}
        </Button>
      </div>

      <Button
        onClick={() => {
          query.resetQuery(["page-size"]);
        }}
        variant="outline"
        className="gap-2"
      >
        <RotateCcw className="size-4" />
        Reset
      </Button>
    </div>
  );
};

export default AccountFilter;
