import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar";
import { Account } from "@/types";
import { ColumnDef } from "@tanstack/react-table";
import AccountUpdateDialog from "./AccountUpdateDialog";
import { Badge } from "@/components/ui/badge";

export const AccountColumns: ColumnDef<Account>[] = [
  {
    accessorKey: "id",
    header: "Id",
  },
  {
    accessorKey: "image",
    header: "Image",
    cell: ({ row }) => (
      <Avatar>
        <AvatarImage src={row.getValue("image") ?? "#"} />
        <AvatarFallback>
          {row.getValue("firstName")!.toString().charAt(0).toUpperCase()}
        </AvatarFallback>
      </Avatar>
    ),
  },
  {
    accessorKey: "firstName",
    header: "First Name",
  },
  {
    accessorKey: "lastName",
    header: "Last Name",
  },
  {
    accessorKey: "code",
    header: "Code",
  },
  {
    accessorKey: "email",
    header: "Email",
  },
  {
    accessorKey: "gender",
    header: "Gender",
  },
  {
    accessorKey: "role",
    header: "Role",
    cell: ({ row }) => <Badge variant="outline">{row.getValue("role")}</Badge>,
  },
  {
    id: "action",
    cell: ({ row }) => <AccountUpdateDialog id={row.getValue("id")} />,
  },
];
