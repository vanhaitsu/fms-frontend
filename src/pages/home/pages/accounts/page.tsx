import { DataTable } from "@/components/DataTable";
import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import { GET_ACCOUNTS_BY_FILTERS } from "@/lib/api";
import { Account, PaginationData } from "@/types";
import { Filter } from "lucide-react";
import { useEffect, useState } from "react";
import { AccountColumns } from "./components/AccountColumns";
import useQuery from "@/hooks/useQuery";
import useDebounce from "@/hooks/useDebounce";
import { initialPagination } from "@/types/initialSeed";
import DataTablePagination from "@/components/DataTablePagination";
import AccountCreateDialog from "./components/AccountCreateDialog";
import { useUpdateAccountStore } from "@/config/store";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import AccountFilter from "./components/AccountFilter";
import {
  Drawer,
  DrawerContent,
  DrawerFooter,
  DrawerTrigger,
} from "@/components/ui/drawer";

const Accounts = () => {
  const query = useQuery();
  const debouncedQuery = useDebounce(query.getQuery().toString(), 500);
  const [paginationData, setPaginationData] =
    useState<PaginationData>(initialPagination);
  const [accounts, setAccounts] = useState<Account[]>([]);
  const update = useUpdateAccountStore();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetch = async () => {
      const response = await GET_ACCOUNTS_BY_FILTERS(debouncedQuery);
      if (response.status) {
        setAccounts(response.data);
        setPaginationData(
          JSON.parse(response.headers["xPagination"]) as PaginationData
        );
      }
      setIsLoading(false);
    };
    fetch();
  }, [debouncedQuery, update.status]);

  return (
    <main className="p-6 space-y-6">
      <section className="flex sm:flex-row flex-col gap-2">
        <div className="flex gap-2">
          <Input
            placeholder="Search"
            onChange={(e) => {
              query.setQuery("search", e.target.value);
            }}
            value={query.getQueryValue("search")}
            className="bg-background w-full sm:w-96"
          />

          <Popover>
            <PopoverTrigger className="hidden sm:block">
              <Button variant="outline" className="gap-2">
                <Filter className="size-4" />
                Filter
              </Button>
            </PopoverTrigger>
            <PopoverContent>
              <AccountFilter />
            </PopoverContent>
          </Popover>

          <Drawer>
            <DrawerTrigger>
              <Button variant="outline" size="icon" className="flex sm:hidden">
                <Filter className="size-4" />
              </Button>
            </DrawerTrigger>
            <DrawerContent>
              <DrawerFooter>
                <AccountFilter />
              </DrawerFooter>
            </DrawerContent>
          </Drawer>
        </div>

        <AccountCreateDialog />
      </section>

      <Card>
        <CardHeader>
          <CardTitle>Accounts</CardTitle>
          <CardDescription>Manage all accounts here</CardDescription>
        </CardHeader>
        <CardContent>
          <DataTable
            columns={AccountColumns}
            data={accounts}
            visibilityState={{ id: false }}
            isLoading={isLoading}
          />
        </CardContent>
        <CardFooter>
          <DataTablePagination paginationData={paginationData} />
        </CardFooter>
      </Card>
    </main>
  );
};

export default Accounts;
