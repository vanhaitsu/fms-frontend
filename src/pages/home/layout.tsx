import { Outlet } from "react-router-dom";
import Navbar from "./components/Navbar";
import {
  Briefcase,
  BriefcaseBusiness,
  File,
  Folder,
  FolderOpen,
  Home,
  PencilRuler,
  Settings,
  User,
} from "lucide-react";
import Footer from "./components/Footer";
import PrivateComponent from "@/components/PrivateComponent";

const HomeLayout = () => {
  return (
    <div className="lg:flex h-full">
      <PrivateComponent isAllowed={["Administrator"]}>
        <Navbar
          linkList={[
            {
              tittle: "Home",
              url: "/home",
              icon: <Home className="size-4" />,
            },
            {
              groupTittle: "Projects",
            },
            {
              tittle: "Projects",
              url: "/home/projects",
              icon: <Folder className="size-4" />,
            },
            {
              tittle: "Deliverable types",
              url: "/home/deliverable-types",
              icon: <File className="size-4" />,
            },
            {
              groupTittle: "Users",
            },
            {
              tittle: "Accounts",
              url: "/home/accounts",
              icon: <User className="size-4" />,
            },
            {
              tittle: "Freelancers",
              url: "/home/freelancers",
              icon: <BriefcaseBusiness className="size-4" />,
            },
            {
              tittle: "Skills",
              url: "/home/skills",
              icon: <PencilRuler className="size-4" />,
            },
          ]}
        />
      </PrivateComponent>

      <PrivateComponent isAllowed={["Staff"]}>
        <Navbar
          linkList={[
            {
              tittle: "Home",
              url: "/home",
              icon: <Home className="size-4" />,
            },
            {
              groupTittle: "Projects",
            },
            {
              tittle: "Projects",
              url: "/home/projects",
              icon: <Folder className="size-4" />,
            },
            {
              tittle: "Deliverable types",
              url: "/home/deliverable-types",
              icon: <File className="size-4" />,
            },
          ]}
        />
      </PrivateComponent>

      <PrivateComponent isAllowed={["Freelancer"]}>
        <Navbar
          linkList={[
            {
              tittle: "Home",
              url: "/home",
              icon: <Home className="size-4" />,
            },
            {
              groupTittle: "Projects",
            },
            {
              tittle: "Projects",
              url: "/home/projects",
              icon: <Folder className="size-4" />,
            },
            {
              tittle: "Applied projects",
              url: "/home/applied-projects",
              icon: <FolderOpen className="size-4" />,
            },
          ]}
        />
      </PrivateComponent>

      <section className="bg-muted/40 w-full overflow-y-auto justify-between flex flex-col h-full">
        <Outlet />
        <Footer />
      </section>
    </div>
  );
};

export default HomeLayout;
