import { type ClassValue, clsx } from "clsx";
import { twMerge } from "tailwind-merge";

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export const formatNumberToDecimal = (
  number?: number,
  min: number = 2,
  max: number = 2
): string => {
  return new Intl.NumberFormat("en-US", {
    style: "decimal",
    minimumFractionDigits: min,
    maximumFractionDigits: max < min ? min : max,
  }).format(number ?? 0);
};

export const convertEnumToList = (enumObject: any): string[] => {
  return Object.keys(enumObject)
    .filter((key) => isNaN(Number(key)))
    .map((key) => key);
};
