import api from "@/config/axios";
import { AccountRole } from "@/types";
import {
  DeliverableProductStatus,
  ProjectApplyStatus,
  ProjectStatus,
} from "@/types/initialSeed";

export const GET_USER = async (id: string, role: AccountRole) => {
  let response;
  if (role === "Freelancer") {
    response = await GET_FREELANCER(id);
  } else {
    response = await GET_ACCOUNT(id);
  }
  return response;
};

export const GET_ACCOUNTS_BY_FILTERS = async (query?: string) => {
  let response = await api.get("api/v1/account?" + query);
  return response;
};

export const REGISTER_ACCOUNT = async (account: object) => {
  let response = await api.post("api/v1/authentication/register", account);
  return response;
};

export const GET_ACCOUNT = async (id: string) => {
  let response = await api.get("api/v1/account/" + id);
  return response;
};

export const GET_FREELANCER = async (id: string) => {
  let response = await api.get("api/v1/freelancer/" + id);
  return response;
};

export const UPDATE_ACCOUNT = async (account: object, id: string) => {
  let response = await api.put("api/v1/account/" + id, account);
  return response;
};

export const DELETE_ACCOUNT = async (id: string) => {
  let response = await api.delete("api/v1/account/" + id);
  return response;
};

export const RESTORE_ACCOUNT = async (id: string) => {
  let response = await api.put("api/v1/account/restore/" + id);
  return response;
};

export const GET_FREELANCERS_BY_FILTERS = async (query?: string) => {
  let response = await api.get("api/v1/freelancer?" + query);
  return response;
};

export const GET_SKILLS_GROUP_BY_TYPE = async () => {
  let response = await api.get("api/v1/skill/group-by-type");
  return response;
};

export const IMPORT_FREELANCERS = async (freelancers: object[]) => {
  let response = await api.post("api/v1/freelancer", freelancers);
  return response;
};

export const UPDATE_FREELANCER = async (freelancer: object, id: string) => {
  let response = await api.put("api/v1/freelancer/" + id, freelancer);
  return response;
};

export const DELETE_FREELANCER = async (id: string[]) => {
  let response = await api.delete("api/v1/freelancer", {
    data: id,
  });
  return response;
};

export const RESTORE_FREELANCER = async (id: string[]) => {
  let response = await api.put("api/v1/freelancer/restore/" + id);
  return response;
};

export const CHANGE_PASSWORD = async (password: object) => {
  let response = await api.post(
    "api/v1/authentication/change-password",
    password
  );
  return response;
};

export const GET_PROJECTS_BY_FILTERS = async (query?: string) => {
  let response = await api.get("api/v1/project?" + query);
  return response;
};

export const GET_PROJECT_CATEGORIES_ID_BY_SKILLS_NAME = async (
  name: string[]
) => {
  let query = "";
  name.forEach((item) => {
    query = query + "&names=" + encodeURIComponent(item);
  });
  let response = await api.get("api/v1/project-category/get-by-name?" + query);
  return response;
};

export const GET_PROJECT_CATEGORIES_BY_FILTER = async (query?: string) => {
  let response = await api.get("api/v1/project-category?" + query);
  return response;
};

export const CREATE_PROJECT = async (project: object) => {
  let response = await api.post("api/v1/project", project);
  return response;
};

export const UPDATE_PROJECT = async (id: string, project: object) => {
  let response = await api.put("api/v1/project/" + id, project);
  return response;
};

export const DELETE_PROJECT = async (id: string) => {
  let response = await api.delete("api/v1/project?id=" + id);
  return response;
};

export const GET_PROJECT_DELIVERABLES_BY_FILTER = async (query?: string) => {
  let response = await api.get("api/v1/project-deliverable?" + query);
  return response;
};

export const GET_DELIVERABLE_TYPES_BY_FILTER = async (query?: string) => {
  let response = await api.get("api/v1/deliverable-type?" + query);
  return response;
};

export const CREATE_PROJECT_DELIVERABLE = async (
  projectDeliverable: object
) => {
  let response = await api.post(
    "api/v1/project-deliverable",
    projectDeliverable
  );
  return response;
};

export const CREATE_DELIVERABLE_TYPE = async (deliverableType: object) => {
  let response = await api.post("api/v1/deliverable-type", deliverableType);
  return response;
};

export const DELETE_PROJECT_DELIVERABLE = async (id: string) => {
  let response = await api.delete("api/v1/project-deliverable?id=" + id);
  return response;
};

export const CREATE_PROJECT_APPLY = async (projectApply: object) => {
  let response = await api.post("api/v1/project-apply", projectApply);
  return response;
};

export const GET_PROJECT = async (id: string) => {
  let response = await api.get("api/v1/project/" + id);
  return response;
};

export const CLOSE_PROJECT = async (id: string, status: ProjectStatus) => {
  let response = await api.put(
    "api/v1/project/close?project-id=" + id + "&status=" + status
  );
  return response;
};

export const GET_DELIVERABLE_PROJECT_BY_FILTER = async (query?: string) => {
  let response = await api.get("api/v1/deliverable-product?" + query);
  return response;
};

export const UPDATE_DELIVERABLE_PROJECT = async (
  id: string,
  object: object
) => {
  let response = await api.put("api/v1/deliverable-product/" + id, object);
  return response;
};

export const GET_PROJECT_APPLY_BY_FILTER = async (query?: string) => {
  let response = await api.get("api/v1/project-apply?" + query);
  return response;
};

export const DELETE_PROJECT_APPLY = async (id: string) => {
  let response = await api.delete("api/v1/project-apply/" + id);
  return response;
};

export const UPDATE_PROJECT_APPLY = async (
  id: string,
  status?: ProjectApplyStatus,
  endDate?: Date
) => {
  let response = await api.put("api/v1/project-apply", {
    id: id,
    status: status,
    endDate: endDate,
  });
  return response;
};

export const CREATE_DELIVERABLE_PRODUCT = async (product: object) => {
  let response = await api.post("api/v1/deliverable-product", product);
  return response;
};

export const DELETE_DELIVERABLE_PRODUCT = async (id: string) => {
  let response = await api.delete("api/v1/deliverable-product?id=" + id);
  return response;
};

export const GET_DASHBOARD = async (role: AccountRole) => {
  let response = await api.get("api/v1/dashboard/" + role.toLocaleLowerCase());
  return response;
};

export const UPDATE_DELIVERABLE_TYPE = async (
  deliverableType: object,
  id: string
) => {
  let response = await api.put(
    "api/v1/deliverable-type/" + id,
    deliverableType
  );
  return response;
};

export const DELETE_DELIVERABLE_TYPE = async (id: string) => {
  let response = await api.delete("api/v1/deliverable-type/" + id);
  return response;
};

export const GET_DELIVERABLE_TYPE = async (id: string) => {
  let response = await api.get("api/v1/deliverable-type/" + id);
  return response;
};

export const GET_SKILLS_BY_FILTERS = async (query?: string) => {
  let response = await api.get("api/v1/skill?" + query);
  return response;
};

export const GET_SKILL = async (id: string) => {
  let response = await api.get("api/v1/skill/" + id);
  return response;
};

export const UPDATE_SKILL = async (skill: object, id: string) => {
  let response = await api.put("api/v1/skill/" + id, skill);
  return response;
};

export const DELETE_SKILL = async (id: string) => {
  let response = await api.delete("api/v1/skill/" + id);
  return response;
};

export const CREATE_SKILL = async (skill: object[]) => {
  let response = await api.post("api/v1/skill", skill);
  return response;
};
