import { z } from "zod";

export const loginSchema = z.object({
  email: z
    .string()
    .email()
    .min(1, { message: "Email is required" })
    .max(256, { message: "Email must be no more than 256 characters" }),
  password: z
    .string()
    .min(8, { message: "Password is required" })
    .max(128, { message: "Password must be from 8 to 128 characters" }),
});

export const accountCreateSchema = z
  .object({
    firstName: z
      .string()
      .min(1, { message: "First Name is required" })
      .max(50, { message: "First Name must be no more than 50 characters" }),
    lastName: z
      .string()
      .min(1, { message: "Last Name is required" })
      .max(50, { message: "Last Name must be no more than 50 characters" }),
    gender: z.number({ required_error: "Gender is required" }),
    dateOfBirth: z.date({ required_error: "Date of Birth is required" }),
    phoneNumber: z
      .string()
      .min(1, { message: "Phone Number is required" })
      .max(15, { message: "Phone Number must be no more than 15 characters" })
      .regex(/^[0-9+()-\s]+$/, { message: "Invalid phone format" }),
    email: z
      .string()
      .min(1, { message: "Email is required" })
      .max(256, { message: "Email must be no more than 256 characters" })
      .email({ message: "Invalid email format" }),
    password: z
      .string()
      .min(1, { message: "Password is required" })
      .min(8, { message: "Password must be from 8 to 128 characters" })
      .max(128, { message: "Password must be from 8 to 128 characters" })
      .regex(/[A-Z]/, {
        message: "Password must contain at least one uppercase letter",
      })
      .regex(/[0-9]/, {
        message: "Password must contain at least one number",
      }),
    confirmPassword: z
      .string()
      .min(1, { message: "Confirm Password is required" })
      .min(8, {
        message: "Confirm Password must be from 8 to 128 characters",
      })
      .max(128, {
        message: "Confirm Password must be from 8 to 128 characters",
      }),
    code: z
      .string()
      .min(1, { message: "Code is required" })
      .max(50, { message: "Code must be no more than 50 characters" }),
    role: z.number({ required_error: "Role is required" }),
  })
  .refine((data) => data.password === data.confirmPassword, {
    path: ["confirmPassword"],
    message: "Passwords must match",
  });

export const accountUpdateSchema = z.object({
  firstName: z
    .string()
    .min(1, { message: "First Name is required" })
    .max(50, { message: "First Name must be no more than 50 characters" }),
  lastName: z
    .string()
    .min(1, { message: "Last Name is required" })
    .max(50, { message: "Last Name must be no more than 50 characters" }),
  gender: z.number({ required_error: "Gender is required" }),
  dateOfBirth: z.date({ required_error: "Date of Birth is required" }),
  phoneNumber: z
    .string()
    .min(1, { message: "Phone Number is required" })
    .max(15, { message: "Phone Number must be no more than 15 characters" })
    .regex(/^[0-9+()-\s]+$/, { message: "Invalid phone format" }),
  email: z
    .string()
    .min(1, { message: "Email is required" })
    .max(256, { message: "Email must be no more than 256 characters" })
    .email({ message: "Invalid email format" }),
  code: z
    .string()
    .min(1, { message: "Code is required" })
    .max(50, { message: "Code must be no more than 50 characters" }),
  role: z.number({ required_error: "Role is required" }),
});

export const freelancerCreateSchema = z.object({
  firstName: z
    .string()
    .min(1, { message: "First Name is required" })
    .max(50, { message: "First Name must be no more than 50 characters" }),
  lastName: z
    .string()
    .min(1, { message: "Last Name is required" })
    .max(50, { message: "Last Name must be no more than 50 characters" }),
  gender: z.number({ required_error: "Gender is required" }),
  dateOfBirth: z.date({ required_error: "Date of Birth is required" }),
  phoneNumber: z
    .string()
    .min(1, { message: "Phone Number is required" })
    .max(15, { message: "Phone Number must be no more than 15 characters" })
    .regex(/^[0-9+()-\s]+$/, { message: "Invalid phone format" }),
  email: z
    .string()
    .min(1, { message: "Email is required" })
    .max(256, { message: "Email must be no more than 256 characters" })
    .email({ message: "Invalid email format" }),
  code: z
    .string()
    .min(1, { message: "Code is required" })
    .max(50, { message: "Code must be no more than 50 characters" }),
});

export const freelancerUpdateSchema = z.object({
  firstName: z
    .string()
    .min(1, { message: "First Name is required" })
    .max(50, { message: "First Name must be no more than 50 characters" }),
  lastName: z
    .string()
    .min(1, { message: "Last Name is required" })
    .max(50, { message: "Last Name must be no more than 50 characters" }),
  gender: z.number({ required_error: "Gender is required" }),
  dateOfBirth: z.date({ required_error: "Date of Birth is required" }),
  phoneNumber: z
    .string()
    .min(1, { message: "Phone Number is required" })
    .max(15, { message: "Phone Number must be no more than 15 characters" })
    .regex(/^[0-9+()-\s]+$/, { message: "Invalid phone format" }),
  email: z
    .string()
    .min(1, { message: "Email is required" })
    .max(256, { message: "Email must be no more than 256 characters" })
    .email({ message: "Invalid email format" }),
  code: z
    .string()
    .min(1, { message: "Code is required" })
    .max(50, { message: "Code must be no more than 50 characters" }),
  warning: z.number({ required_error: "Gender is required" }),
});

export const accountChangePasswordSchema = z
  .object({
    oldPassword: z
      .string()
      .min(1, { message: "Old Password is required" })
      .min(8, { message: "Old Password must be from 8 to 128 characters" }),
    newPassword: z
      .string()
      .min(1, { message: "New Password is required" })
      .min(8, { message: "New Password must be from 8 to 128 characters" })
      .max(128, { message: "New Password must be from 8 to 128 characters" })
      .regex(/[A-Z]/, {
        message: "New Password must contain at least one uppercase letter",
      })
      .regex(/[0-9]/, {
        message: "New Password must contain at least one number",
      }),
    confirmPassword: z
      .string()
      .min(1, { message: "Confirm Password is required" })
      .min(8, {
        message: "Confirm Password must be from 8 to 128 characters",
      })
      .max(128, {
        message: "Confirm Password must be from 8 to 128 characters",
      }),
  })
  .refine((data) => data.newPassword === data.confirmPassword, {
    path: ["confirmPassword"],
    message: "Passwords must match",
  });

export const projectCreateSchema = z
  .object({
    code: z
      .string()
      .min(1, { message: "Code is required" })
      .max(50, { message: "Code must be no more than 50 characters" }),
    name: z
      .string()
      .min(1, { message: "Name is required" })
      .max(256, { message: "Name must be no more than 256 characters" }),
    description: z.string().min(1, { message: "Description is required" }),
    duration: z.coerce
      .number()
      .min(1, { message: "Duration must be a positive number" }),
    price: z.coerce
      .number()
      .min(1, { message: "Price must be a positive number" }),
    deposit: z.coerce
      .number()
      .min(1, { message: "Deposit must be a positive number" }),
    visibility: z.number({ required_error: "Visibility is required" }),
    projectCategoryId: z.string().min(1, { message: "Category is required" }),
  })
  .refine((data) => data.deposit <= data.price, {
    message: "Deposit must be smaller than price",
    path: ["deposit"],
  });

export const projectDeliverableCreateSchema = z.object({
  name: z
    .string()
    .min(1, { message: "Name is required" })
    .max(256, { message: "Name must be no more than 256 characters" }),
  deliverableTypeId: z.string().min(1, { message: "Type is required" }),
});

export const deliverableTypeCreateSchema = z.object({
  name: z
    .string()
    .min(1, { message: "Name is required" })
    .max(256, { message: "Name must be no more than 256 characters" }),
  description: z.string().min(1, { message: "Description is required" }),
});

export const projectUpdateSchema = z
  .object({
    code: z
      .string()
      .min(1, { message: "Code is required" })
      .max(50, { message: "Code must be no more than 50 characters" }),
    name: z
      .string()
      .min(1, { message: "Name is required" })
      .max(256, { message: "Name must be no more than 256 characters" }),
    description: z.string().min(1, { message: "Description is required" }),
    duration: z.coerce
      .number()
      .min(1, { message: "Duration must be a positive number" }),
    price: z.coerce
      .number()
      .min(1, { message: "Price must be a positive number" }),
    deposit: z.coerce
      .number()
      .min(1, { message: "Deposit must be a positive number" }),
    visibility: z.number({ required_error: "Visibility is required" }),
    projectCategoryId: z.string().min(1, { message: "Category is required" }),
  })
  .refine((data) => data.deposit <= data.price, {
    message: "Deposit must be smaller than price",
    path: ["deposit"],
  });

export const skillCreateSchema = z.object({
  type: z
    .string()
    .min(1, { message: "Type is required" })
    .max(50, { message: "Type must be no more than 50 characters" }),
  name: z
    .string()
    .min(1, { message: "Name is required" })
    .max(256, { message: "Name must be no more than 256 characters" }),
  description: z.string(),
});
