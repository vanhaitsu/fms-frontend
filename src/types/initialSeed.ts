import { AccountRole, FreelancerStatus, PaginationData } from ".";

// Enums
export enum ProjectStatus {
  Pending,
  Processing,
  Checking,
  Closed,
  Done,
}

export enum ProjectVisibility {
  Private,
  Public,
}

export enum ProjectDeliverableStatus {
  Checking,
  Accepted,
  Rejected,
}

export enum DeliverableProductStatus {
  Checking,
  Accepted,
  Rejected,
}

export enum ProjectApplyStatus {
  Invited,
  Checking,
  Accepted,
  Rejected,
}

// List
export const accountRoleList: AccountRole[] = [
  "Administrator",
  "Staff",
  "Freelancer",
];

export const freelancerStatusList: FreelancerStatus[] = [
  "Available",
  "NotAvailable",
];

export const initialPagination: PaginationData = {
  CurrentPage: 0,
  PageSize: 0,
  TotalPages: 0,
};
