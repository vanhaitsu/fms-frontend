import {
  DeliverableProductStatus,
  ProjectApplyStatus,
  ProjectDeliverableStatus,
} from "./initialSeed";

// Enums
export type AccountRole = "Administrator" | "Staff" | "Freelancer";
export type FreelancerStatus = "Available" | "NotAvailable";

// Entities
export type User = {
  userId: string;
  userEmail: string;
  accessToken: string;
  accessTokenExpiryTime: Date;
  role: AccountRole;
};

export type PaginationData = {
  // LOL, the header is Pascal Case instead of Camel Case
  CurrentPage: number;
  TotalPages: number;
  PageSize: number;
};

// Entities
export type BaseEntity = {
  id: string;
  isDeleted: boolean;
};

export type Account = BaseEntity & {
  firstName: string;
  lastName: string;
  code?: string;
  phoneNumber: string;
  dateOfBirth: Date;
  gender: string;
  address?: string;
  role?: AccountRole;
  image?: string;
  email: string;
};

export type Freelancer = Account & {
  wallet?: number;
  warning: number;
  status: string;
  invited?: boolean;
  skills: SkillGroup[];
};

export type SkillGroup = {
  skillType: string;
  skillNames: string[];
};

export type Project = BaseEntity & {
  code?: string;
  name: string;
  description: string;
  duration: number;
  price: number;
  deposit: number;
  status: ProjectStatus;
  visibility: ProjectVisibility;
  projectCategoryName?: string;
  accountFirstName?: string;
  accountLastName?: string;
  projectCategoryId?: string;
  accountId?: string;
};

export type ProjectCategory = BaseEntity & {
  name: string;
  description?: string;
};

export type ProjectDeliverable = BaseEntity & {
  name: string;
  deliverableTypeName?: string;
  status: ProjectDeliverableStatus;
  submissionDate?: Date;
};

export type DeliverableType = BaseEntity & {
  name: string;
  description: string;
};

export type DeliverableProduct = BaseEntity & {
  name: string;
  uRL: string;
  status: DeliverableProductStatus;
  feedback?: string;
  projectId?: string;
  projectName?: string;
};

export type ProjectApply = BaseEntity & {
  projectId: string;
  freelancerId: string;
  freelancerFirstName: string;
  freelancerLastName: string;
  image?: string;
  skills?: SkillGroup[];
  startDate?: Date;
  endDate?: Date;
  status: ProjectApplyStatus;
  project?: Project;
};

export type StaffDashboard = {
  numOfYourOngoingProject: number;
  numOfWaitingChecking: number;
  yourTotalPaid: number;
  recentProjects: Project[];
  recentProducts: DeliverableProduct[];
};

export type AdministratorDashboard = StaffDashboard & {
  numOfProject: number;
  numOfFreelancer: number;
  numOfAccount: number;
  totalPaid: number;
};

export type FreelancerDashboard = {
  wallet: number;
  warning: number;
  numOfYourOngoingProject: number;
  remainTasks: number;
  recentProjects: ProjectApply[];
  recentProducts: DeliverableProduct[];
};

export type Skill = BaseEntity & {
  name: string;
  type: string;
  description?: string;
};
