import useAuth from "@/hooks/useAuth";
import { AccountRole } from "@/types";
import React, { FC } from "react";
import { Navigate } from "react-router-dom";

type PrivateRouteProps = {
  isAllowed: AccountRole[] | "PermitAll";
  children: React.ReactNode;
};

const PrivateRoute: FC<PrivateRouteProps> = ({ isAllowed, children }) => {
  const auth = useAuth();

  if (
    (auth.authenticated && auth.user && isAllowed.includes(auth.user.role)) ||
    (auth.authenticated && auth.user && isAllowed === "PermitAll")
  ) {
    return children;
  } else {
    return <Navigate to={"/login"} replace />;
  }
};

export default PrivateRoute;
