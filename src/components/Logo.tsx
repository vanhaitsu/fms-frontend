import { Package } from "lucide-react";
import { FC } from "react";

interface LogoProps {
  displayName?: boolean;
  className?: string;
}

const Logo: FC<LogoProps> = ({ displayName = false, className }) => {
  return (
    <a href="/" className={`flex items-center gap-2 ${className}`}>
      <Package className="size-8" />
      {displayName && (
        <div className="flex flex-col">
          <span className="text-sm font-medium">FMS</span>
          <span className="text-xs text-muted-foreground">
            NextBean Edition
          </span>
        </div>
      )}
    </a>
  );
};

export default Logo;
