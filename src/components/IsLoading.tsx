import { LoaderCircle } from "lucide-react";
import { FC } from "react";

interface IsLoadingProps {
  className?: string;
}

const IsLoading: FC<IsLoadingProps> = ({ className }) => {
  return (
    <div
      className={`flex text-sm font-medium text-muted-foreground gap-2 items-center animate-pulse justify-center w-fit ${className}`}
    >
      <LoaderCircle className="size-4 animate-spin" />
      Loading, please wait...
    </div>
  );
};

export default IsLoading;
