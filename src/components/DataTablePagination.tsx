import { PaginationData } from "@/types";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "./ui/select";
import { Button } from "./ui/button";
import useQuery from "@/hooks/useQuery";
import {
  ChevronLeft,
  ChevronRight,
  ChevronsLeft,
  ChevronsRight,
} from "lucide-react";

const DataTablePagination = ({
  paginationData,
  className,
}: {
  paginationData: PaginationData;
  className?: string;
}) => {
  const query = useQuery();

  const setCurrentPage = (value: number) => {
    query.setQuery("page-index", value.toString());
  };

  if (paginationData.PageSize) {
    return (
      <div
        className={`text-sm font-medium flex items-center gap-2 sm:justify-between w-full flex-col-reverse sm:flex-row ${className}`}
      >
        <div className="flex items-center gap-2">
          <p>Rows per page</p>
          <Select
            value={paginationData.PageSize!.toString()}
            onValueChange={(e) => query.setQuery("page-size", e)}
          >
            <SelectTrigger className="w-20">
              <SelectValue placeholder={paginationData.PageSize} />
            </SelectTrigger>
            <SelectContent side="top">
              {["10", "20", "30", "40", "50"].map((pageSize) => (
                <SelectItem key={pageSize} value={`${pageSize}`}>
                  {pageSize}
                </SelectItem>
              ))}
            </SelectContent>
          </Select>
        </div>

        <div className="flex items-center gap-2">
          <p>
            Page {paginationData.CurrentPage} of {paginationData.TotalPages}
          </p>
          <Button
            variant="outline"
            size="icon"
            disabled={paginationData.CurrentPage <= 1}
            onClick={() => setCurrentPage(1)}
          >
            <ChevronsLeft className="size-4" />
          </Button>
          <Button
            variant="outline"
            size="icon"
            disabled={paginationData.CurrentPage <= 1}
            onClick={() => setCurrentPage(paginationData.CurrentPage - 1)}
          >
            <ChevronLeft className="size-4" />
          </Button>
          <Button
            variant="outline"
            size="icon"
            disabled={paginationData.CurrentPage >= paginationData.TotalPages}
            onClick={() => setCurrentPage(paginationData.CurrentPage + 1)}
          >
            <ChevronRight className="size-4" />
          </Button>
          <Button
            variant="outline"
            size="icon"
            disabled={paginationData.CurrentPage >= paginationData.TotalPages}
            onClick={() => setCurrentPage(paginationData.TotalPages)}
          >
            <ChevronsRight className="size-4" />
          </Button>
        </div>
      </div>
    );
  }

  return null;
};

export default DataTablePagination;
