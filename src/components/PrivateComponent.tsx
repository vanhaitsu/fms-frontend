import useAuth from "@/hooks/useAuth";
import { AccountRole } from "@/types";
import { FC } from "react";

type PrivateComponentProps = {
  isAllowed: AccountRole[];
  children: React.ReactNode;
  accountId?: string; // For authorization
};

const PrivateComponent: FC<PrivateComponentProps> = ({
  isAllowed,
  children,
  accountId,
}) => {
  const auth = useAuth();

  if (
    auth.authenticated &&
    auth.user &&
    auth.authenticated &&
    auth.user &&
    isAllowed.includes(auth.user.role)
  ) {
    if (accountId && accountId === auth.user.userId) {
      return children;
    } else if (!accountId) {
      return children;
    }
  }
};

export default PrivateComponent;
