import { format, isValid } from "date-fns";
import { Button } from "./ui/button";
import { Calendar } from "./ui/calendar";
import { Input } from "./ui/input";
import { Popover, PopoverContent, PopoverTrigger } from "./ui/popover";
import { FC, useRef } from "react";
import { CalendarDays } from "lucide-react";

interface InputDateProps {
  date: Date | undefined;
  setDate: (e: Date) => void;
  className?: string;
  disabled?: boolean;
  startDate?: Date;
  endDate?: Date;
  showCalendar?: boolean;
}

const InputDate: FC<InputDateProps> = ({
  date,
  setDate,
  className,
  disabled,
  startDate,
  endDate,
  showCalendar = true,
}) => {
  //   eslint-disable-next-line @typescript-eslint/no-explicit-any
  const calendarButtonRef = useRef<any>();

  const handleDateChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const inputDate = new Date(e.target.value);
    if (isValid(inputDate)) {
      setDate(new Date(format(inputDate, "yyyy-MM-dd")));
    } else {
      console.error("Invalid date");
    }
  };

  return (
    <div className={`flex gap-2 ${className}`}>
      {date ? (
        <Input
          type="date"
          value={format(new Date(date), "yyyy-MM-dd")}
          onChange={handleDateChange}
          disabled={disabled}
        />
      ) : (
        <Button
          type="button"
          variant="outline"
          className="h-9 w-full px-3 justify-start font-normal hover:bg-transparent"
          onClick={() => {
            setDate(new Date());
            calendarButtonRef.current.click();
          }}
        >
          Pick a date
        </Button>
      )}

      {showCalendar && (
        <Popover>
          <PopoverTrigger asChild disabled={disabled}>
            <Button
              ref={calendarButtonRef}
              variant="outline"
              size="icon"
              className="hover:bg-transparent shrink-0"
            >
              <CalendarDays className="size-4" />
            </Button>
          </PopoverTrigger>
          <PopoverContent className="w-auto p-0" align="center">
            <Calendar
              mode="single"
              onSelect={(e) => setDate(new Date(e!))}
              initialFocus
              selected={date ? new Date(date) : undefined}
              defaultMonth={date ? new Date(date) : undefined}
              disabled={(date) => {
                if (startDate && !endDate) {
                  return date < startDate;
                }
                if (!startDate && endDate) {
                  return date > endDate;
                }
                if (startDate && endDate) {
                  return date < startDate || date > endDate;
                }
                return false;
              }}
            />
          </PopoverContent>
        </Popover>
      )}
    </div>
  );
};

export default InputDate;
